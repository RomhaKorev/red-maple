cd hugo
hugo
cd ../tools/hexa-of-content/

fastMode=""

while :; do
    case $1 in
        -f|--fast) fastMode=1            
        ;;
        *) break
    esac
    shift
done

if [ -z "$fastMode" ]
then
    mvn package
else 
    echo "Fast Mode. Skipping build" 
fi

java -jar target/hexa-of-content-1.0-SNAPSHOT-jar-with-dependencies.jar  "../../hugo/content" "../../hugo/public/index.html"