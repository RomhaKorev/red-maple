# Things Explained

Because we have to continuously improve our skills and practices, we build trainings, kata, dojo to share our learning with the community

Because we do prefer collaborate, we write them with Hugo and Reveal

## Illustrated subjects

You can find more information on their dedicated pages:

[Table Of Contents](https://crazy-crafters.gitlab.io/red-maple)

## Production pipeline

How to build with Hugo and Reveal:

```shell
cd hugo
hugo
```

How to build the table of content:
```shell
cd tools/hexa-of-content
java -jar app/target/generator-1.0-SNAPSHOT-jar-with-dependencies.jar  "../../hugo/content" "../../hugo/public/index.html"
```


How to build legacy slides in LateX:
```shell
cd latex
make
```


The slides will be built in hugo/public, and they are accessible through these Gitlab Pages.

## Usage

### online
The slides are [here](https://crazy-crafters.gitlab.io/red-maple/)

Add the parameter `?print-pdf` in the url just before the last `#` to get a printable version of the slides. \
For example, [https://crazy-crafters.gitlab.io/red-maple/development/oop/basics?print-pdf#/](https://crazy-crafters.gitlab.io/red-maple/development/oop/basics?print-pdf#/)

### PDF version

With Brave, right-click on the slide and select *print*, then `print as PDF`. Remove the margin

### Offline

Each html files is standalone. Open it with your favorite browser (tested with Brave, Chrome and Firefox).


## How to write slides

- The content must be in `hugo/content`
- Create a new directory in one of the existing categories (a directory without a `_index.html` file is a category)
- Create a `_index.html` file containing:
```
+++
title= ""
outputs = ["Reveal"]
subtitle=""
authors=""
draft = true
+++
```
- Write your slides using the Hugo/Reveal nomenclature. You can find the documentation [here](https://github.com/dzello/reveal-hugo)

### Adding snippets

- Create a directory `snippets/your-language` beside the `_index.html` files
- Put the snippet in a source file
- Create a `.md` file with the same name than the snippet (without the extension) to add information about the code
- In the slides:
    - Use the shortcode `{{% snippet "your-snippet" %}}`

You can create a snippet in more than one languages: a button will be added in the slides to select the language

### Layouts and templating

#### introduction & conclusion

Use the shortcode `{{% introduction %}}` at the begining of the `_index.md` file to add the introduction slides

Add a conclusion with:
```
---
{{% conclusion %}}
```

#### Picture

- Create a directory `img` beside the `_index.html` file
- Copy the image into it
- Use the shortcode `{{% illustration "img/you-image" "copyright" %}}`
    - The copyright can be written with html
