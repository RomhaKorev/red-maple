+++
title = "Object Calisthenics"
outputs = ["Reveal"]
[cascade]
    authors = "Dimitry Ernot"
+++

{{% introduction %}}



---
{{% sectionbreak "Rule #1" %}}
Only One Level Of Indentation Per Method
{{% /sectionbreak %}}

---
{{< snippet "levelOfIndention" >}}

---
## Extract method pattern

{{< snippet "levelOfIndention-respected" >}}

---
{{% sectionbreak "Rule #2" %}}
Don’t Use The `ELSE` Keyword
{{% /sectionbreak %}}

---
{{< snippet "elsekeyword-1" >}}

---
We don't need it

{{< snippet "elsekeyword-1-respected" >}}

---
{{< snippet "elsekeyword-2" >}}

---
We may not respect the Single Responsibilities Principle

{{< snippet "elsekeyword-2-respected" >}}

---
{{% sectionbreak "Rule #3" %}}
Wrap All Primitives And Strings
{{% /sectionbreak %}}

---
{{< snippet "primitive-obsession" >}}

---
No primitive obsession: use Value Objects (Name, Money, Age, etc.)

{{< snippet "primitive-obsession-respected" >}}

---
{{% sectionbreak "Rule #4" %}}
First Class Collections

A class containing a collection should not have any other members
{{% /sectionbreak %}}


---
{{< snippet "wrap-collection" >}}

---
Wrap the collection & create an dedicated interface

{{< snippet "wrap-collection-respected" >}}

---
{{% sectionbreak "Rule #5" %}}
One Dot Per Line
{{% /sectionbreak %}}

---
{{< snippet "demeter" >}}

---
Respect the Law of Demeter
{{< snippet "demeter-respected" >}}

<small>Fluent interfaces are not concerned</small>

---
{{% sectionbreak "Rule #6" %}}
Don’t Abbreviate
{{% /sectionbreak %}}

---
- A recurrent name: Sign of code duplication
- A too long name: Sign of Single Responsibilities Principle violation
---
{{% sectionbreak "Rule #7" %}}
Keep All Entities Small
{{% /sectionbreak %}}

---
- Long blocks are harder to understand and maintain
- Sign of Single Responsibilities Principle violation

---
{{% sectionbreak "Rule #8" %}}
No Classes With More Than Two Instance Variables
{{% /sectionbreak %}}

---
{{< snippet "twoInstances" >}}

---
High cohesion, Strong encapsulation

{{< snippet "twoInstances-respected" >}}

---
{{% sectionbreak "Rule #9" %}}
No Getters/Setters/Properties
{{% /sectionbreak %}}

---
We don't need it

{{< snippet "nosetter-1" >}}

---
Tell, don't ask
{{% stretchcolumns %}}
{{% column %}}
Telling the object what to do

{{< snippet "nosetter-2" >}}

{{< snippet "nosetter-2-respected" >}}

{{% /column %}}
{{% column %}}
Protecting our invariants
{{< snippet "nosetter-3" >}}

{{< snippet "nosetter-3-respected" >}}

{{% /column %}}
{{% /stretchcolumns %}}


---

- Rule #1 Only One Level Of Indentation Per Method
- Rule #2 Don’t Use The ELSE Keyword
- Rule #3 Wrap All Primitives And Strings
- Rule #4 First Class Collections
- Rule #5 One Dot Per Line
- Rule #6 Don’t Abbreviate
- Rule #7 Keep All Entities Small
- Rule #8 No Classes With More Than Two Instance Variables
- Rule #9 No Getters/Setters/Properties


---
{{% conclusion %}}