public String getThreatLevelFor(List<Aircraft> aircraftList) {
    if (!aircraftList.isEmpty()) {
        return getHighestLevelFor(aircraftList);
    }
    return "NO THREAT";
}