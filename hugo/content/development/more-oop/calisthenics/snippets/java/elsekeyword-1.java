public String getThreatLevelFor(List<Aircraft> aircraftList) {
    if (!aircraftList.isEmpty()) {
        return getHighestLevelFor(aircraftList);
    } else {
        return "NO THREAT";
    }
}