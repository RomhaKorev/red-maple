public Classifier() {
    database = GeographicalRepositoryProvider.getInstance();

    areasInSector = new ArrayList<>();
    ownLocation = getOwnLocation();
    
    for (Area area : database.getAllAreas()) {
        double distance = Math.sqrt(Math.pow(area.center.x - ownLocation.x, 2) + Math.pow(area.center.y - ownLocation.y, 2));
        if (distance <= 1000) {
          areasInSector.add(area);
        }
    }
}