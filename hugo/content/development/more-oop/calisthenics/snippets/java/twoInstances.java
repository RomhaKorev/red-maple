public class Aircraft {
    public UUID id;
    public Vector3D position;
    public Vector3D speed;
    public String type;
  
    public Aircraft(UUID id, Vector3D position, Vector3D speed, String type) {
      this.id = id;
      this.position = position;
      this.speed = speed;
      this.type = type;
    }
  }