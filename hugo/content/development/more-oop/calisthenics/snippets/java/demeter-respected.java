if (aircraft.velocity() > 2000)
    return ThreatLevel.HIGH;
//...

class Aircraft {
    double velocity() {
        return speed.normalized();
    }
}

class Speed {
    double normalized() {
        return Math.sqrt(Math.pow(x, 2)
        + Math.pow(y, 2)
        + Math.pow(z, 2));
    }
}