public ThreatLevel getThreatLevelFor(Aircrafts aircrafts) {

    return aircrafts.transform(aircraft -> getThreatLevelFor(aircraft))
                    .fold( globalThreat, threat -> globalThreat.compare(threat), ThreatLevel.LOW);

}