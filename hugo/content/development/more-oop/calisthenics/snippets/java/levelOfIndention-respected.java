public String getThreatLevelFor(List<Aircraft> aircraftList) {
    if (!aircraftList.isEmpty()) {
        return getHighestLevelFor(aircraftList);
    } else {
        return "NO THREAT";
    }
}

private String getHighestLevelFor(List<Aircraft> aircraftList) {
    String highestThreat = "LOW";
    for(Aircraft aircraft: aircraftList) {
        highestThreat = getHighestLevelBetween(aircraft, highestThreat);
    }
    return highestThreat;
}

private String getHighestLevelBetween(Aircraft aircraft, String highestThreat) {
    if (a != null) {
        String currentThreat = getThreatLevelFor(a);
        return compare(currentThreat, highestThreat);
    } else {
        return highestThreat
    }
}

private String compare(String left, String right) {
    if (left.equals("HIGH")) {
        return left;
    } else if (right.equals("LOW") && left.equals("MEDIUM")) {
        return left;
    }
    return right;
}