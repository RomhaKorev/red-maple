private ThreatLevel compare(ThreatLevel left, ThreatLevel right) {
    if (left == ThreatLevel.HIGH)) {
        return left;
    } else if (right == ThreatLevel.LOW && left == ThreatLevel.MEDIUM) {
        return left;
    }
    return right;
}