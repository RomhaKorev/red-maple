public class Aircraft {
    public UUID id;
  
    public Aircraft(UUID id) {
      this.id = id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }
}