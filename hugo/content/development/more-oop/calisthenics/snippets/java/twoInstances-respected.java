public class Aircraft {
    private AircraftId id;
    private Coordinates coordinates;
}

class AircraftId {
    private UUID id;
    private String type;
}

class Coordinates {
    private Position position;
    private Velocity velocity;
}

class Position {
    private GeodeticSystem coordinates;
    private Altitude altitude;
}

class GeodeticSystem {
    private Phi latitude;
    private Lambda longitude;
}

