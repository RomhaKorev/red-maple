public String getThreatLevelFor(List<Aircraft> aircraftList) {
    if (!aircraftList.isEmpty()) {
        String highestThreat = "LOW";
        for (Aircraft a : aircraftList) {
            if (a != null) {
                String currentThreat = getThreatLevelFor(a);
                if (currentThreat.equals("HIGH")) {
                    highestThreat = currentThreat;
                } else if (highestThreat.equals("LOW") && currentThreat.equals("MEDIUM")) {
                    highestThreat = currentThreat;
                }
            }
        }
        return highestThreat;
    } else {
        return "NO THREAT";
    }
}