Student grace = new Student(new Email("grace.slick@foo.bar"), "Grace Slick", firstYearPath)
Student graceIn2ndYear = new Student(new Email("grace.slick@foo.bar"), "Grace", firstYearPath)
Student angus = new Student(new Email("angus.young@foo.bar"), "Angus Young", firstYearPath)

assertThat(grace).isEqualTo(graceIn2ndYear) // True
assertThat(grace).isNotEqualTo(angus) // False