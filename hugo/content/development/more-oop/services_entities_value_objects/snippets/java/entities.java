class Student {
    private final Email email;
    private final String name;
    private List<Module> followedModules;
    
    public Student(Email email, String name, List<Module> followedModules) {
        this.email = email;
        this.name = name;
        this.followedModules = followedModules;
    }

    public boolean equals(Object other) {
        if (!(other instanceof Student)) return false;
        Student s = (Student) other;
        return this.email.equals(s.email);
    }
}