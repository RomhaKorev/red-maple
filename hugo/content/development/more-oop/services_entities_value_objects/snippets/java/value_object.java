class Complex {
 
    private final double real, imaginary;
 
    public Complex(double real, double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }
 
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Complex)) return false;        
        Complex c = (Complex) other; 
        return this.real == c.real && this.imaginary == c.imaginary;
    }
}

Complex value1 = new Complex(9, 4);
Complex value2 = new Complex(9, 4);
Complex value3 = new Complex(2, 3);

value1.equals(value2); // TRUE
value1.equals(value3); // FALSE