Student grace = new Student(new Email("grace.slick@foo.bar"), "Grace Slick", firstYearPath)
Student angus = new Student(new Email("angus.young@foo.bar"), "Angus Young", firstYearPath)

assertThat(grace).isNotEqualTo(grace) // True
assertThat(grace).isNotEqualTo(angus) // False