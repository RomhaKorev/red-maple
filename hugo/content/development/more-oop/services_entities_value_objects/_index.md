+++
title = "Value Objects & Entities"
subtitle= "Object Oriented Programming"
outputs = ["Reveal"]
authors="Benoit David, Dimitry Ernot"
+++

{{% introduction %}}

---
{{% sectionbreak "State, Identity & Behavior" %}}
{{% /sectionbreak %}}

{{% note %}}
Un petit disclaimer avant de commencer. Ici, quand on parlera d'objet, on ne parlera pas d'instance comme on l'entend dans le code (qu'on crée avec new tout ça). Mais d'objets au sens du système qui a une durée de vie sans doute supérieure aux instances qu'on va créer. Par exemple, quand on sauvegarde un objet en base, on pourra détruire l'instance mais l'objet en lui-même existera toujours à l'échelle du système.
{{% /note %}}

---
## State
The current values of all properties (variables & constants) inside the object.

****A stage in the lifecycle of the object****

- the speed, mileage, color, gaz tank level, etc. of a car
- The currency and amount of a coin

{{% snippet "state" %}}

{{% note %}}
L'état d'un objet est déterminé par l'ensemble de ses propriétés à un instant donné.
Par exemple, pour une voiture, on va avoir sa vitesse, actuelle, sa couleur, son niveau de carburant, etc.
Pour un étudiant, son nom, sa moyenne, son planning

Certaines propriétés sont invariables. Il y a peu de chance que le nom d'un étudiant change. Par contre, sa moyenne, oui.
Ce qui veut dire que l'état d'objet va évoluer dans le temps.
{{% /note %}}

---
## State and behavior
The internal (private) attributes may be modified
- Only the state from the external point of view should not change
- For example : using caching for expensive computation (memoization)

{{% note %}}
Une précision sur l'état d'un objet. On ne prend en considération que les propriétés visibles depuis l'extérieur. Ou du moins, ce qu'on peut connaître de l'objet.
Si on a besoin de définir des attributs privés pour se simplifier la vie, faire du cache, etc. Ils ne rentreront pas dans la définition de l'état. En gros, on ne prend en compte que les attributs qui représentent une donnée fonctionnelle de l'objet.
{{% /note %}}

---
## Immutability
The state of some object should not be modified during their lifecycle:
- A €10 note cannot become a €20 note


{{% note %}}
Pour certains objets, leur état ne peut pas varier dans le temps. Par exemple, pour un billet de 10€, son état est défini par sa device (l'euro) et son montant (10). A aucun moment, un billet de 10€ ne changera ni de devise, ni de montant.

Ces objets sont considérés comme immuables. C'est à dire qu'on garantit qu'il n'y a aucun moyen de modifier son état. Si on veut le modifier, c'est qu'on veut en réalité créer un autre objet.

Un exemple courant : la classe String. Une fois qu'on a créé une String, on ne la modifie pas. Chaque fois qu'on chercher à la passer en majuscule, la couper, etc. on obtient une nouvelle String.
{{% /note %}}


---
## Immutability: Copy on write

Modifying an immutable object means creating a new one:

- Putting a string in uppercase returns a new string
- Adding two numbers gives a new number

An immutable object is ****thread-safe****:
- no need to protect access to an immutable object
- Can be used by reference

---
## Identity

What that make the object uniquely identifiable
- Their memory address
- The serial number of a phone
- the couple name & social security number of a person
- the address (number, street & city) of a house

{{% note %}}
Un objet possède toujours un moyen de l'identifier. C'est-à-dire de le distinguer d'un autre.

On va se baser sur certaines propriétés. Par exemple, identifier un utilisateur par son adresse mail. Une maison par son adresse postale. Un ordinateur par son adresse IP, etc.

Ce qui nous permet de distinguer deux objets de la même classe, est ce qu'on appelle son identité.

{{% /note %}}

---
## Behavior

All the interactions exposed by an object (its public methods)

- A TV can change channels
- A car can accelerate, stop, start, etc.
- A plane can take off, give its altitude

{{% note %}}
Ce qu'on appelle comportement d'un objet n'est autre que la façon dont on communique avec lui et ce qu'il est capable de faire.

Et derrière tout ça, on peut se dire que, finalement, on n'a pas forcément besoin de connaître l'objet plus que ses fonctions dont on a besoin, qu'on utilise.

Par exemple, si on reprend le kata Mars Rover, le système qui envoie les commandes au rover se soucie peu de savoir si elle les envoie à un mars rover ou à autre chose. Tant que cet autre chose possède les méthodes avancer, reculer, tourner à gauche ou à droite.
{{% /note %}}

---
{{% sectionbreak "Value Objects" %}}
{{% /sectionbreak %}}

---
## Value objects

Objects that don't have identity but only a state

A value object represents a concrete value:
- 2 euros
- the color blue
- a postal address
- a phone number

A value object is ****immutable*****

{{% note %}}
Certains objets sont uniquement différenciables sur l'ensemble de leur champs.
Par exemple, deux pièces de monnaies sont différenciables sur le devise et leur montant.
Et, deux pièces de 2€ sont identiques pour nous. Si je prête 2€ et qu'on me rend 2€, peu importe que ce ne soit pas la même pièce, je m'en fiche

Ca veut dire que certains objets n'ont pas d'identité propre, ou du moins, que leur identité est déterminée par leur état.

On parle alors de Value Object. Des objets dont le seul intérêt est de représenter une valeur : un montant, une couleur, une adresse postale, etc.

{{% /note %}}

---
## Equality between Value objects

Two value objects are equal if their states are the same

- Creating two instances with the same values leads to consider them as the same object
- So, we can use the same object everywhere to represent a value

****A value object does not have a lifespan****

{{% note %}}
Si deux instances représente le même value object dès lors que leur état est identique. Ca veut dire qu'on peut créer autant d'instances qu'on veut pour représenter le même objet (ou une seule si on veut)

Ce qui veut surtout dire qu'on va créer un value object et le détruire aussi souvent que l'on veut. Il ne change pas d'état et n'a donc pas de cycle de vie
{{% /note %}}


---
{{% sectionbreak "Entities" %}}
{{% /sectionbreak %}}

---
## Entities
Objects which have an identity

- Two employees cannot have the same email address
- Two phones cannot have the same serial number
- Two network connection handlers have different memory addresses

****An entity has a lifecycle****

{{% note %}}
Les entités modélisent des objets qu'on différencie par rapport non pas à leur état mais par rapport à leur identité.
Par exemple, deux employés sont différenciables par leurs adresse email. Une commande par son numéro, un patient par son numéro de sécurité sociale, etc.

Les entités sont parfaites pour représenter des objets uniques.

Elles ont leur propre cycle de vie qui va dépendre des opérations que l'on va appliquer. Et, quand on crée une nouvelle entité, c'est pour représenter un tout nouvel objet.

{{% /note %}}

---
## Equality between Entities based on reference

A entity can be identified by its pointer memory address (its reference)

{{% snippet "entities_reference" %}}
{{% snippet "entities_reference_tests" %}}

{{% note %}}
La façon naturelle de distinguer deux entités est de se baser sur leur adresse mémoire : deux objets créés en mémoire ont forcément des adresses différentes.
{{% /note %}}


---
## Equality between Entities based on identity

The equality is based on the properties forming the identity 

{{% snippet "entities" %}}
{{% snippet "entities_reference_equality_tests" %}}

{{% note %}}
Mais souvent ça ne suffit pas. Surtout dans des systèmes complexes avec persistence des données en base ou potentiellement plusieurs flots d'exécution parallèles
On va donc utiliser des attributs. Autrement l'identité de l'objet.
{{% /note %}}

---
## Entity or Value Object

{{%columns%}}
{{%column%}}
****A value object****
- For anyone
- two notes are identical
- Identified by its currency and amount
{{%/column%}}
{{%column%}}
****An entity****
- For the bank
- Identified by the serial number
- Two 20 euros notes are strictly different
{{%/column%}}
{{%/columns%}}
![20 EUR note](img/eur20.png)


{{% note %}}
Pour savoir quand utiliser une entité ou un value object.
Il faut se demander ce qui permet de différencier deux objets et leur cycle de vie.

Si un objet n'a pas d'identité propre et qu'on se fiche bien de savoir quelle instance on manipule dès lors qu'elle a la bonne valeur, alors, on a besoin d'un value object.
Si on a besoin de manipuler un objet précis et peu importe qu'il ait le même état qu'un autre, il reste unique (deux personnes ayant le même âge, taille et poids ne sont pas confondables pour autant). Alors c'est une entité.

Par contre, un objet peut être à la fois un value object dans un bout du système et une entité dans un autre contexte.
Par exemple, deux billets de 20€ pour vous et moi, ce sont les mêmes et on ne fait pas la différence. Voire ça ne nous intéresse pas de faire la différence.
Mais pour la banque de France, chaque billet est unique et c'est important pour eux de considérer les deux billets de 20€. Pour savoir lequel détruire, savoir si l'un a été falsifié, etc.

Du coup, un billet de 20€ est un value object pour nous et une entité pour la banque de France. C'est la façon dont on va modéliser nos objets qui va déterminer leurs usages. Et c'est pourquoi on se retrouvera très certainement avec deux classes qui modélisent un billet dans deux coins de notre code. Et, ce n'est pas grave parce qu'on sait maintenant qu'il y a une très bonne raison à ça

{{% /note %}}

---
{{% conclusion %}}