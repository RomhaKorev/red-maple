+++
title= "Java Stream API"
outputs = ["Reveal"]
subtitle="Introduction to Java Stream API"
authors="Matthieu Archez, Igor Fonkou-Tague, Dimitry Ernot"
draft = true
+++


{{% introduction %}}

---
## Java 8 & Stream API

- It is common to manage collections (list, map, arrays, etc.)
- It is common to iterate through a collection

{{% snippet "stream-example" %}}

- The iteration is made manually outside the collection class (external iteration)
    - The external iteration will generate more code and will make the logic harder to understand


---
## Java 8 & Stream API

- Stream will allow us to manipulate object in a collection as a data stream through functional operation
    - The user can focus on the process
    - The user does not have to manage the iteration
- Stream will introduce:
    - Lazyness: data are evaluated when needed
    - Short-circuiting: stopping an operation before its end
    - Parallelization
    - Internal iteration


---
## Java 8 & Stream API

{{% columns %}}
{{% column %}}
{{% snippet "stream-example-1" %}}
{{% /column %}}
{{% column %}}
{{% snippet "stream-example-2" %}}
{{% /column %}}
{{% /columns %}}

- Stream is made to chain operation (method chaining design pattern)
    - Each operation will execute a task (filtering, mapping, sorting, etc.)
- It will use the lambda expression and built-in methods to describe the operations
- Stream is mostly generic but some specialized streams can be used for specific operations


---
## Java 8 & Stream API

- Intermediate operation: will return a stream and can be chained
    - Filter, map, distinct, sorted, etc.
- Terminal operation: will produce something else than stream
    - reduce, min/max, find, count, etc.


---
## Filtering

- Filtering a list with `filter`
    - **Keep** the element in the stream if the expression is evaluated to **true**
- Filtering duplicate with `distinct`
    - Uses the method `equals` for the comparison
- Limiting the number of items with `limit`
- Skipping items with `skip`


---
## Filtering

{{% snippet "filtering" %}}

---
## Mapping

- Converting items to something else with `map`:
    - **Converts** the items through the expression (must return a value)
- Converting items to a stream of something else with `flatMap`
    - **Converts** the items through the expression (must return a stream)


---
## Mapping

{{% snippet "mapping" %}}


---
## Searching

- `anyMatch` returns **true** if the expression is evaluated to **true** for ****at least one**** item
- `allMatch` returns **true** if the expression is evaluated to **true** for ****all**** items
- `noneMatch` returns **true** if the expression is evaluated to **false** for ****all**** items
- `findAny` returns an **optional** containing an item for which the expression is true
- `findFirst` returns an **optional** containing the first item for which the expression is true


---
## Collecting items

- Producing a result will close the stream (terminal operation)
    - No new operation can be chained after that
- Producing a single result from a stream with:
    - `reduce`: Combines items one by one to produce a single result as an `optional`
    - `collect`: transforms the stream to a data structure (list, map, array, etc.)
    - `forEach`: Applies an expression to each item one by one

{{%columns %}}
{{%column %}}
{{% snippet "collecting-1" %}}
{{% /column %}}
{{% column %}}
{{% snippet "collecting-2" %}}
{{% /column %}}
{{% /columns %}}


---
## Sorting

- Sorting items with sorted:
    - Without parameter: uses the natural order of the items
    - With a parameter: uses a `Comparator`

{{% columns %}}
{{%column%}}
{{% snippet "sorting-1" %}}
{{%/column%}}
{{%column%}}
{{% snippet "sorting-2" %}}
{{%/column%}}
{{% /columns %}}


---
{{% conclusion %}}