Stream.of("foo", "bar")

        .map(value -> value.toUpperCase()) // FOO, BAR
        
        .collect(Collectors.toList());     // [FOO, BAR]