Stream.of("foo", "bar")

.map(value -> value.toUpperCase())                // FOO, BAR

.flatMap( value -> Arrays.stream(value.split("))) // F, O, O, B, A, R

.collect(Collectors.toList());