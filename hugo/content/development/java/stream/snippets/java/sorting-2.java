Stream.of("baz", "foo", "bar", "foobar", "spam")
.sorted(
    Comparator.comparingInt(String::length)
    .thenComparing(Comparing.naturalOrder())
    .reversed()
)
.collect(Collectors.joining(", "));
// foobar, spam, foo, baz, bar