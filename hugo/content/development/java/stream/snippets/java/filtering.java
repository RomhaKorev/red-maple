Stream.of(0, 1, 2, 2, 3, 4, 4, 5, 6, 7, 8)
.filter(n -> n % 2 == 0) // 0, 2, 2, 4, 4, 6, 8
.distinct()              // 0, 2, 4, 6, 8
.limit(4)                // 0, 2, 4, 6
.skip(1)                 // 2, 4, 6
.collect(Collectors.toList());