List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
for (Integer number: numbers) {
    if (number % 2 == 0) {
        System.out.println(number);
    }
}