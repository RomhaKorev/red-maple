int sumOfEvenNumbers = numbers.stream()
    .filter(value -> value % 2 == 0)
    .sum();