numbers.stream()
    .filter(value -> value % 2 == 0)
    .forEach(System.out.println);