Stream.of("baz", "foo", "bar", "foobar", "spam")
.sorted(
    Comparator.comparing(String::toUpperCase)
)
.collect(Collectors.joining(", "));
// bar, baz, foo, foobar, spam