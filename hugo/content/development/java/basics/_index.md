+++
title= "How to start in Java"
outputs = ["Reveal"]
subtitle="Introduction to Java"
authors="Dimitry Ernot"
+++


{{% introduction %}}

---
## Java

- High-level programming language
- Developped by Sun Microsystems in 1995
- Multiplatform : Windows, MacOs & UNIX
    - the code is compiled in bytecode to be independent to the platform
    - and runned by the Java Virtual Machine
- Object Oriented language

{{% floatingimage "img/java-logo.png" "java-logo" %}}

---
## Syntax & Coding Rules

- Case sensitive(`myObject` ≠ `myobject`)
- A class should start with a capital letter (`MyClass`)
- A method should start a lowercase (`doSomething()`)
- One class per file
- The filename should match the name of its class (the class `MyClass` should be `MyClass.java`)
- Every method, variable constant, etc. has to be declared (and used) inside a class
- The entrypoint of every Java application: `public static void main(String args[])`

---
## A first program

{{% snippet "first-program" %}}

---
## Comments

{{% snippet "comments" %}}

---
## Variables

{{% snippet "variables" %}}

---
## Constants

{{% snippet "constants" %}}

---
## Primitive Types

{{% snippet "primitive-types" %}}

---
## Non-Primitive Types

{{% snippet "non-primitive-types" %}}

---
## Operators

{{% columns stretch="true" %}}
{{% column %}}
{{% bracket width=570 height=170 title="Arithmetic operators" %}}
Addition: ****`+`**** \
Substraction: ****`-`**** \
Division: ****`/`**** \
Multiplication: ****`*`****
{{% /bracket %}}
{{% /column %}}

{{% column %}}
{{% bracket width=570 height=170 title="Assignment operator" %}}
****`=`****
{{% /bracket %}}
{{% /column %}}
{{% /columns %}}

{{% vspace %}}

{{% columns stretch="true" %}}
{{% column %}}
{{% bracket width=570 height=170 title="Logical operators" %}}
And: ****`&&`**** \
Or: ****`||`**** \
Not: ****`!`****
{{% /bracket %}}
{{% /column %}}

{{% column %}}
{{% bracket width=570 height=170 title="Comparison operators" %}}
Equality: ****`==`****, ****`!=`**** \
Greater: ****`>`****, ****`>=`**** \
Lesser: ****`<`****, ****`<=`**** \
{{% /bracket %}}
{{% /column %}}
{{% /columns %}}

---
## Strings

{{% snippet "string" %}}

---
## Arrays

{{% snippet "arrays" %}}

---
## Conditions

{{% snippet "conditions" %}}

---
## Switch

{{% snippet "switch" %}}

---
## Loops: While

{{% snippet "while" %}}


---
## Loops: For

{{% snippet "for" %}}

---
## Loops: For each

{{% snippet "foreach" %}}


---
## Functions

{{% snippet "functions" %}}

---
## Functions: Parameters

{{% snippet "functions-parameter" %}}

---
## Functions: Return Type

{{% snippet "functions-return-type" %}}


---
## Exceptions

{{% snippet "exceptions" %}}
