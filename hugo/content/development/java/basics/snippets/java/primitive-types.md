- integers: `byte` (1B), `short` (2B), `int` (4B) et `long` (8B)
- decimals: `float`, `double`
- booleans: `boolean`
- characters: `char`
