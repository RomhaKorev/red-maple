- A exception is thrown with `throw`
  - Commonly when an error occurs
- An exception breaks the code flow
  - until a block catches it with a `try / catch`