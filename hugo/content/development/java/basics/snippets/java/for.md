- A convenient alternative to the `while` syntax
- Three statements:
  - Initialisation: executed only once before running the loop
  - Predicate: the condition to continue the loop
  - Increment: executed at each loop after the block
