if(myFirstPredicate) {

    System.out.println("this predicate is true");

} else if (mySecondPredicate) {

    System.out.println("the first predicate is false and the second one is true");

} else {

    System.out.println("both predicates are false");
}