
void watchAdultMovie() {
    if (age < 18) {
        throw new IllegalOperation();
    }
}


try {
    watchAdultMovie();
} catch(IllegalOperation e) {
    System.out.println("That's illegal...");
} catch (Exception e) {
    System.out.println("Another type of exception");
} finally {
    System.out.println("Will be executed exception or not");
}