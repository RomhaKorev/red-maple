+++
title= "Functional Programming in Java"
outputs = ["Reveal"]
subtitle="Introduction to functional programming in Java"
authors="Matthieu Archez, Igor Fonkou-Tague, Dimitry Ernot"
draft = false
+++


{{% introduction %}}

---
## Functional Programming

- Paradigm that considers computation as evaluation of mathematical expressions
- Declarative paradigm based on expression and declaration instead of statement
- Based on lambda calculus
- Restricted side-effect


---
## Side Effect

- A function with a side effect modify the state of an object outside its local environment
    - It has a observable effect on the world beside returning a value
- Examples:
    - Modifying a non-local variable (static, global, etc. variable)
    - Modifying a parameter passed by reference
    - Printing a message (performing I/O)
    - Using another function with a side effect


---
## Pure function

- **Idempotence**: the result is the same for the same set of parameters
    - `a + b` will always return 4 for a = 1 and b = 3
- The function is **deterministic**: it will not use non-local variables
    - It will not use global variables or system-based mechanism (current time, system file, random number, etc.)
- The function has no **side-effect**


---
## Pure or Impure function?
{{% snippet "pure-function-example-1" %}}
{{% fragment %}}
****It's a pure function****
{{< /fragment >}}


---
## Pure or Impure function?
{{% snippet "pure-function-example-2" %}}
{{% fragment %}}
****It's NOT a pure function****

The variable `c` is not local to the function
{{< /fragment >}}


---
## Pure or Impure function?
{{% snippet "pure-function-example-3" %}}
{{% fragment %}}
****It's **not** a pure function****

`System.out.println` will modify the I/O. It's a side effect
{{< /fragment >}}
