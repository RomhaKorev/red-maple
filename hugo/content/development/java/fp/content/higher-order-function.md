+++
outputs = ["Reveal"]
weight = 1
+++



---
## Higher order function

- Functions that:
    - Will take another function in parameter
    - Will return a function
- Examples:
    - Design pattern Visitor
    - Sort function with a criteria


---
## First-class function

- Functions are treated as first-class citizen:
    - Can be passed as parameter or returned by another function
    - Will return a function
- A first-class function supports all operations applied to other entities (type, object, etc.)


---
## Lambda function

- Anonymous function
- First-class function
- Contains a function & a **scope**

---
{{% sectionbreak "Functional programming applied to Java" %}}
{{% /sectionbreak %}}

---
{{% sectionbreak "Higher order functions in Java" %}}
{{% /sectionbreak %}}


---
## Higher order function in Java

Java doesn’t not allow first-class function

{{% vspace %}}

Two workarounds to pass a function as parameter or to return a function:

- Using an interface and generic class (before Java 8)
    - The function passed in parameter will be replaced by an object
- Using lambda expressions (since Java 8)
    - The lambda expression will execute the function
