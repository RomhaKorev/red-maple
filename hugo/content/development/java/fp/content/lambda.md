+++
outputs = ["Reveal"]
weight = 2
+++


{{% sectionbreak "Lambda functions in Java" %}}
{{% /sectionbreak %}}


---
## Lambda in Java

- Since Java 8. One the most important features of Java
- Allows to pass an operation as parameter
- The treatment is explicit. The way it will be applied is implicit
- The lambda feature allows us to:
    - pass a function that will respect the Consumer interface
    - Pass a reference to a function


---
## Lambda in Java

Focusing on *how you are printing the list*
{{% snippet "lambda-example-1" %}}

{{% vspace %}}

Focusing on *what your are printing*
{{% snippet "lambda-example-2" %}}


---
## Lambda expression in Java

- A lambda expression is an anonymous function
- Very useful when an operation is local and used only once
- The compiler will infer the interface of the lambda expression
- A lambda expression uses the operator `->`


---
## Lambda expression in Java

{{% snippet "lambda-example-4" %}}


- The parameter are declared between brackets followed by the operator `->`
- They have to match with the interface used by the caller
- The type of each parameter can be explicitly set. But mandatory if the compiler cannot infer them
- A lambda expression can have 0 parameter. The brackets will be empty, then
- The brackets are optional if there is only parameter and its type is not set


---
## Lambda expression in Java

{{% snippet "lambda-example-5" %}}

- The body can be:
    - A single expression
    - A block between curly brackets
- The returned value is the state of the last instruction
    - The keyword return will refer to the parent function, not to the lambda expression


---
## Lambda expression in Java

- An expression lambda does not have explicit return type
- The returned value is the state of the last instruction
- The keyword return can be used to define the returned value if it is ambiguous
- If the return type cannot be infered by the compiler, it will raise an error


---
## Lambda expression in Java

{{% snippet "lambda-expression-6" %}}

- A lambda expression can be stored in a variable
    - The type is based on the interface matching the lambda
- It can be manipulated as an object


---
## Lambda expression in Java

{{% snippet "lambda-expression-7" %}}
{{% snippet "lambda-expression-8" %}}

- We can create an interface to match the lambda using the annotation `@FunctionalInterface`
- Most common lambda interfaces are defined in the package `java.util.function`


---
{{% conclusion %}}