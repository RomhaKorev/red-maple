@FunctionalInterface
interface MyFunction<One, Two, Three, R> {
    R apply(One p1, Two p2, Three p3);
}