+++
weight = 1
+++

{{% sectionbreak "How to" %}}
{{% /sectionbreak %}}

---
## How to create a class
{{< snippet "class-declaration" >}}

---
## How to create an object

{{< snippet "object-instance" >}}


---
## How to create a method
{{< snippet "method-declaration" >}}

---
## How to create an attribute
{{< snippet "attribute-declaration" >}}

---
## How to create a constructor
{{< snippet "constructor-declaration" >}}