+++
weight = 3
+++


{{% sectionbreak "Abstraction" %}}
{{% /sectionbreak %}}

---
## Abstract classes
{{< snippet "abstract-classes" >}}


---
## Abstract methods

{{< snippet "abstract-methods" >}}

---
{{% sectionbreak "Interfaces" %}}
{{% /sectionbreak %}}

---
## Declaring an interface in Java
{{< snippet "interface" >}}


---
## Using an interface in Java

{{< snippet "interface-usage" >}}


---
## Interface as type in Java

{{< snippet "interface-type" >}}