+++
weight = 2
+++

{{% sectionbreak "Encapsulation" %}}
{{% /sectionbreak %}}

---
## Visibility
- public: everybody can access to public members
- private: Only the objects of the same class can access to private members
- protected: Only the objects of the same class or derived class can access to private members
