+++
weight = 4
+++

{{% sectionbreak "Generalization" %}}
{{% /sectionbreak %}}

---
## Overloaded functions

{{< snippet "overloaded-function" >}}

---
{{% conclusion %}}