- A child class can override methods from its parent class
- The parent method will be not called anymore (virtual methods linking)