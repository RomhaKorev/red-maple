abstract class Explorer {
    public Explorer() {
        isMoving = false;
    }

    public abstract void gotTo(Position position);    
    public abstract void stop();
}

