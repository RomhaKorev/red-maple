class Robot {
    private Position currentPosition;

    public void moveForward() {
        currentPosition = computeNextPosition();
    }
}

class Rover extends Robot {
    public void moveForward() {
        System.out.println("Moving forward");
    }
}