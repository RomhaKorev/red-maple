interface Flying {
    void land();
    Boolean takeOff();
}