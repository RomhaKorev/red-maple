+++
title= "Generic types in Java"
outputs = ["Reveal"]
subtitle="Introduction to templates"
authors="Dimitry Ernot"
+++

{{% introduction %}}

---
## What's a generic

- Abstracting coherent concepts to make it generic
- Algorithms and data structures are written in terms of types *to-be-specified-later*
- The code becomes a generic model


---
## Principles

- Generic classes
    - To specify the type of an attribute
- Generic functions
    - To specify the type of a parameter or the return type


---
## Generic class or function

- Has a partial definition
    - The type of an attribute or parameter is unknown
- Has to be specified during the instantiation
- Is checked by the compiler
    - So, during the compile time

---
{{% sectionbreak "Template functions" %}}
{{% /sectionbreak %}}

---
Problem: we have to write the same function twice if we want the same behavior for `Integers` and `Doubles`
{{< snippet "1" >}}


---
A generic function defines a template `T` that can be used as a type
{{< snippet "2" >}}


---
A method can specify several generic types for its parameters
{{< snippet "3" >}}


---
A method can specify its return type
{{< snippet "4" >}}


---
{{% sectionbreak "Template classes" %}}
{{% /sectionbreak %}}


---
A generic class could be useful when the type of our attribute *doesn't matter*
{{< snippet "6" >}}

****We focus on the behavior****

---
## Type inference

When the compiler identifies the right signature for our template
{{< snippet "5" >}}

---
{{% sectionbreak "Common behavior & generic class" %}}
{{% /sectionbreak %}}

---
## Upper Bound

- Specifies that all types should extend an upper bound
- Defines the common behavior of all the concretions

{{% snippet "upperbound" %}}

---
## Covariance

- A specification of a generic type creates a new type
- Two instanciations are not compatible even between subtype & supertype

{{% snippet "covariance-failure" %}}

---
## Covariance & Wildcard

- The wildcard `?` creates a supertype of all the generic type concretions

{{% snippet "covariance" %}}

---
## Lower Bound

- The wildcard `?` can be specified with a lower bound

{{% snippet "lowerbound" %}}


---
{{% conclusion %}}