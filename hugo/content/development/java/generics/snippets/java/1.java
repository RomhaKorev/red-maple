class A {
    void thisIs(Integer a, Integer b) {
        System.out.println("This is a: " + a + " and this is " + b);
    }

    void thisIs(Double a, Double b) {
        System.out.println("This is a: " + a + " and this is " + b);
    }

    public static void main(String[] args) {
        A obj = new A();
        obj.thisIs(1, 2);
        obj.thisIs(2.4, 1.8);
    }
}