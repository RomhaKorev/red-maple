class Bike implements Parkable {}
class MountainBike extends Bike {}

public static void main(String[] args) 
    Garage<Parkable> vehicles = new Garage<>();
    
    Garage<Bike> bikes = new Garage<>();

    Garage<MountainBike> mountainBikes = new Garage();

    Garage<? super Bike> either = bikes;
    
    either = vehicles;
    either = mountainBikes; // Error at compile time
}