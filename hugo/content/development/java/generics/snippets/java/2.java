class MyClass {
    public <T> void thisIs(T a, T b) {
        System.out.println("This is a: " + a.toString() + " and b: " + b.toString());
    }

    public static void main(String[] args) {
        MyClass o = new MyClass();
        o.thisIs(1, 2);
        o.thisIs(2.4, 1.8);
    }
}