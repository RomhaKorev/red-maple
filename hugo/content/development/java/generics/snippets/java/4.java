class MyClass {
    public <T> T thisIs(T a, T b) {
        return a;
    }

    public static void main(String[] args) {
        MyClass o = new MyClass();
        o.thisIs(1, 2);
        o.thisIs(2.4, 1.8);
    }
}