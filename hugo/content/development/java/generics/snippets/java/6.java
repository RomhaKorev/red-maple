class Container<T> {
    private T value;
    public Container(T value) {
        this.value = value;
    }

    public T get() {
        return value;
    }

    public static void main(String[] args) {
        Container<Integer> a = new Container<>(12); // Integer
        Container<String> b = Container<>("abc"); // String
    }
}