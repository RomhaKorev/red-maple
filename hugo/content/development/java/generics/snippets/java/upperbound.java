interface Parkable {
    void park(Place place);
    Place isParkedIn();
}

class Garage<T extends Parkable> {
    private List<T> vehicles = new ArrayList();
    private Places places = new Places();

    public void park(T vehicle) {
        Place available = findFreePlace();
        vehicle.park(available);
    }

    public Optional<Place> find(T vehicle) {
        return vehicles.stream()
                .filter(v -> v == vehicle)
                .map(v -> v.isParkedIn())
                .findFirst();
    }
}