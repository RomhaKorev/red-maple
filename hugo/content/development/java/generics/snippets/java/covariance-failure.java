public class Bike implements Parkable {}

public static void main(String[] args) {
    Garage<Bike> bikes = new Garage<>();
    Garage<Parkable> garage = bikes; // Error at compile time
}