class MyClass {
    public <T, U> void thisIs(T a, U b) {
      System.out.println("This is a: " + a.toString() + " and b: " + b.toString());
    }
  
    public static void main(String[] args) {
      MyClass o = new MyClass();
      o.thisIs(1, "abc");
    }
  }
  