+++
tile="Basics"
title= "An introduction to kotlin"
outputs = ["Reveal"]
subtitle="For those who already know another language"
authors="Dimitry Ernot, Igor Fonkou Tague"
draft=true
+++


{{% introduction %}}


---
## Hello World

{{< inline language="kotlin" tokens="fun">}}
fun main() {

    println("Hello World!)
}
{{< /inline >}}

- ****`fun`**** keyword defines a function

---
## Hello World

{{< inline language="kotlin" tokens="main,()">}}
fun main() {

    println("Hello World!)
}
{{< /inline >}}

- `fun` keyword defines a function
- ****`main`**** is the entry point for a Kotlin program


---
## Variables

{{< inline language="kotlin" tokens="var">}}
var name: Type = value
{{< /inline >}}

- ****`var`**** declares a new variable

---
## Variables

{{< inline language="kotlin" tokens=": ,Type">}}
var name: Type = value
{{< /inline >}}

- `var` declares a new variable
- Variables are ****statically & strongly typed****

---
## Variables

{{< inline language="kotlin" tokens="= ,value">}}
var name: Type = value
{{< /inline >}}

- `var` declares a new variable
- Variables are strongly typed
- Variables ****must be initialized during declaration****

---
## Variables

{{< inline language="kotlin" tokens="val">}}
val name: Type = value
{{< /inline >}}

- Use ****`val`**** for variables assigned only once







