class Drone implements Flying {
    ...
}

public class Main {
    private static void land(Flying ufo) {
        ufo.land();
    }

    public static void main(String[] args) {
        Drone ingenuity = new Drone();

        land(ingenuity);
    }
}