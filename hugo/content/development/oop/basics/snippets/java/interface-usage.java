interface Flying {
    void play();
}

class Drone extends Robot implements Flying, Driving {
    public void land() {
        ...
    }

    public void takeOff() {
        ...
    }
}