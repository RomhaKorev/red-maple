- A new object is created with the `new` operator following by the constructor
- ****`new`**** will create an instance that can be referenced by a variable
- An object will be automatically destroyed by the Garbage Collector *at the right time*