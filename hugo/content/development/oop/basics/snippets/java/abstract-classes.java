abstract class Explorer {
    public Explorer() {
        isMoving = false;
    }

    public void gotTo(Position nextPosition) {
        isMoving = true;
    }
    
    public void stop() {
        isMoving = false;
    }

    private Boolean isMoving;
}

