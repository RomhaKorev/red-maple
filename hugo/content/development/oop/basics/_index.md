+++
title= "OOP starter pack"
outputs = ["Reveal"]
subtitle="Key concepts"
authors="Dimitry Ernot"
draft = false
+++

{{% introduction %}}

---
{{% sectionbreak "Object-Oriented programming" %}}
Definitions & key concepts
{{% /sectionbreak %}}

---
## Object-Oriented programming

- New Paradigm created in 60s
- Defines the notion of objects that interact

{{% vspace %}}

{{% bracket title="By definition, components will be" %}}
  - Based on the real world
  - Reusable
  - Replaceable
{{% /bracket %}}

{{% note %}}
La POO consiste à penser son code sous la forme d'objets qui interagissent au travers d'actions et d'échanges de messages. Ces objets seront une modélisation du monde. Ils prendront la forme d'objets du monde réel -- une voiture, une personne --, de concepts ou d'idées moins tangibles -- une adresse mail, un numéro de téléphone, une connexion réseau. Pour faire simple, si nous pouvons mettre un nom sur ce que nous manipulons, nous pouvons le représenter sous la forme d'un objet.
{{% /note %}}

---
## OOP quickly explained
Focusing on what we want to do instead of how we have to do
{{% vspace %}}
{{% columns %}}
{{% column %}}
{{< fragment >}}
****Abstraction**** \
Actions and control expression
{{< /fragment >}}

{{< fragment >}}
****Encapsulation**** \
Hiding details of implementation
{{< /fragment >}}
{{% /column %}}
{{% column %}}

{{< fragment >}}
****Inheritance**** \
Behavior specialization
{{< /fragment >}}

{{< fragment >}}
****polymorphism**** \
Different kind of objects through the same behavior
{{< /fragment >}}
{{% /column %}}
{{% /columns %}}


{{% note %}}
Tout l'objectif de la POO est que notre code reflète ce que nous voulons faire plutôt que comment il faut le faire. Le quoi plutôt que le comment.

Une rapide présentation des 4 grands sujets de l'OOP. Nous entrerons plus en détails tout au long de ce cours.

On va chercher à prendre un peu de hauteur et mieux exprimer notre intention dans notre code. C'est-à-dire d'abstraction

Par exemple, on se concentrera sur le fait qu'une voiture peut accélérer ou freiner. Et non pas sur comment une voiture augmente ou réduit sa vitesse.


L'abstraction va introduire une notion importante en POO : l'encapsulation. Eviter que les détails d'implémentation (le comment) soient connus pour qu'on puisse les modifier sans impacter quoi que ce soit : si on décide de ne plus enregistrer les étudiants dans un fichier Excel mais dans une base de données, cela ne devrait pas impacter le fonctionnement de l'école. Et, personne n'a besoin de savoir comment l'admnistration gère les étudiants.

Un feutre et un stylo bille ont beau être différents, on peut faire fondamentalement la même chose avec : écrire, retirer le capuchon, regarder le niveau d'encre, etc. Certains comme un surligneur ou un stylo plume permettront d'autres fonctionnalités (surligner, remplir). Pourtant quand on parle de stylos sans préciser exactement le type de stylo, on peut tous imaginer ce à quoi il sert. On va avoir une catégorisation d'objets de même nature avec des principes généraux et d'autres plus spécifiques. On parlera donc d'héritage

Le polymorphisme dans le cadre de la POO est la capacité de gérer des objets différents ensemble dès le moment qu'ils offrent des propriétés communes. On peut mettre des stylos dans une trousse. Mais pas que... Par exemple, on peut y mettre une règle ou un compas. En fait, la trousse se moque complètement de ce qu'on y met tant que les dimensions le permettent.

{{% /note %}}

---
{{% sectionbreak "Class, Object & other fancy words" %}}
{{% /sectionbreak %}}

---
## An object is:

{{% illustrated "img/junk.jpg" "<a href='https://unsplash.com/@margarita_ua?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Margarita Marushevska</a> sur <a href='https://unsplash.com/fr/s/photos/flea-market?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% block "small" %}}
- A physical entity from the real world (bike, student, pen, etc.)
- An idea (email, name, etc.)
- A concept (network connection, movement, position, etc.)

{{% vspace %}}

- Has properties
  - Your speed, temperature, age, address, status, etc.
- Provides a way to interact with it
  - Commands (do something)
  - Queries (give me something)

{{% /block %}}
{{% /illustrated %}}


{{% note %}}
En POO, les objets représentent de tout ce qu'on veut manipuler dans notre code. Ca peut être des objets réels (un stylo, une voiture) ou des concepts plus abstraits comme l'âge d'une personne, la note à un contrôle de maths, etc.

 our faire simple, si nous pouvons mettre un nom sur ce que nous manipulons, nous pouvons le représenter sous la forme d'un objet.

A noter, qu'un objet n'a pas besoin d'avoir une modélisation exhaustive : on se contente de modéliser uniquement ce dont nous avons besoin dans notre application, voire dans **cette** partie de l'application. Quand nous créerons des objets pour représenter un étudiant, nous nous contenterons d'un étudiant avec une adresse mail, une liste de cours, des notes, etc.  En gros, les propriétés minimum dont nous aurons besoin (nos objets étudiants n'auront pas de notion de morphologie, par exemple)
{{% /note %}}


---
## A class

****Defines the common properties of a group of objects****
{{% illustrated "img/architect-plan.jpg" "<a href='https://unsplash.com/@ryanancill?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Ryan Ancill</a> sur <a href='https://unsplash.com/fr/photos/0w1MiTY78h0?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% block "small" %}}
- Template or blueprint of an object
    - That means that an object is associated to a class
- A class is the implementation of our objets
- A non-primitive datatype
{{% /block %}}
{{% /illustrated %}}

{{% note %}}
Pour créer nos objets, nous avons besoin de définir à quoi ils ressemblent : les propriétés qu'ils possèdent, comment ils sont créés, comment on peut interagir avec eux. Ce qu'il est possible de faire, etc.

C'est la classe qui définira tout ça. Une classe est le schéma de conception d'un objet. Tout comme on a des plans d'architecte pour construire un immeuble qui déterminent le nombre de pièces, de fenêtres, la forme du bâtiment, etc. nous aurons des classes qui définiront nos objets, leur fonctionnement, l'implémentation des actions, etc.
{{% /note %}}


---
## An attribute

{{% illustrated "img/gauge.jpg" "<a href='https://unsplash.com/fr/@christianlue?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Christian Lue</a> sur <a href='https://unsplash.com/fr/photos/UZDV43IQNOk?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
- Variable and constant proper to an object
  - the name of an employee or the weight of a package
- Defined by the class
{{% /illustrated %}}
****Each object has its own set of attributes****

{{% note %}}
Nos classes vont commencer par définir les attributs de nos objets. C'est-à-dire quelles les informations seront stockées dans nos objets et comment elles seront représentées. On peut voir les attributs comme des variables locales dans nos objets.

Chaque objet aura son propre set d'attributs. Quand on fera évoluer la vitesse d'un vélo, seul le vélo concerné accélèrera ou freinera. Les autres ne seront pas impactés.
{{% /note %}}

---
## An instance method

{{% illustrated "img/joystick.jpg"  "<a href='https://unsplash.com/ja/@thomasdes?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Thomas Despeyroux</a> on <a href='https://unsplash.com/photos/i_qs6f6y8ag?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
Function that can be applied to an object
  - `accelerate` for a car
  - `isMajor` for a person

Must called on a existing instance

{{% /illustrated %}}

{{% note %}}
La deuxième chose que les classes vont définir, ce sont les méthodes. Les fonctions que nous allons pouvoir appliquer sur un objet afin de l'utiliser et le faire vivre (accélérer/freiner, calculer la moyenne d'un étudiant, etc.)

Les méthodes vont avoir un rôle important pour nos objets : elles vont définir la façon dont on va interagir avec eux.

Leurs noms doivent donc être judicieusement choisis pour porter l'action qu'elles effectuent.
{{% /note %}}

---
## Constructors

{{% illustrated "img/notebook.jpg" "<a href='https://unsplash.com/@aaronburden?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Aaron Burden</a> on <a href='https://unsplash.com/photos/xG8IQMqMITM?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>"%}}
- Routine called when an object is created
-  Initializes an object (sets attributes, reserves resources, etc.)
{{% /illustrated %}}

{{% note %}}
Dernière étape pour nos classes : définir comment on crée nos objets.

Quand un étudiant s'inscrit dans une formation, il doit remplir un formulaire pour renseigner ses informations personnelles, lui demander des documents supplémentaires, etc.


C'est le rôle du constructeur : son but est de construire un nouvel objet à partir des informations fournies (de ses paramètres, en somme)
Le constructeur est une fonction très particulière dans le sens où nous ne l'appelerons pas nous-mêmes : il sera automatiquement appelé lors de l'instanciation d'un nouvel objet.

La façon dont il est nommé et utilisé dépend du langage : le même nom de la classe pour C++ et Java, `constructor` pour Kotlin, `__init__` pour Python, etc.
{{% /note %}}

---
## Constructors
A class can have more than one constructor
- with no argument is the ****default constructor****
- with at least one argument is a ****constructor with parameters****
- where the single argument is an object of the same class is the ****copy constructor****

{{% note %}}
Il peut être utile de pouvoir créér un objet de plusieurs façons différentes. Par exemple, on pourrait se dire qu'un vélo est créé en série (donc tous sur le même modèle). Mais que nous aurions besoin de préciser sa couleur ou autre personnalisation.
{{% /note %}}

---
## A class

{{% columns %}}
{{% column %}}
{{< fragment >}}
****Attributes**** \
All variables inside an object
{{< /fragment >}}

{{< fragment >}}
****Methods**** \
All functions that can be applied to an object
{{< /fragment >}}
{{% /column %}}
{{% column %}}

{{< fragment >}}
****Constructors**** \
How an object is created
{{< /fragment >}}

{{% /column %}}
{{% /columns %}}

{{% note %}}
Pour résumer, une classe va donc définir les attributs, les méthodes (et leurs implémentations) et les constructeurs. Tout ce que permet un objet est donc définit par sa classe.
{{% /note %}}