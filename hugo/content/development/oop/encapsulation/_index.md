+++
title= "Encapsulation"
outputs = ["Reveal"]
subtitle="An introduction to OOP"
authors="Dimitry Ernot"
+++


{{% introduction %}}

---
{{% sectionbreak "Encapsulation" %}}
{{% /sectionbreak %}}

---
## An object is responsible of its own state
{{% illustrated "img/cairn.jpg" "<a href='https://unsplash.com/fr/@karsten116?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Karsten Winegeart</a> sur <a href='https://unsplash.com/fr/photos/JusfYMAUyGs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
- The state is a snapshot of its attributes
- An object must always be in a stable object
- The other objects should use its methods to interact with it
{{% /illustrated %}}

{{% note %}}
Un objet a un état qui définit par ses attributs. C'est-à-dire les valeurs de ses attributs à l'instant T.

Son état va donc évoluer avec le temps. Par exemple, l'âge d'une personne va augmenter, le solde d'un compte banquaire va varier en fonction des retrait et virements, la vitesse d'un avion, etc.

Si on prend un cycliste sur son vélo, avec tout l'entraînement du monde, ce sera difficile de faire du 0 à 100 instantanément. Ou même monter jusqu'à 200km/h. A la fois parce que les lois de la thermodynamique nous l'interdit. Mais aussi parce qu'il y a des contraintes techniques, mécaniques liées au vélo et cycliste lui-même.

Et, c'est notre objet, et lui seul, qui connaît ses contraintes. C'est donc à lui de s'assurer qu'elles sont bien respecter : une personne ne peut rajeunir, un retrait ne peut pas dépasser une certaine somme, un vélo ne peut pas dépasser la vitesse du son, etc.

C'est pour cela qu'on ne peut s'adresser à un objet qu'à travers ses méthodes publiques : parce qu'elles vont imposer l'ensemble des règles qui régissent nos objets. Et, donc assurer que l'état de notre objet est toujours stable et cohérent (une liste triée l'est toujours après insertion d'un nouvel élément)

C'est à l'objet lui-même de s'assurer de la cohérence des valeurs de ses attributs, donc de son état. Par exemple, une voiture ne peut passer de 0 à 100 
{{% /note %}}

---
{{% illustrated "img/blackbox.jpg" "<a href='https://unsplash.com/@gaspanik?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Masaaki Komori</a> on <a href='https://unsplash.com/s/photos/closed?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
****Combining data & behavior****
{{% block "small" %}}
- The state should be inside the object
- An object should only expose its behavior
- The one managing the data should be the only one using it
{{% /block %}}
{{% /illustrated %}}

{{% note %}}
Pour qu'un objet puisse gérer correctement son propre état, le mieux est encore que personne ne puisse le voir et y accéder.

C'est pour ça qu'on dit qu'un objet doit être une boîte noire : on voit comment on peut communiquer avec lui mais pas ce qui se passe en interne. Et, il contrôlera lui-même les données qu'il nous partagera.
{{% /note %}}


---
## Visibility
{{% illustrated "img/access.jpg" "<a href='https://unsplash.com/@sebbb?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>seabass creatives</a> on <a href='https://unsplash.com/photos/U3m4_cKbUfc?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>"%}}
- ****public****: Everybody can access
- ****private****: Only the objects of the same class
- ****protected****: Only the objects of the same class or derived class
{{% /illustrated %}}

The behavior is defined by the public members \
The details of implementation should be private or protected

{{% note %}}
Si je veux empêcher les gens d'entrer dans mon appartement, demander aux gens de ne pas le faire fonctionnera sans doute dans la large majorité des cas. Mais je préfère encore verrouiller la porte à clé.

Si on veut absolument que les autres objets se basent uniquement sur notre interface et n'aillent pas tirer des détails d'implémentation, le mieux est encore de faire en sorte qu'ils ne puissent simplement pas le faire.

Et, pour ça, on va utiliser le mécanisme de visibilité. C'est-à-dire qu'on va pouvoir donner un niveau de visibilité à nos méthodes et attributs.

On a globalement 3 niveaux :
public : tout le monde peut accéder à la méthode. C'est le niveau qu'on donnera à tout ce qui fera partie de notre comportement
privé : seuls les objets de la classe peuvent y accéder. C'est en rendant privé qu'on va masquer les détails d'implémentation comme les attributs, méthodes internes, etc. Tous les objets d'une même classe pourront y accéder cependant. Mais c'est normal : ils connaissent eux-mêmes leur propre implémentation. Ce sera utile pour implémenter une méthode d'égalité, par exemple.
protégé : même principe que privé à la différence qu'on étend la visibilité aux classes enfant. Les classes enfants ont donc accès à leurs propres membres privé mais aussi aux membres protégés de leur classe parent, grand-parent, etc.
{{% /note %}}

---
## Hiding details of implementation
- We don't need to know how it works
- Other devs should not rely on details but abstraction
    - it will be easier to change things

{{% note %}}
Cacher les détails d'implémentation, le comment on fait, permet de rendre le code plus facile à faire évoluer : si personne d'autre que la classe elle-même dépend de cet attribut qui est un entier, il sera facile de le transformer en double sans risque.
Utiliser des méthodes pour interagir avec nos objets permet de rendre explicite les traitements qu'on souhaite faire.
{{% /note %}}


---
## Focusing on the intention and the actions

- We need to know what it does and how to interact
- We should never guess what we are doing

{{% note %}}
Pour conclure sur l'encapsulation, au même titre que l'abstraction, tout est une question d'intention et de ce que nous tentons d'exprimer dans notre code : moins il sera technique et plus il fera appel à des notions fonctionnels, plus il sera facile de le maintenir et de le faire évoluer.
{{% /note %}}

---

*When our car does move fast enough, we don't open the engine to inject more fuel in it. We accelerate.*

---
{{% sectionbreak "Anemic objects" %}}
{{% /sectionbreak %}}


---
**This idea of OOP is combining data and process together**
{{% illustrated "img/dead-tree.jpg" "Photo by <a href='https://unsplash.com/@xtruh_terrestrial?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Juvian Duff</a> on <a href='https://unsplash.com/photos/x_mdcjGHnUI?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a> " %}}
- An object is responsible of its own state
- But also its own implementation
{{% /illustrated %}}

****Anemic object is an anti-pattern****

{{% note %}}
Si un objet est responsable de son propre état, il est aussi responsable de sa propre implémentation. 

Dès lors où un objet est utilisé uniquement pour stocker des données et que tous les traitements qu'on fait avec ses données sont diluées à l'extérieur, l'objet devient anémique. On vient de lui retirer toute sa logique pour n'en faire qu'une simple structure de données. Ce qui va à l'encontre de l'OOP

Le principe d'un objet est de combiner les données (son état) et leur process (l'implémentation).

Mais les objets anémiques ne sont pas obligatoirement un mal : certains langages ne proposent pas de moyen de créer des structures de données, comme Java, par exemple

Tout l'exercice est donc de savoir quand un objet est anémique et quand il représente une structure de données. Ce qui est relativement simple : l'usage des structurees de données est relativement restreint en OOP
{{% /note %}}


---

{{% conclusion %}}