+++
title = "Composition\nvs\nInheritance"
outputs = ["Reveal"]
[cascade]
    authors = "Yann Danot & Dimitry Ernot"
+++


{{% introduction %}}

---
## What is the problem ?

When we want to serve a behavior, we can serve it by :
- Ourselves
- Inheriting it from a component 
- Using a component

---
{{% sectionbreak "Composition" %}}
{{% /sectionbreak %}}

---
- Composition is making wholes out of parts
- Technically, when a class uses an other one to serve a behavior 

---
{{% sectionbreak "Inheritance" %}}
{{% /sectionbreak %}}

---
## Inheritance captures semantics

- Arranging concepts from generalized to specialized
- Grouping related concepts in subtrees

---
## Inheritance captures mechanics

- the subclass will inherit the implementation of the superclass and thus also its interface
- When inheriting from a class, we are implicitly accepting responsibilities.
- Subclasses are tightly coupled to its superclass

---
## Inheritance uses

- Both classes are in the same logical domain
- The subclass is a proper subtype of the superclass
- The superclass’s implementation is necessary for the subclass
- The enhancements are primarily additive

---
## Composition vs Inheritance

Classes should achieve ****polymorphic**** behavior and ****code reuse**** by their ****composition**** (by containing instances of other classes that implement the desired functionality) rather than ****inheritance**** from a base or parent class

---
## Appendix

[https://www.thoughtworks.com/insights/blog/composition-vs-inheritance-how-choose](https://www.thoughtworks.com/insights/blog/composition-vs-inheritance-how-choose)

---
{{% conclusion %}}