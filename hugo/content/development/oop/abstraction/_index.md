+++
title= "Abstraction"
outputs = ["Reveal"]
subtitle="An introduction to OOP"
authors="Dimitry Ernot"
draft = false
+++

{{% introduction %}}

---
{{% sectionbreak "Abstraction" %}}
{{% /sectionbreak %}}

---
## Code is hard to read, not to write
{{% columns %}}
{{% column %}}
{{% illustration "img/ship.jpg" "<a href='https://unsplash.com/@jbcreate_?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Joseph Barrientos</a> sur <a href='https://unsplash.com/fr/photos/eUMEWE-7Ewg?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% /column %}}
{{% column %}}
{{% block %}}
A method modelizes an interaction

How to talk to the object
{{% /block %}}
{{% /column %}}
{{% /columns %}}
****Affordance: an object should suggest its usage****


{{% note %}}

C'est toujours facile de comprendre le code au moment où on l'écrit. Tout est frais dans notre tête et on a une idée claire de la direction à prendre. Par contre, quand on revient dessus après quelques semaines ou mois, les choses sont beaucoup moins claires. Et il peut devenir compliqué de comprendre ce qu'on a voulu faire, si on ne prend pas le soin de faire en sorte que notre code explicite lui-même son intention.

Une méthode décrit la façon dont on peut interagir avec notre objet : les informations nécessaires via les paramètres. Le retour qu'on peut en attendre avec la valeur retournée. Et, surtout ce qu'on peut faire comme action via son nom. C'est pourquoi il est très important de choisir intelligemment le nom de nos méthodes.

Un capitaine de bateau ne descend pas dans la salle des machines changer l'axe du gouvernail ou la vitesse du rotor quand il veut virer de bord. Il utilise la barre pour manoeuvrer. Et, par sa forme et la façon de l'utiliser, on comprend son but et son fonctionnement.
{{% /note %}}

---
{{% illustrated "img/joystick.jpg"  "<a href='https://unsplash.com/ja/@thomasdes?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Thomas Despeyroux</a> on <a href='https://unsplash.com/photos/i_qs6f6y8ag?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
## Behavior
- How to interact with an object
- What the object can do

****That's the public methods****

{{% /illustrated %}}

{{% note %}}
Ce qu'on appelle comportement d'un objet n'est autre que la façon dont on communique avec lui et ce qu'il est capable de faire.

Et derrière tout ça, on peut se dire que, finalement, on n'a pas forcément besoin de connaître l'objet plus que ses fonctions dont on a besoin, qu'on utilise.

Par exemple, si on reprend le kata Mars Rover, le système qui envoie les commandes au rover se soucie peu de savoir si elle les envoie à un mars rover ou à autre chose. Tant que cet autre chose possède les méthodes avancer, reculer, tourner à gauche ou à droite.
{{% /note %}}

---
## Interfaces
{{% illustrated "img/contract.jpg" "Photo by <a href='https://unsplash.com/@kellysikkema?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Kelly Sikkema</a> on <a href='https://unsplash.com/photos/LNlzd-Y7orw?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
- Defines a contract that a class commits to implement
- Defines a **behavior**
    - What an object can do
{{% /illustrated %}}

{{% note %}}
Ce principe de "je peux manipuler n'importe quel objet tant qu'il a le comportement dont j'ai besoin" s'exprime à travers ce qu'on appelle un contrat d'interface ou plus communément une interface.

Une interface est un contrat qui décrit les propriétés qu'une classe va s'engager à implémenter. Elle va donc définir une liste de méthodes, attributs, etc. qu'une classe l'implémentant va obligatoirement devoir posséder.

La classe en question peut avoir d'autres propriétés, mais devra obligatoirement au moins avoir ceux décrit par l'interface.

Une interface ne fait que définir les prototypes des méthodes. Elle ne définit aucune implémentation.
{{% /note %}}


---
{{% illustrated "img/airplane-ground.jpg" "Photo by <a href='https://unsplash.com/@__menglong?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Bao Menglong</a> on <a href='https://unsplash.com/photos/white-airplane--FhoJYnw-cg?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Unsplash</a> " %}}

Focusing on essential info at design level

Regrouping key concepts

{{% /illustrated %}}
****A same object can have multiple aspects that must be decolerrate****

{{% note %}}

On va donc pouvoir utiliser les interfaces pour regrouper les concepts importants sous forme de comportements que possèdent nos objets. Et, surtout les comportements dont dépendent d'autres objets.

Si on prend le cas d'un avion, on peut facilement définir des grands concepts : pouvoir décoller, atterrir, prendre un cap, etc. Communiquer avec la tour de contrôle, donner le statut de sa checklist, etc.

On peut donc avoir différentes vues pour un même objet.
{{% /note %}}

---
{{% columns %}}
{{% column width="30" %}}
{{% illustration file="img/cockpit.jpg" author="<a href='https://unsplash.com/@cikedondong?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Abby AR</a> on <a href='https://unsplash.com/photos/1uwzsExrKzY?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% /column %}}
{{% column width="30" %}}
{{% illustration "img/arrivals.jpg" "<a href='https://unsplash.com/@jeshoots?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>JESHOOTS.COM</a> on <a href='https://unsplash.com/s/photos/arrivals?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% /column %}}
{{% column width="30" %}}
{{% illustration "img/maintenance.jpg" "<a href='https://unsplash.com/@jamesplewis?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>James Lewis</a> on <a href='https://unsplash.com/photos/46fJyLS6jmM?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}} 
{{% /column %}}
{{% /columns %}}
Multiple behavior for the same object

Its class will commit to implement the given interfaces

{{% note %}}
Un avion va donc être un objet qui va implémenter plusieurs comportements.

Autrement dit, la classe Avion va s'engager à implémenter plusieurs interfaces.

La tour de contrôle le verra comme un objet capable d'atterrir, décoller, prendre un cap, etc. Le hall de l'aéroport comme une arrivée ou un départ avec une destination et une heure. Les mécaniciens comme un planning de maintenance.
{{% /note %}}

---
## Separation of concerns

{{% columns %}}
{{% column %}}
{{% illustration "img/airplane.jpg" "<a href='https://unsplash.com/@nathanhobbs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Nathan Hobbs</a> on <a href='https://unsplash.com/images/things/airplane?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% block "small" %}}
What is your position and altitude? \
Land on runway #4
{{% /block %}}
{{% /column %}}
{{% column %}}
{{% illustration "img/arrivals.jpg" "<a href='https://unsplash.com/@jeshoots?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>JESHOOTS.COM</a> on <a href='https://unsplash.com/s/photos/arrivals?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% block "small" %}}
Where are you going? \
What time will you arrive?
{{% /block %}}
{{% /column %}}
{{% /columns %}}  
{{% note %}}

Tout l'intérêt sera de dire que la tour de contrôle a besoin de communiquer avec l'avion mais n'a pas forcément besoin de connaître son plannig de maintenance. Uniquement les informations relatives à son vol

Quant à l'affichage des arrivées dans le terminal de l'aéroport, lui a besoin de connaître les informations relatives au vol (destination, origine, heure d'arrivée, etc.) mais ne doit surtout par être capable de communiquer avec l'avion.

L'objectif sera de voir nos avions d'un côté comme un objet avec lequel nous pouvons communiquer et afficher sur un radar. Et, de l'autre, comme un objet qui nous indique sa destination et l'heure d'arrivée. Donc de voir le même objet (l'avion) avec deux comportements différents.
{{% /note %}}


---
Airport
{{% illustration small="true" file="img/control-tower.jpg" author="<a href='https://unsplash.com/@rob_vesseur?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Rob Vesseur</a> on <a href='https://unsplash.com/photos/HCvXRe1-vYM?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}

{{% block "tiny" %}}
Track airplane \
Detect emergency
{{% /block %}}
{{% columns %}}
{{% column width="50" %}}
{{% illustration small="true" file="img/radar.jpg" author="<a href='https://unsplash.com/@stellanj?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Stellan Johansson</a> on <a href='https://unsplash.com/photos/1PP0Fc-KSd4?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% /column %}}
{{% column width="50"%}}
{{% illustration small="true" file="img/cockpit.jpg" author="<a href='https://unsplash.com/@cikedondong?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Abby AR</a> on <a href='https://unsplash.com/photos/1uwzsExrKzY?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% /column %}}
{{% /columns %}}
Different sources with the same behavior: \
Send location and identifier

{{% note %}}
Les interfaces nous permettent de nous baser sur des comportements plutôt que des implémentations. La tour de contrôle d'un aéroport n'a pas besoin de savoir que les avions qui apparaissent sur ses écrans viennent d'un radar ou bien de ce que le pilote transmet. Tout ce qui importe, c'est d'avoir la donnée, peu importe sa source.

Pour une aplication, c'est la même chose : peu importe que les données proviennent d'une base de données ou bien d'un simple fichier. L'essentiel, c'est qu'on puisse récupérer ou modifier ces données.
{{% /note %}}

---
{{% sectionbreak "Inheritance" %}}
{{% /sectionbreak %}}

---
## From general to specific

{{% triptych 
file1="img/pens.jpg"
author1="Photo by <a href='https://unsplash.com/@kellysikkema?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Kelly Sikkema</a> on <a href='https://unsplash.com/photos/LFGAatMUDvc?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>"

file2="img/pencil.jpg"
author2="Photo by <a href='https://unsplash.com/@umby?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Umberto</a> on <a href='https://unsplash.com/photos/GQ4VBpgPzik?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" 

file3="img/fountain-pen.jpg"
author3="<a href='https://unsplash.com/@aaronburden?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Aaron Burden</a> on <a href='https://unsplash.com/photos/y02jEX_B0O0?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}

- Creating a generalization/specialization relation between classes
- Including features & characteristics from a class to another

{{% note %}}
Parfois, on a besoin d'obtenir un objet qui aurait des fonctionnalités supplémentaires ou une implémentation différentes. Par exemple, si on modélise des stylos quelconques, on pourrait avoir besoin également de crayon papier qui peuvent être gommé. Ou des stylo plumes rechargeables.

La façon d'écrire sera également différente : l'un avec du graphite qu'il faudra tailler, l'autre avec de l'encre qu'il faudra recharger

On aura donc des stylos classiques et des versions plus spécifiques.

Cette notion d'objet identique à quelques fonctionnalités ou implémentation prêtes se gère avec ce qu'on appelle l'héritage

Le principe d'héritage consiste à étendre une classe existante (la classe Stylo) pour en créer une nouvelle qui hérite des propriétés de cette dernière.
{{% /note %}}

---
## Parent class & children classes

{{< svg "/development/oop/basics/img/inheritance.svg" >}}

{{% vspace %}}

- The class `FountainPen` & `Pencil` inherit from the class `Pen`
    - `FountainPen` & `Pencil` are name **child classes** & `Pen` is a **parent class**
- `FountainPen` & `Pencil` will share the same methods inherited from `Pen`
- `FountainPen` & `Pencil` will be able to change some behavior from `Pen`
- `FountainPen` & `Pencil` will have the same nature than `Pen`

{{% note %}}
On parlera alors de classe parent et de classe enfant. Et, on va créer une hiérarchie entre nos classes où on part du cas le plus général (n'importe quel stylo) vers un cas plus spécifique (le stylo-plume).

Il va également avoir une relation forte entre notre classe parent et notre classe enfant puisque nous sommes en train de dire que les deux objets sont de même nature. C'est-à-dire qu'un stylo-plume est un stylo.

C'est là le piège de l'héritage : il ne sert surtout pas à factoriser du code. Mais bien à définir des objets de même nature pour les enrichir de nouvelles fonctionnalités ou d'adapter l'implémentation
{{% /note %}}

---
{{% sectionbreak "Abstract classes" %}}
{{% /sectionbreak %}}


---
## Abstract class

- A class whose implementation is not complete (eg: method defined without a body)
- An abstract class cannot be instantiated
- An abstract class must be extended
- Defines general concepts & objects families (human beings, vehicles, shapes, etc.)

{{% note %}}
On peut ne pas être en mesure de créer un objet à partir d'une classe qui modélise un concept trop général ou abstrait.

Quand on parle de stylo, tout le monde comprend de quoi on parle. Mais l'idée reste vague : on ne sait pas si on parle d'un stylo feutre, d'un crayon, marqueur, etc. La notion de stylo est trop vague pour qu'on puisse savoir concrètement de quoi on parle.

Ces concepts abstraits sont modélisés par des classes abstraites. Une classe abstraite est une classe comme une autre. A la différence qu'on ne peut pas l'instancier parce que son implémentation n'est pas complète. Le seul moyen d'utiliser une classe abstraite est donc d'en hériter pour définir les détails d'implémentation qui nous manque.

Une classe abstraite va donc poser les bases de ce que seront nos objets tout en nous obligeant à créer de nouvelles classes pour raffiner leur modélisation.
{{% /note %}}

---

{{% illustrated "img/warning.jpg" "<a href='https://unsplash.com/@randylaybourne?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Randy Laybourne</a> on <a href='https://unsplash.com/photos/duTm7WvwN0U?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
- Don't factorize code with inheritance
    - Use composition instead
- Don't use inheritance for particular state
    - Use property instead
- Don't extend a similar class in another context
    - Duplicate the class instead
{{% /illustrated %}}

{{% note %}}
Il faut faire attention et bien réfléchir à ce qu'on fait lorsqu'on utilise l'héritage.

L'héritage lie la classe parent et les classes enfant par une relation de même nature. C'est aussi le cas pour les classes enfants d'une même classe parent. Si le code pour faire voler un canard est le même que celui pour un avion, cela ne signifie pas qu'on peut créér une classe parent au-dessus pour factoriser le code. Parce qu'on serait en train de dire qu'un canard et un avion, dans le fond, c'est presque pareil...

On utilisera plutôt le pattern composition pour factoriser le code.

Il peut être tentant de vouloir faire de l'héritage pour représenter des cas particuliers de nos objets. L'exemple le plus commun est celui du rectangle et du carré : imaginons que nous ayons une classe Rectangle qui possède une longueur et une largeur qu'on peut agrandir ou réduire. On peut aussi calculer son périmètre, etc.

Maintenant, nous avons besoin de carré. Il pourrait être tentant de créer une classe Square qui hérite de Rectangle. Après tout, un carré est un rectangle dont la largeur est égale à la hauteur. Sauf qu'en héritant de Rectangle, on récupère aussi son comportement, dont la possibilité d'agrandir ou réduire sa hauteur et sa largeur. Sauf que, si on modifie la hauteur d'un carré, on obtient un rectangle. Et, si par malheur, on modifie le comportement de Square pour mofidier hauteur et largeur en même temps, on vient d'introduire un bug... ** /!\ FAIRE UNE ANIMATION **

{{% /note %}}

---

{{% illustrated "img/traffic-lights.jpg" "<a href='https://unsplash.com/es/@lucasbeckphotography?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Lucas Beck</a> sur <a href='https://unsplash.com/fr/photos/fXFlArRMJ3c?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% block "small" %}}
- Both classes are in the same business domain
- The parent class is necessary for the child class
- The child class is a proper subtype of the parent class
    - Preconditions aren't strengthened
    - Postconditions aren't weakened
    - Invariants are still valid
{{% /block %}}
{{% /illustrated %}}

{{% note %}}

L'héritage ne s'utilise que lorsqu'on peut respecter certaines règles. La première étant que nous restons dans  le même espace du domaine. Que la classe enfant a absolument besoin du comportement de la classe parent.
Et surtout que la class enfant est un sous-type de la classe parent.

Ce qui sous-entend que nous n'avons pas besoin d'ajouter de vérification sur le type avant d'utiliser un objet, Par exemple, si j'ai des canards et des canards en plastique, je n'ai pas besoin de vérifier qu'il a besoin de pile avant de le faire décoller. 

On s'assure aussi qu'on n'a pas non plus besoin de vérifier quoi que ce soit après l'avoir utiliser. Je n'ai pas besoin de vérifier qu'il faut éteindre mon canard.

Et dernier point, qu'on respecte les invariants : si on hérite d'une liste triée, la classe enfant doit aussi s'assurer que ses élements sont triés.

Avec tout ça, il y a peu de cas où l'héritage est ce dont on a besoin. Ca veut dire qu'il va falloir bien réfléchir à quand on décide d'en utiliser ou non.
{{% /note %}}

---
## Interface or Inheritance?

- A parent class: what an object is
    - A pencil is a pen. A car is a vehicle

- An interface: what an object can do
    - An airplane & a duck can fly. A car can move

{{% vspace %}}

{{% colored green "*A triangle is geometric shape* makes sense" %}}
{{% colored red "*A triangle can be geometric shape* doesn't make sense" %}}



{{% note %}}
Ca peut être compliqué de choisir entre utiliser des interfaces et de l'héritage.

Une interface définit un comportement qu'un objet a. C'est-à-dire ce qu'il est capable de faire.

L'héritage définit une relation dans la nature des objets. C'est-à-dire ce qu'un objet est.
{{% /note %}}