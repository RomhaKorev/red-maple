+++
title= "Polymorphism"
outputs = ["Reveal"]
subtitle="An introduction to OOP"
authors="Dimitry Ernot"
+++


{{% introduction %}}

---
{{% sectionbreak "Polymorphism" %}}
{{% /sectionbreak %}}

---
## Polymorphism

- Some operations are similar no matter the types
    - Summing two numbers or two strings
    - Printing a number or an object

- Instances can behave as if it were an instance of a parent class
    - A instance of `Rover` is an object of `Explorer`
    - Objects are used through general concepts & not proper to their own classes

---
{{% sectionbreak "Ad hoc polymorphism" %}}
{{% /sectionbreak %}}

---
A function is defined by its prototype:

- Its name 
- Its signature (i.e. the inputs & outputs)
  - Its return type  
  - Its parameters types 

---
- Two functions cannot have the same prototype (in the same scope)
- Two functions can have the same name if their signatures are different


---
Overloading a function

Creating a new functions with the same name but a different signature & a different behavior

- The function `add()` could be used to sum two integers, two decimal or to concatenate two strings
- The function `min()` could accept two or three integers as parameters

---
{{% sectionbreak "Parametric polymorphism" %}}
{{% /sectionbreak %}}

---
Some objects don't care about data type of their attribute
    - a list can store number, strings, object, etc.

Some algorithm could be generalized to any data type
    - sorting a list only needs how to get the comparison criteria


---
{{% sectionbreak "Generic polymorphism" %}}
{{% /sectionbreak %}}

---
A generic function or class is written in terms of data types to-be-specified-later

- A list can store any type of data
    - But a list of integers is not the same thing than a list of strings
- The min/max function can handle any kind of comparable values
    - But must handle the same type for parameters and return type

{{% conclusion %}}
