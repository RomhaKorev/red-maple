+++
title= "Clean Code"
outputs = ["Reveal"]
authors="Yann Danot"
+++


{{% introduction %}}

---
{{% sectionbreak "What is Clean Code for you ?" %}}
{{% /sectionbreak %}}

---
{{% sectionbreak "" %}}
![Focus Shift](img/wtfs.png)
{{% /sectionbreak %}}

---
{{% quote "Any fool can write a code that computer can understand. Good programmers write code that ****humans can understand****." "Martin Fowler" %}}

---
{{%columns %}}
{{%column %}}
{{% illustration "img/shining.png" %}}
{{% /column %}}
{{% column %}}

{{% quote "Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live." "Martin Golding" %}}

{{% /column %}}
{{% /columns %}}

---
{{%columns %}}
{{%column %}}
{{% illustration "img/windows.png" %}}
{{% /column %}}
{{% column %}}
## Broken Window Theory
{{% /column %}}
{{% /columns %}}

---
{{% quote "Clean code always looks like it was written by ****someone who cares.****" "Michael Feathers" %}}


---
## Meaningful name 1/2
- Use intention revealing names
- Use pronounceable names
- No magical number
- Avoid encoding (Hungarian notation)
- Avoid mental mapping


---
## Meaningful name 2/2
- Class names :
    - Avoid words like Manager, Data, Info
    - Use only nouns
- Method names :
    - Must contain a verb

---
{{% sectionbreak "" %}}
![Geek And Poke](img/comments.png)
## What about comments?
{{% /sectionbreak %}}

---
## Formatting

{{% snippet "formatting" %}}

---

{{% bracket title="Principles" %}}
{{% block "small" %}}
- Design pattern
- ****SLAP****
- ****SOLID**** vs STUPID
- ****YAGNI****/****KISS****/****DRY****
- Composition over inheritance
{{% /block %}}
{{% /bracket %}}

{{% bracket title="Practices" %}}
{{% block "small" %}}
 - Read/Read/Read
 - Code review/Pair programming
 - ****TDD****/BDD/DDD
 - Code quality metrics
 - Continuous integration/delivery
{{% /block %}}
{{% /bracket %}}

{{% bracket title="Mindset" %}}
{{% block "small" %}}
- Software Craftsmanship
- Boy scout rule
{{% /block %}}
{{% /bracket %}}

{{% bracket title="Heuristics" %}}
{{% block "small" %}}
- Experiences
- Code smells
{{% /block %}}
{{% /bracket %}}

---
{{% conclusion %}}