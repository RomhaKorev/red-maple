+++
title = "Technical Debt vs Technical Mess"
outputs = ["Reveal"]
weight = 1
+++

{{% sectionbreak "Technical ****Debt****" %}}
{{% /sectionbreak %}}


---
#### A considered and reasoned decision to do quickly

- To check our hypothesis
- The best way will not bring value *right now*

---
#### It's a business-driven decision in line with the long-term strategy


- Always for the sake of the project
- Everyone is aware of the debt, not only the developers


---
#### Only if we will fix it later

- As soon as possible
- We never forget our decisions


---
#### The code is still clean, well-written and tested

- A shortcut for the business
- Not an excuse for a quick & dirty solution