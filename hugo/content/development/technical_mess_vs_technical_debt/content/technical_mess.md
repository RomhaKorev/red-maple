+++
title = "Technical Debt vs Technical Mess"
outputs = ["Reveal"]
weight = 2
+++

{{% sectionbreak "Technical ****Mess****" %}}
{{% /sectionbreak %}}

---
#### Nothing more than a mess

- No business value, here
- No good reason to keep it like that

---
#### The code is hard to read, understand, maintain, etc.

- Entropy of code
- Lack of refactoring
- Lack of best practices

---
#### It will not benefit in the future

- Will lead to a rework at iso-perimeter
- Would be done in a better way for same business results

---
#### It is a loss for the business and the development

- High risk of bugs & defaults
- Could require a stop-the-line