+++
draft=true
title = "Overview"
subtitle = "Of S.O.L.I.D. Principles"
hidden = true
outputs = ["Reveal"]
[cascade]
    authors = "Olivier Albiez, Yann Danot & Dimitry Ernot"
+++


{{% introduction %}}

---
{{% illustration "img/warning.jpg" "Photo de <a href='https://unsplash.com/fr/@jannerboy62?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Nick Fewings</a> for <a href='https://unsplash.com/fr/photos/4pZu15OeTXA?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a> " %}}