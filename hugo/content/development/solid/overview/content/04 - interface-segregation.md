+++
outputs = ["Reveal"]
weight = 4
+++
{{% sectionbreak "S.O.L.****I****.D." %}}
Interface Segregation
{{% /sectionbreak %}}

---

## Interface Segregation
{{% illustration "./img/kitchen.jpg" "Photo by <a href='https://unsplash.com/@teodor_skrebnev?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Teodor Skrebnev</a> on <a href='https://unsplash.com/s/photos/kitchen?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}

---
## Interface Segregation
A client should never be forced to implement an interface that it doesn’t use or clients shouldn’t be forced to depend on methods they do not use.

---
## Interface Segregation
{{< snippet "interface-segregation" >}}

---
## Interface Segregation
* Fat interface/Class with a lot of methods
* Interface has multiple responsibilities
* Difficulties to expose a subset of responsibilities

