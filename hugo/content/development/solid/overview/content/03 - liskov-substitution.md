+++
outputs = ["Reveal"]
weight = 3
+++

{{% sectionbreak "S.O.****L****.I.D." %}}
Liskov Substitution
{{% /sectionbreak %}}

---
## Liskov Substitution
{{% illustration "./img/salad.jpg" "Photo by <a href='https://unsplash.com/@damaris01?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Damaris</a> on <a href='https://unsplash.com/s/photos/salad?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}

---
## 
{{%columns%}}
{{%column width="20" centered=true %}}
{{% illustration file="img/Barbara_Liskov.png" author="Mirko Raner" small=true %}}
{{% /column%}}
{{%column width="78" %}}
****Barbara Liskov****
{{% block "small" %}}
- American computer scientist
- CLU: first language to support abstraction 
- IEEE John von Neumann Medal (2004)
- A. M. Turing Award (2008)
- Computer Pioneer Award (2018)
{{% /block %}}
{{% /column%}}
{{% /columns%}}

---

---
## Liskov Substitution

Every subclass/derived class should be substitutable for their base/parent class.

---
## 
{{%columns %}}
{{%column %}}
{{% illustration "./img/duck.jpg" "Photo by <a href='https://unsplash.com/@mattseymour?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Matt Seymour</a> on <a href='https://unsplash.com/s/photos/duck?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>"%}}
{{% /column %}}
{{% column centered=true %}}
> « If it looks like a duck, quacks like a duck, but need battery. You probably have the wrong abstraction »
{{% /column %}}
{{% /columns %}}

---
## Liskov Substitution - Example
{{< snippet "liskov" >}}

---
## Liskov Substitution - Smells
* You have to check for the type provided
* You have to cast your type
