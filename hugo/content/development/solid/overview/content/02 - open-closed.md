+++
outputs = ["Reveal"]
weight = 2
+++

{{% sectionbreak "S.****O****.L.I.D." %}}
Open / Closed
{{% /sectionbreak %}}

---
## Open / Closed
{{% illustration "./img/television.jpg" "Photo by <a href='https://unsplash.com/@sveninho?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Sven Scheuermeier</a> on <a href='https://unsplash.com/s/photos/television?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}

---
## Open / Closed
Objects or entities should be open for extension, but closed for modification

---
## Open / Closed - Example
{{< snippet "open-closed" >}}

---
## Open / Closed - Smells
* Complex switch/Lot of `if`
* High cyclomatic complexity
