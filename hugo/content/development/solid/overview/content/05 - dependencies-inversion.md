+++
outputs = ["Reveal"]
weight = 5
+++

{{% sectionbreak "S.O.L.I.****D.****" %}}
Dependencies Inversion
{{% /sectionbreak %}}

---
## Dependencies Inversion

{{% illustration "img/car.jpg" "Photo by <a href='https://unsplash.com/@egor_vikhrev?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Egor Vikhrev</a> on <a href='https://unsplash.com/s/photos/car-assembly?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}

---
## Dependencies Inversion

Entities must depend on abstractions not on concretions.

It states that the high level module must not depend on the low level module, but they should depend on abstractions.

---
## Dependencies Inversion - Example
{{< snippet "dependencies-inversion" >}}

---
## Dependencies Inversion
* Dependencies between classes (vs interface)
* Monolithic architecture
* Abstraction depends on details/implementation