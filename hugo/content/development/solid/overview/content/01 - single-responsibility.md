
+++
outputs = ["Reveal"]
weight = 1
draft = true
+++

{{% sectionbreak "****S****.O.L.I.D." %}}
Single Responsibility
{{% /sectionbreak %}}

{{% note %}}
{{% /note %}}

---
## Single Responsibility
{{% illustration "./img/siren.jpg"  "Photo by <a href='https://unsplash.com/@justusmenke?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Justus Menke</a> on <a href='https://unsplash.com/s/photos/siren?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}

{{% note %}}
Imaginons une alarme dans la caserne des secours d'aéroport.

L'alarme sonne lorsqu'il y a un grave accident sur les pistes. Les secours doivent alors prendre tout leur matériel médical, s'équiper contre les incendies, etc.

Elle sonne quand il y a des oies sauvages en bout de piste. Ils doivent alors rapidement s'équiper avec le matériel pour les effrayer : fusil, fusée de détresse, pétard, etc.

L'alarme sert aussi pour pévenir des intempéries potentiellement dangereux pour les avions

Le soucis, c'est que quand elle retentit, les secours ne savent pourquoi on les appelle avant d'être en rang devant la caserne. Ce qui fait qu'ils prennent à la fois leur matériel pour les accidents et pour effrayer les oies...
{{% /note %}}

---
## Single Responsibility - Definition

{{% flow %}}
{{% illustration "./img/swiss-army-knife.jpg"  "Photo by <a href='https://unsplash.com/@dmjdenise?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Denise Jans</a> on <a href='https://unsplash.com/s/photos/swiss-knife?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}
{{% illustration "./img/toolbox.jpg" "Photo by <a href='https://unsplash.com/@toddquackenbush?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Todd Quackenbush</a> on <a href='https://unsplash.com/s/photos/tools?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}
{{% /flow %}}

An artifact should have one and only one reason to change, meaning that an artifact should have only one job.

{{% note %}}
Le SRP nous dit qu'un artefact ne doit avoir qu'une seule et unique utilité. Dans le sens que lorsqu'on doit le modifier, alors on est sûr de n'impacter aucun autre comportement.

Quand on touche à l'implémentation d'une fonction, on est sûr de la raison pour laquelle on la modifie et que ça n'aura pas d'autre impact
{{% /note %}}

---
## Single Responsibility - Function

- Doesn't do A then B
    - But calls a function doing A, then a function doing B
- Isn't used for several purposes

{{% note %}}
Au niveau des fonctions, SRP nous dit qu'une méthode ne doit pas faire plusieurs choses à la fois
Si l'objectif est de faire A puis B. On écrira une fonction (très certainement privée) qui fera A et une seconde qui fera B

Et notre méthode aura la charge de coordonner l'appel de ces deux fonctions
{{% /note %}}


---
{{< snippet "single-responsibility-function" >}}

- Too long body
- Too many parameters
- When we need a `else`
- Multiple functional/technical concepts at the same


---
## Single Responsibility - Variable

{{% fragment %}}
{{< snippet "single-responsibility-variable-1" >}}
{{% /fragment %}}
{{% fragment %}}
{{< snippet "single-responsibility-variable-2" >}}
{{% /fragment %}}

- The value has only one interpretation and usage
- The variable does serve a single purpose

{{% note %}}
Une variable ne devrait avoir qu'un seul objectif et sa valeur une seule interprétation.

Dans le cas du fizzbuzz, on utilise souvent la variable `result` pour à la fois stocker la chaîne à afficher et savoir si le résultat n'est un multiple de rien. On a donc deux usages différents de la même variable

Un autre exemple : ici, la variable `distance` représente d'abord la distance qui sépare notre avion de l'aéroport. Puis, la distance avant que l'aéroport puisse le détecter. Elle change d'interprétation.
{{% /note %}}


---
## Single Responsibility - Class

{{< snippet "single-responsibility-class" >}}
- A class should manage only one concern
- A same model can be represented by multiple classes
  - One class `Employee` to deal with the salary
  - One class `Employee` to deal with the projects
  - etc.

---
## Single Responsibility - Smells

* Large Class
* Long Method
* Lot of methods
* High Coupling/Low cohesion
* Helper class
* Multiple functional/technical concepts at the same level

