interface GeographicalRepository {
  List<Area> getAllAreas();

  Vector3D getOwnLocation();

  LocalTime getReferenceTime();
}
