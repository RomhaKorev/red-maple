public void fizzbuzz(int value) {
  String result = compute(value);

  if (result == "") {
    return String.valueOf(value);
  }

  return result;
}