public void drawLine(Point a, Point b, Color color) {

  Painter painter = createPainterFromContext();

  if (color != Color.Transparent) {
    painter.setPen(color);
  } else {
    painter.setPen(Color.Black);
  }

  if (a.equals(b)) {
    Line line = new Line(a, b);
    painter.drawLine(line);
  }
}