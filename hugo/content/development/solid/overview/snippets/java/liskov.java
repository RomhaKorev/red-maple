public String getThreatLevelFor(Aircraft aircraft) {
  GeographicalRepository database = GeographicalRepositoryProvider.getInstance();

  if (database == null) {
    return "";
  } else if (database instanceof SQLGeographicalDatabase) {
    try {
      ((SQLGeographicalDatabase) database).connect();
    } catch (DBConnectionException exception) {
      System.out.println("Cannot get areas: access to DB refused");
      return "";
    }
  }

  Vector3D ownLocation = database.getOwnLocation();
  List<Area> areasInSector = new ArrayList<>();
  for (Area area : database.getAllAreas()) {
    double distance = Math.sqrt(Math.pow(area.center.x - ownLocation.x, 2) + Math.pow(area.center.y - ownLocation.y, 2));
    if (distance <= 1000) {
      areasInSector.add(area);
    }
  }

  return process(aircraft, areasInSector);
}