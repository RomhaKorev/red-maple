public void distance(Airport airport, Airplane airplane) {
  double x = airport.x() - airplane.x();
  double y = airport.y() - airplane.y();

  double distance = Math.sqrt(x * x + y * y);

  distance = distance - airport.detectionRadius();

}