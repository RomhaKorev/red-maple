public class Employee {

  private Salary salary;
  private List<Right> rights;
  private ProjectRepository projects;

  public Salary getSalary() {
    return salary.times(12);
  }


  public boolean isAdmin() {
    return rights.contains(Right.Admin);
  }

  public void joinTheProject(Project project) {
    projects.create(new ProjectDTO(this, project.getId()));
  }
}