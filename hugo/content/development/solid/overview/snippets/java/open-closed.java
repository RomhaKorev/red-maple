private static String process(Aircraft aircraft, List<Area> areas) {
    double norm = Math.sqrt(Math.pow(aircraft.speed.x, 2)
        + Math.pow(aircraft.speed.y, 2)
        + Math.pow(aircraft.speed.z, 2));
    
    if (norm > 2000)
      return "HIGH";
    if (aircraft.type.equals("MIG"))
      return "HIGH";
    if (aircraft.type.equals("Rafale"))
      return "LOW";
    if (aircraft.type.equals("A-320")) {
      if (checkIfInArea(aircraft.position, areas))
        return "HIGH";
      else return "LOW";
    }

    if (aircraft.type.equals("F-35") || aircraft.type.equals("UNKNOWN")) {
      if (checkIfInArea(aircraft.position, areas))
        return "HIGH";
      else return "MEDIUM";
    }
    return "MEDIUM";
  }