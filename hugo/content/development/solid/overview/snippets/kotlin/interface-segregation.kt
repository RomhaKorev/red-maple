interface GeographicalRepository {
  fun getAllAreas(): List<Area>
  fun getOwnLocation(): Vector3D
  fun getReferenceTime(): LocalTime
}
