fun process(aircraft: Aircraft, areas: List<Area>): String {
    val norm = sqrt(
        aircraft.speed.x.pow(2.0)
                + aircraft.speed.y.pow(2.0)
                + aircraft.speed.z.pow(2.0)
    )
    if (norm > 2000)
        return "HIGH"
    if (aircraft.type == "MIG")
        return "HIGH"
    if (aircraft.type == "Rafale")
        return "LOW"
    if (aircraft.type == "A-320") {
        return if (checkIfInArea(aircraft.position, areas)) "HIGH" else "LOW"
    }
    if ((aircraft.type == "F-35") || (aircraft.type == "UNKNOWN")) {
        return if (checkIfInArea(aircraft.position, areas)) "HIGH" else "MEDIUM"
    }
    return "MEDIUM"
}