fun getThreatLevelFor(aircraft: Aircraft): String {
    val database = GeographicalRepositoryProvider.getInstance()
    if (database == null) {
        return ""
    } else if (database is SQLGeographicalDatabase) {
        try {
            database.connect()
        } catch (exception: DBConnectionException) {
            println("Cannot get areas: access to DB refused")
            return ""
        }
    }
    val ownLocation = database.getOwnLocation()
    val areasInSector: MutableList<Area> = ArrayList()
    for (area: Area in database.getAllAreas()) {
        val distance =
            sqrt((area.center.x - ownLocation.x).pow(2.0) + (area.center.y - ownLocation.y).pow(2.0))
        if (distance <= 1000) {
            areasInSector.add(area)
        }
    }
    return process(aircraft, areasInSector)
}