public void distance(Airport airport, Airplane airplane) {

  double x = airport.x() - airplane.x();
  double y = airport.y() - airplane.y();

  final double distanceFromAirport
                    = Math.sqrt(x * x + y * y);

  final double distanceBeforeDetection
                    = distance - airport.detectionRadius();
}