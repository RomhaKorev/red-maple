public void fizzbuzz(int value) {
  
  Result result = compute(value);

  if (result.oneRuleApplied()) {
    return result.value();
  }

  return String.valueOf(value);
}