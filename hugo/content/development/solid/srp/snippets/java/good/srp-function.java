void drawLine(Point a, Point b, Color color) {
  
  Painter painter = createPainterFromContext();

  setColor(painter, color);

  draw(painter, new Line(a, b));
}


private void setColor(Painter painter, Color) {...}

private void draw(Painter painter, Line line) {...}


