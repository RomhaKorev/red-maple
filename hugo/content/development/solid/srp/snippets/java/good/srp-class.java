public class ProjectUser {
  private List<Right> rights;
  private ProjectRepository projects;

  public boolean isAdmin() {
    return rights.contains(Right.Admin);
  }

  public void joinTheProject(Project project) {
    projects.create(new DTO(this, project.getId()));
  }
}

public class Employee {
  private Salary salary;
  
  public Salary getSalary() {
    return salary.times(12);
  }
}