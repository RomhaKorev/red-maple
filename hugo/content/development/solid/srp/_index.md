+++
subtitle = "****S****.O.L.I.D. Principles"
title = "Single Responsability"
outputs = ["Reveal"]
[cascade]
    authors = "Olivier Albiez, Yann Danot & Dimitry Ernot"
+++

{{% sectionbreak "****S****.O.L.I.D." %}}
Single Responsibility
{{% /sectionbreak %}}

{{% note %}}
{{% /note %}}

---
## Single Responsibility
{{% illustration "./img/siren.jpg"  "Photo by <a href='https://unsplash.com/@justusmenke?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Justus Menke</a> on <a href='https://unsplash.com/s/photos/siren?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}

{{% note %}}
Imaginons une alarme dans la caserne des secours d'aéroport.

L'alarme sonne lorsqu'il y a un grave accident sur les pistes. Les secours doivent alors prendre tout leur matériel médical, s'équiper contre les incendies, etc.

Depuis peu, il y a des oies sauvages qui squattent le bout de la piste. La caserne est chargée d'aller les effrayer avec le matériel adéquat : fusil, fusée de détresse, pétard, etc.

Vu qu'il y a déjà une alarme, c'est autant la réutiliser, plutôt que d'en installer une deuxième.

Le soucis, c'est que quand elle retentit, les secours ne savent pourquoi on les appelle avant d'être en rang devant la caserne. Ce qui fait qu'ils prennent à la fois leur matériel pour les accidents et pour effrayer les oies...
{{% /note %}}

---
## Single Responsibility - Definition

{{% flow %}}
{{% illustration "./img/swiss-army-knife.jpg"  "Photo by <a href='https://unsplash.com/@dmjdenise?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Denise Jans</a> on <a href='https://unsplash.com/s/photos/swiss-knife?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}
{{% illustration "./img/toolbox.jpg" "Photo by <a href='https://unsplash.com/@toddquackenbush?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Todd Quackenbush</a> on <a href='https://unsplash.com/s/photos/tools?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}
{{% /flow %}}

An artifact should have one and only one reason to change, meaning that an artifact should have only one job.

{{% note %}}
Le SRP nous dit qu'un artefact (variable, fonction, classe, module, etc.) ne doit avoir qu'une seule et unique utilité. Dans le sens que lorsqu'on doit le modifier, alors on est sûr de n'impacter aucun autre comportement.

Quand on touche à l'implémentation d'une fonction, on est sûr de la raison pour laquelle on la modifie et que ça n'aura pas d'autre impact ailleurs parce qu'elle ne sert qu'à la tâche que nous devons faire évoluer.
{{% /note %}}

---
## Single Responsibility - Function

- Doesn't do A then B
    - But calls a function doing A, then a function doing B
- Isn't used for several purposes

{{% note %}}
Au niveau des fonctions, SRP nous dit qu'une méthode ne doit pas faire plusieurs choses à la fois
Si l'objectif est de faire A puis B. On écrira une fonction (très certainement privée) qui fera A et une seconde qui fera B

Et notre méthode aura la charge de coordonner l'appel de ces deux fonctions
{{% /note %}}

---
Conditional blocks

{{% columns %}}
{{% column %}}
Bad
{{< snippet "bad/srp-function" >}}
{{% /column %}}
{{% column %}}
Good
{{< snippet "good/srp-function" >}}
{{% /column %}}
{{% /columns %}}

**Use Return Early Pattern**: try to not use `else` keyword

{{% note %}}
Un bon moyen de détecter une fonction qui aurait plus d'une responsabilité, c'est le mot-clé `else`.
Il y a deux cas de figure ou le `else` peut être utilisé : 
- soit une fonction se termine par un bloc `else`. On peut le retirer
- soit une fonction a du code suite à un `else`. Donc un second traitement après le if : on peut sortir le bloc if/else dans une autre fonction
{{% /note %}}

---
## Single Responsibility - Variable
{{% columns %}}
{{% column %}}
Bad
{{< snippet "bad/srp-variable-1" >}}
{{% /column %}}
{{% column %}}
Good
{{< snippet "good/srp-variable-1" >}}
{{% /column %}}
{{% /columns %}}

The value has only one interpretation and usage

---
## Single Responsibility - Variable
{{% columns %}}
{{% column %}}
Bad
{{< snippet "bad/srp-variable-2" >}}
{{% /column %}}
{{% column %}}
Good
{{< snippet "good/srp-variable-2" >}}
{{% /column %}}
{{% /columns %}}


The variable does serve a single purpose

{{% note %}}
Une variable ne devrait avoir qu'un seul objectif et sa valeur une seule interprétation.

Dans le cas du fizzbuzz, on utilise souvent la variable `result` pour à la fois stocker la chaîne à afficher et savoir si le résultat n'est un multiple de rien. On a donc deux usages différents de la même variable

Un autre exemple : ici, la variable `distance` représente d'abord la distance qui sépare notre avion de l'aéroport. Puis, la distance avant que l'aéroport puisse le détecter. Elle change d'interprétation.
{{% /note %}}


---
## Single Responsibility - Class

{{% columns %}}
{{% column %}}
Bad
{{< snippet "bad/srp-class" >}}
{{% /column %}}
{{% column %}}
Good
{{< snippet "good/srp-class" >}}
{{% /column %}}
{{% /columns %}}

- A class should manage only one concern
- A same model can be represented by multiple classes
  - One class `Employee` to deal with the salary
  - One class `Employee` to deal with the projects
  - etc.

{{% note %}}
Pour les objets, il s'agit de ne modéliser que ce dont on a besoin. et de réfléchir en terme de comportement.

Pour les RH, les employés peuvent prendre des congés, négocier leur salaire, passer leur entretien annuel, etc.

Par contre, pour la DSI, on va parler de droits d'accès, de matériel, badges, etc.

Vouloir tout modéliser dans une seule classe Employee va conduire à des objets très gros. Et en plus, la DSI va se retrouver avec la possibilité d'accéder aux données personnelles d'un employé...
{{% /note %}}


---
## Single Responsibility - Smells

* Large Class
* Long Method
* Lot of methods
* High Coupling/Low cohesion
* Helper class
* Multiple functional/technical concepts at the same level

---

SRP is a compromise between the quantity of artifact to understand and the ease of understanding an artifact.

{{% note %}}
En résumé, le SRP nous dit qu'un artefact doit n'avoir qu'une seule responsabilité dans le sens où il ne doit avoir qu'une seule bonne raison de changer.

Mais c'est surtout une tension de design entre la facilité et la vitesse qu'il nous faut pour comprendre un artefact et le nombre d'artefact à comprendre.

Si on surdécoupe, on risque de se retrouver à avec un paquet de classes, fonctions, etc. à devoir maîtriser pour comprendre ce que fait le code.

Si on ne découpe pas assez, il va falloir ajouter à la compréhension la charge mentale de démêler les concepts
{{% /note %}}

---
{{% conclusion %}}