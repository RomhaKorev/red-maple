+++
subtitle = "S.****O****.L.I.D. Principles"
title = "Open / Closed"
outputs = ["Reveal"]
[cascade]
    authors = "Olivier Albiez, Yann Danot & Dimitry Ernot"
+++

{{% sectionbreak "S.****O****.L.I.D." %}}
Single Responsibility
{{% /sectionbreak %}}

{{% note %}}
{{% /note %}}

---
## Open/Closed Principle
{{% illustration "./img/television-2.jpg" "Photo by <a href='https://unsplash.com/@ajeetmestry?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Ajeet Mestry</a> on <a href='https://unsplash.com/photos/turned-off-black-television-UBhpOIHnazM?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Unsplash</a> " %}}

{{% note %}}
Lorsqu'on souhaite ajouter une console de jeu ou une nouvelle barre de son, on branche le nouvel équipement à sa TV via des câbles et des prises spéciales. La TV va ensuite détecter ce qui est branché et diffuser le son ou l'image comme il faut.
A aucun moment, nous avons besoin d'ouvrir la TV et d'aller souder des composants électroniques à l'intérieur...

C'est exactement la même chose pour le code : on doit éviter de devoir aller triturer le code existant pour ajouter une nouvelle fonctionnalité
{{% /note %}}

---
## Open/Closed - Definition
Objects or entities should be open for extension, but closed for modification

{{% note %}}
Le principe d'open closed nous dit que notre code doit être ouvert à l'extension et fermer aux modifications.

En d'autres termes, l'ajout d'une nouvelle fonctionnalité doit impliquer le minimum de modification du code existant voire aucune modification
{{% /note %}}

---
## Open/Closed

{{% columns %}}
{{% column %}}
{{% illustration "./img/this-way.jpg" "Photo by <a href='https://unsplash.com/@jamietempleton?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Jamie Templeton</a> on <a href='https://unsplash.com/photos/brown-wooden-plank-fence-with-this-way-signboard-6gQjPGx1uQw?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Unsplash</a> " %}}
{{% /column %}}
{{% column %}}
- A code could prevent new features
    - Third part library
    - Closed class
- A code could impose a way of doing
    - incomptabile with our code
    - Violating our practices
{{% /column %}}
{{% /columns %}}
  
{{% note %}}
Il arrive qu'on tombe sur du code que nous devons faire évoluer mais qui est verroullié de par sa structure même.

Une classe ou un module peut être compilée ou partagée dans une bibliothèque qui rend sa modification impossible. Sauf que cela peut rentrer en conflit avec nos besoins ou objectifs : l'ajout de nouvelle fonctionnalité est compliquée dans une bibliothèque tierce. La façon dont on utilise une classe peut poser problème. Ou tout simplement, il faudra remplacer un bout de code par un autre dans un futur proche

On passera alors par un moyen de limiter l'impact de ce code sur le nôtre en l'isolant par exemple.
{{% /note %}}

---
## Open/Closed - Why

- When the business logic must **often** change
    - Modification leads to regression
- We should work with abstraction. Not implementation
    - A component can easily evolve if we don't use it directly

{{% note %}}
Tout l'objectif est de dire que le fonctionnel qui change souvent doit pouvoir évoluer sans avoir à modifier le code source du composant

Parce que modifier du code implique de potentiellement impacter les autres fonctionnalités et d'introduire bugs ou régression

Open/Closed revient en somme à dire qu'on veut se baser sur des comportements plutôt que des implémentations.

Et à trouver des moyens d'introduire de nouveaux composants sans modifier le code (ou en modifiant le moins de code possible). On peut par exemple, penser aux injections de dépendance de Spring, .Net, etc.
{{% /note %}}


---
# Open/Closed - Conclusion

The effort required to add a new feature

VS

How often one will need to add a new feature

{{% note %}}
Le principe Open/Closed permet de réfléchir à l'effort nécessaire pour ajouter une nouvelle fonctionnalité dans un code existant en apportent le moins modification possible. Et de le mettre en balance avec la fréquence à laquelle nous avons besoin d'ajouter une nouvelle fonctionnalité.
{{% /note %}}

---
{{% conclusion %}}