+++
subtitle = "S.O.****L****.I.D. Principles"
title = "Liskov Substitution"
outputs = ["Reveal"]
[cascade]
    authors = "Olivier Albiez, Yann Danot & Dimitry Ernot"
+++

{{% sectionbreak "S.O.****L****.I.D." %}}
Liskov Substitution
{{% /sectionbreak %}}

---
{{% illustration "./img/salad.jpg" "Photo by <a href='https://unsplash.com/@damaris01?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Damaris</a> on <a href='https://unsplash.com/s/photos/salad?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}

{{% note %}}
Imaginons que nous avons une machine pour hacher le persil pour mettre dans une salade. Et que maintenant, on voudrait aussi hacher de la cigüe pour en faire des analgésiques.

Le processus est le même. Alors pourquoi ne pas utiliser la même machine ? Après tout, persil et cigüe sont deux plantes qui ont pas mal de caractéristiques en commun.

Le souci, c'est que la cigüe est hautement toxique. Il va donc falloir bien vérifier si on vient de hacher du persil ou de la cigüe
{{% /note %}}


---
## Liskov Substitution

Every subclass/derived class should be substitutable for their base/parent class.

{{% note %}}
Le principe de substitution de Liskov nous dit que une classe enfant doit pouvoir substituer leur classe parent sans aucune contrainte.

Pour faire plus simplement, nous ne devrions pas nous demander si nous sommes en train de manipuler un objet de la classe parent ou de la classe enfant.
{{% /note %}}

---
## 
{{%columns %}}
{{%column %}}
{{% illustration "./img/duck.jpg" "Photo by <a href='https://unsplash.com/@mattseymour?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Matt Seymour</a> on <a href='https://unsplash.com/s/photos/duck?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>"%}}
{{% /column %}}
{{% column centered=true %}}
> « If it looks like a duck, quacks like a duck, but need battery. You probably have the wrong abstraction »
{{% /column %}}
{{% /columns %}}

{{% note %}}
Le principe de substitution de Liskov peut s'exprimer avec le test du canard.

Pour citer Douglas Adams : Si ça ressemble à un canard, que ça fait coin coin comme un canard, on peut considérer la possibilité que nous avons un oiseau aquatique de la famille des anatidae dans les mains.

Par contre, si ça ressemble à un canard, que ça fait coin coin comme un canard, mais que ça a besoin de piles, c'est qu'on a un problème d

{{% /note %}}



---
## Behavioural subtyping

- Subclasses should satisfy the expectations of clients
    - We expect that the method `User.getName()` returns the name of the given user
    - We expect that the method `Collection.first()` returns the first item, not the second

- Syntactic safety is not enough
    - Design matters
    - Behavior over data types


{{% note %}}
Quand on parle de sous-typage, on ne parle pas uniquement d'héritage d'un point de vue du code. Mais plutôt en termes de design et de hiérarchie de classes cohérente.

Quand on manipule des véhicules et qu'on peut connaître leur niveau de carburant, on évitera de considérer qu'un vélo ou un cheval fait partie de ce type de véhicule. Et, sans doute qu'il n'y aura aucune raison pour considérer qu'un cheval est un véhicule dans notre code.

On évitera donc d'utiliser l'héritage pour factoriser notre code.
{{% /note %}}


---
## Strenghtening preconditions

We should not add preconditions to deal with subtypes

- If the base type needs an integer as input, the subtype should not handle positive integers only
- The subtype should not require more action before usage than the base type
    - We don't need to check the batteries of our duck
    - We should not check if the database is connected or if the file exists
- We should not check if we use the base type or a subtype

{{% note %}}

Une précondition est une condition attachée à une fonction qui doit être vérifiée avant d'exécuter la fonction. Par exemple, une fonction qui divise a par b définit comme pré-condition que b n'est pas 0.

Le principe de Liskov tire une première règle sur les pré-conditions. C'est-à-dire ce qu'on considère comme les règles à appliquer avant d'utiliser un objet ou une fonction.

Par exemple, si un type pose le postulat que le paramètre de sa fonction est un entier, un sous type ne pourra considérer que la fonction prend uniquement des entiers positifs.
En ajoutant de nouvelles règles d'utilisation, on vient de casser le code qui fonctionnait très bien avec les entiers négatifs.
{{% /note %}}

---
## Weakening postconditions
We should not remove postconditions because of a subtype

- The postconditions on returned values should be respected
    - A subtype of abs() must return a positive integer
    - We should not have to check the returned value before using it
- A subtype should not throw an exception if the base type doesn't

{{% note %}}
Une post-condition est une condition qui doit être garantie après l'exécution d'une fonction. Une liste est forcément triée après avoir appeler la fonction sort.

Deuxième règle : l'affaiblissement des post-conditions. Si le type de base pose des post-conditions. Par exemple, l'entier retourné par abs() est forcément positif ou la liste est triée après sort(), alors les sous-types doivent absolument valider ses règles.

Autrement dit, on ne devrait pas ajouter de contrôle au cas où un objet du sous-type ne respecte pas les post-conditions établies par le type de base.
{{% /note %}}

---
## Variance
- Contravariance
    - parameters can be a more generic (less derived) type than originally specified
    - Dog can become Animal
- Covariance
    - Return type can be a more derived type than originally specified
    - Animal can become Dog

{{% note %}}
Dernière étape : la notion de variance.

La notion de variance fait référence à la façon dont le sous-typage d'un composant est lié au sous-typage de ses composants.
Notamment, dans le cas qui nous intéressera ici, comment le sous-typage d'une fonction est liée au sous-typage des paramètres et des valeurs de retour.

Par exemple, le lien entre une fonction nourrir(chat) et une fonction nourrir(animal).

La contravariance sur les paramètres.

C'est-à-dire qu'un paramètre de la fonction dans la classe dérivée ne peut être que du même type ou un super-type du paramètre.
Si on a une fonction nourrir(chien) dans notre type de base. Le sous-type ne pourra avoir qu'une fonction nourrir(chien) ou une fonction nourrir(animal). Mais pas nourrir(bouledogueFrançais)

En gros, on ne peut que remonter dans l'héritage.
Cela vient du fait qu'on ne peut pas ajouter de comportement supplémentaire à nos paramètres. On peut en retirer en utilisant un de ses super-types.

La covariance sur les types de retour

Le type de retour de la fonction dans la classe dérivée ne peut être que du même type ou un sous-type.
Si une fonction adopt() retourne un Dog, dans la classe de base, alors le sous-type nous pourra avoir une fonction adpot() qui retourne un Labrador ou un Dog. Mais pas un Animal
Pour faire simple, on ne peut retourner qu'un objet qui a au moins le même comportement.

Cela vient du fait qu'on ne peut retirer de comportement à la valeur de retour. De façon à ce qu'on puisse utiliser l'objet retourné exactement de la même façon qu'il s'agisse d'un objet de la classe de base ou d'une classe dérivée.

{{% /note %}}

---
## Barbara Liskov
{{%columns%}}
{{%column width="20" centered=true %}}
![Barbara Liskov](https://upload.wikimedia.org/wikipedia/commons/6/60/Barbara_Liskov.PNG?20091027173400)
{{% /column%}}
{{%column width="78" %}}
{{% block "small" %}}
- American computer scientist
- CLU: first language to support abstraction 
- IEEE John von Neumann Medal (2004)
- A. M. Turing Award (2008)
- Computer Pioneer Award (2018)
{{% /block %}}
{{% /column%}}
{{% /columns%}}

{{% note %}}

Petite note de culture générale, Barbara Liskov est une mathématicienne de renom connue pour avoir contribuer à CLU, le premier langage de programmation à supporter l'abstraction de données.

Elle  a reçu en 2004 la médaille John von Neumann pour « ses contributions fondamentales aux langages de programmation, à la méthodologie de la programmation et aux systèmes distribués » et le Prix Turing en 2008.

{{% /note %}}