+++
subtitle = "S.O.L.I.****D****. Principles"
title = "Dependencies Inversion Principle"
outputs = ["Reveal"]
[cascade]
    authors = "Olivier Albiez, Yann Danot & Dimitry Ernot"
+++

{{% sectionbreak "S.O.L.I.****D****." %}}
Dependencies Inversion Principle
{{% /sectionbreak %}}

---
## Dependencies Inversion
{{% illustration "img/rocket.jpg" "Photo by <a href='https://unsplash.com/@nasa?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>NASA</a> on <a href='https://unsplash.com/photos/soyuz-rocket-launches-from-baikonur-launch-pad-JkaKy_77wF8?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Unsplash</a> " %}}

{{% note %}}
Imaginons une chaîne de montage d'une fusée. On construit le module de commande. Il a besoin du module de service. Du coup, il va le construire. Le module de service a besoin du moteur de fusée. Le moteur de fusée a besoin de son réservoir qui construit son moteur de poussée qui va construire son réservoir qui aura besoin du lanceur, etc.

Et petit à petit, on obtient une fusée où chaque étage construit l'étage d'en-dessous.

Maintenant, on veut changer le lanceur pour un nouveau modèle un peu différent (par exemple qui a besoin d'un paramètre en plus de son constructeur, si on revient à l'OOP). Mais en faisant ça, on du coup doit aussi modifier le moteur de fusée. Et, si on le modifie, on doit aussi modifier le module de service, etc. On a donc besoin de changer tous les étages pour un changement qui ne concerne que le lanceur.
{{% /note %}}

---
## Dependencies Inversion

Entities must depend on abstractions not on concretions.

It states that the high level module must not depend on the low level module, but they should depend on abstractions.

{{% note %}}
Autrement dit, quand une classe a une dépendence, elle devrait passe par une interface.

De cette façon, ele dépendra unqiuement du comportement et non pas de la classe concrète. Ce sera donc plus facile de faire évoluer ou de remplacer l'implémentation. Le code devient plus modulaire.
{{% /note %}}


---
## How to deal with dependencies
- Objects should not create their dependencies
    - But, it should be passed as parameters
- Objects should only know interfaces, not implementation

{{% note %}}
Dans notre code, ça se traduit par le fait qu'une classe ne va pas créer elle-même les objets dont elle dépend. Mais elle va attendre à ce qu'on lui les passe en paramètre de son constructeur.
De cette façon, elle ne dépend que des comportements dont elle a besoin et non plus des implémentations
{{% /note %}}

---
## Details of implementation
{{% illustrated "img/clockwork.jpg" "Photo by <a href='https://unsplash.com/@viazavier?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Laura Ockel</a> on <a href='https://unsplash.com/photos/gold-and-silver-round-accessory-UQ2Fw_9oApU?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Unsplash</a> " %}}
- Changing details should not impact higher levels
    - How to drive a car does not depend on the kind of engine
    - The data source does not change the business rules
- A component should work in isolation
    - We should be able to test a component alone
{{% /illustrated %}}

Details should depend on abstraction

{{% note %}}
Les composants devraient fonctionner en isolation : nous devrions prendre un composant et être capable de le tester indépendemment des autres. Simplement en stubbant ses dépendances (et sans devoir utiliser un framework de Mock)

Un composant doit être coherent en terme de fonctionnalités qu'il fournit.

La cohesion fait référence à la façon dont les éléments d'un composant vont ensemble. On va donc chercher à regrouper les fonctionnalités qui sont liées dans le même composant. Et donc, sortir ses dépendances dans d'autre composants
{{% /note %}}

---


{{% illustrated "img/boxes.jpg" "Photo de <a href='https://unsplash.com/fr/@tozzo?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Brunno Tozzo</a> sur <a href='https://unsplash.com/fr/photos/pallet-in-legno-marrone-t32lrFimPlU?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Unsplash</a> " %}}
- Dependencies inversion is about isolating components
- A tradeoff between cohesion and coupling
{{% /illustrated %}}

{{% note %}}
En résumé, le principe d'inversion de dépendances consiste à isoler et regrouper les fonctionnalités de façon cohérente et faire en sorte que les composants dont elles dépendent sont créés à l'extérieur.
De cette façon, on peut facilement détourer les composants eux-mêmes.

Toute la problématique est de déterminer si un sous-composant est une dépendance ou non. C'est-à-dire si ça a un sens de sortir un sous-composant et de le passer en tant que dépendance ou non.
{{% /note %}}

