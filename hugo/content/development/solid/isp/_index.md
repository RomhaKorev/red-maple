+++
subtitle = "S.O.L****I****.D. Principles"
title = "Interface Separation Principle"
outputs = ["Reveal"]
[cascade]
    authors = "Olivier Albiez, Yann Danot & Dimitry Ernot"
+++

{{% sectionbreak "S.O.L.****I****.D." %}}
Interface Separation Principle
{{% /sectionbreak %}}

---
## Interface Segregation
{{% illustration "./img/kitchen.jpg" "Photo by <a href='https://unsplash.com/@teodor_skrebnev?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Teodor Skrebnev</a> on <a href='https://unsplash.com/s/photos/kitchen?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash </a>" %}}

{{% note %}}
Imaginons que nous souhaitions installer une nouvelle cuisine chez nous. On a besoin d'un plan de travail, une plaque de cuisson, un four et un réfrigérateur. C'est tout.
Sauf que le cuisiniste nous dit que si on prend un meuble pour frigo, il faut aussi un congélateur pour aller avec. Si on prend le meuble des plaques de cuisson, il faut un lave-vaisselle et une hotte. Le four vient forcément avec un chauffe-plat et un rangement pour casseroles

Sauf que nous, on n'a pas besoin de tout ça...
{{% /note %}}

---
## Interface Separation Principle
> A client should never be forced to implement an interface that it doesn’t use or clients shouldn’t be forced to depend on methods they do not use.

{{% note %}}
L'ISP nous dit qu'un client ne devrait jamais être obligé d'implémenter des fonctions dont il n'a aucune utilité ou qu'il ne sait simplement pas comment l'implémenter.
{{% /note %}}

---
## Interface
- Defines a behavior that the user will depend on
- It means that the user should define it


{{% note %}}
Une interface définit un comportement attendu par un consommateur. Une fonction ou un autre objet.

C'est donc au consommateur de définir le comportement, et donc l'interface, dont il a besoin. Et de se limiter à ce dont il dépend uniquement.
{{% /note %}}

---
## Separation of concerns

{{% columns %}}
{{% column %}}
{{% illustration "img/airplane.jpg" "<a href='https://unsplash.com/@nathanhobbs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Nathan Hobbs</a> on <a href='https://unsplash.com/images/things/airplane?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% block "small" %}}
What is your position and altitude? \
Land on runway #4
{{% /block %}}
{{% /column %}}
{{% column %}}
{{% illustration "img/arrivals.jpg" "<a href='https://unsplash.com/@jeshoots?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>JESHOOTS.COM</a> on <a href='https://unsplash.com/s/photos/arrivals?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% block "small" %}}
Where are you going? \
What time will you arrive?
{{% /block %}}
{{% /column %}}
{{% /columns %}}  

{{% note %}}
S'il y a plusieurs consommateurs, et que ça fait sens, l'interface doit regrouper uniquement ce qui est commun à tous les consommateurs. Charge ensuite à chaque consommateur de définir de nouvelles interfaces pour leurs besoins spécifiques.

Par exemple, un avion peut avoir plusieurs comportements différents : d'un côté comme un objet avec lequel nous pouvons communiquer et afficher sur un radar. Et, de l'autre, comme un objet qui nous indique sa destination et l'heure d'arrivée. Donc de voir le même objet (l'avion) avec deux comportements différents.

Mais en aucun cas, on veut que le panneau d'affichage puisse ordonner à un avion d'atterrir.
{{% /note %}}

---

{{% illustrated "img/three-wise-monkeys.jpg" "Photo by <a href='https://unsplash.com/@ctwtn?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Chris Tweten</a> on <a href='https://unsplash.com/photos/three-wise-monkeys-wLx_WCkWvHg?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Unsplash</a> " %}}
- A consumer should only see what it needs to know
- Interface separation ensures the Need to Know
{{% /illustrated %}}  


{{% note %}}
Isoler les comportements dans des interfaces différentes permet de respecter le besoin d'en connaître. C'est-à-dire qu'on ne doit connaître d'un objet uniquement ce dont on a besoin qu'il fasse pour nous.

Par contre, un objet peut faire plus que ce dont on a besoin. Par exemple, quand on manipule une source données, nous pouvons définir une interface pour lire les informations et une pour écrire. De cette façon, les composants qui font uniquement de la recherche ne pourront effectuer que les opérations de lecture. Alors que les composants chargés de la sauvegarde, suppression, etc. ne pourront faire que de l'écriture.

{{% /note %}}

---


{{% illustrated "img/sprout.jpg" "Photo by <a href='https://unsplash.com/@rkrc?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>qinghill</a> on <a href='https://unsplash.com/photos/tree-trunk-with-green-leaves-x8MZ2MoEKLE?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash'>Unsplash</a> " %}}
- Functional interfaces
    - Interfaces with a single method could be a function
- Could push to think otherwise
{{% /illustrated %}}  

{{% note %}}
Séparer les interfaces peut aussi faire émerger de nouveaux designs : manipuler des interfaces ne possédant qu'une seule méthode mais plusieurs implémentations différentes pousserait à se demander si nous ne pourrions pas les remplacer par des fonctions et des lambdas.
{{% /note %}}
