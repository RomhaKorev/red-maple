+++
title = "The Boy Scout Rule"
outputs = ["Reveal"]
[cascade]
    authors = "Dimitry Ernot"
+++

{{% practices/introduction %}}


---
{{%columns %}}
{{%column %}}
{{% illustration "./img/scout.jpg" "Photo by <a href='https://unsplash.com/@mael_balland?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Mael BALLAND</a> on <a href='https://unsplash.com/s/photos/scout?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% /column %}}
{{% column %}}
{{% quote "Leave Things better than you found them." "Robert Baden Powell, Scouting pioneer" %}}
{{% /column %}}
{{% /columns %}}


---
## Make it obvious
{{% quote "In the face of ambiguity, refuse the temptation to guess" "Tim Peters" %}}

- If something is unclear for you, it will be unclear for someone else
- All code has to be obvious for everybody, not only the expert
- If it's not obvious, you may miss something


---
## Before leaving
- Without an author: the code is obvious

> I can understand what the author did. I can refactor the code alone.

- With an author: the code is logic

> I can understand the code and why the author did it *this* ways.


---
## Before you refactor 1/2
- Avoid the temptation to rewrite everything
- Incremental changes. No massive changes
- The tests pass after each iteration


---
## Before you refactor 2/2
- Be rational: no personal preferences or ego
- A new technology is not a good reason to refactor


---
## The software entropy
- A used code will be modified and will evolve
- When a program is modified, its complexity will increase
- The dev teams has to work actively to reduce the complexity

{{% quote "The second law of thermodynamics, in principle, states that a closed system's disorder cannot be reduced, it can only remain unchanged or increase. A measure of this disorder is entropy. This law also seems plausible for software systems; as a system is modified, its disorder, or entropy, tends to increase. This is known as software entropy." "Ivar Jacobson" %}}

---
{{% conclusion %}}