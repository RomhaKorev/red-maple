+++
title= ""
outputs = ["Reveal"]
+++

{{% sectionbreak "Why we refactor" %}}
{{% /sectionbreak %}}



---
> The second law of thermodynamics, in principle, states that a closed system's disorder cannot be reduced, it can only remain unchanged or increase. A measure of this disorder is entropy. This law also seems plausible for software systems; as a system is modified, its disorder, or entropy, tends to increase. This is known as software entropy.

- A used code will be modified and will evolve
- When a program is modified, its complexity will increase
- The dev teams has to work actively to reduce the complexity


---
- Always leave the campground cleaner than you found it.
- If something is unclear for you, it will be unclear for someone else
- The combination of new and legacy code has to be clean


---
- Code versioning: nothing is lost
- Nobody cares is something if you break something while you are working on it
- Remove the tumour: A healthy limited project will be better than a sick and unstable project