+++
title= ""
outputs = ["Reveal"]
+++

\section{What is Refactoring}


---
## Clean Code principles

- KISS: Keep It Simple, Stupid
- DRY: Don't Repeat Yourself
- YAGNI: You Aren't Gonna Need It
- Favor readability over conciseness: A code is always obvious for the machine


---
## Clean Code principles
KISS: Keep It Simple, Stupid

{{% vspace %}}

- Be humble: your code has to be simple for everyone
- Break down problems into many small problems
- Each method should only solve one little problem, not many uses cases
- Solve the problem, then code it


---
## Clean Code principles
DRY: Don't Repeat Yourself

{{% vspace %}}

- Remove code redundancy
- Don't repeat the knowledge: Work on the patterns not the code
    - First time you have to do something, just do it
    - Second time you have to do the same thing, do it again
    - Third time you have to do it, start refactoring


---
## Clean Code principles
YAGNI: You Aren't Gonna Need It

{{% vspace %}}

- Do the simplest thing that could possibly work
- Don't anticipate. Open your code


---
## Technical debt vs technical mess
Technical Debt

{{% vspace %}}

- A considered and reasoned decision to do quickly
- It's a business-driven decision in line with the long-term strategy
- Only if you will fix it later
- The code is still clean, well-written and tested


---
## Technical debt vs technical mess
Technical Mess

{{% vspace %}}

- Nothing more than a mess
- The code is hard to read, understand, maintain, etc.
- It will not benefit in the future
- It is a loss for the business and the development


---
## Bad reasons to refactor the code

- New technology is not a sufficient reason
- If you don't need it, don't touch it
- Ego and Personal preferences don't matter
