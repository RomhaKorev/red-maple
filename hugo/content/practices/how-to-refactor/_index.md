+++
title = "How to refactor"
subtitle = "Overview"
outputs = ["Reveal"]
authors = "Olivier Albiez & Dimitry Ernot"
+++


whatto.tex


+++
title= ""
outputs = ["Reveal"]
+++

\section{What to refactor}
%## Not obvious code
%## Si beaucoup de bugs
%## Respect of Good practices
%## Duplications
%## Sharing when independent


---
- All code has to be obvious for evrybody, not only the expert
- You need the documentation to understand the code: it should be explicit
- If it's not obvious, you may miss something


---
\item


---


---


---

---
{{% conclusion %}}, 

################################
whento.tex


+++
title= ""
outputs = ["Reveal"]
+++

\section{When to refactor}
%## Before you refactor
%## Continuous refactor
%### During a code review
%### Before adding a feature or fixing a bug


---
- Avoid the temptation to rewrite everything
- Incremental changes. No massive changes
- The tests pass after each iteration
- Be rational: no personal preferences or ego
- A new technology is not a good reason to refactor


---
- Without an author: the code is obvious
- With an author: the code is logic


---
- Refactoring helps you to understand the code
- In a clean code, bugs are obvious
- Much easier to insert a new feature in a clean code

---
{{% conclusion %}}]

Process finished with exit code 0
