+++
title = "SLAP, KISS, YAGNI & DRY Principles"
outputs = ["Reveal"]
authors = "Yann Danot & Dimitry Ernot"
+++

{{% practices/introduction %}}

---
{{% sectionbreak "SLAP" %}}
****S****ingle ****L****evel of ****A****bstraction ****P****rinciple
{{% /sectionbreak %}}

---
## Single Level of Abstraction Principle

- Every statement of a method should be on the same level
  - Don't mix rules with technical statements
- A method should be read smoothly: don't make the devs think
- Need another level of abstraction? Call private methods with an explicit name

---
## SLAP?

{{% snippet "slap" %}}

---
{{% sectionbreak "KISS" %}}
Keep It Simple, Stupid
{{% /sectionbreak %}}

---
## Keep It Simple, Stupid

- ****Be humble****: your code has to be simple for everyone
- Break down problems into many small problems
- Each methods should solve one little problem, not many uses cases
- Solve the problem, then code it

---
{{% sectionbreak "YAGNI" %}}
You Ain't Gonna Need It
{{% /sectionbreak %}}

--- 

## You Ain't Gonna Need It

- Do the simplest thing that could possibly work
- Don't anticipate. ****Open your code****

---
{{% sectionbreak "DRY" %}}
Don't Repeat Yourself
{{% /sectionbreak %}}

---
## Don't Repeat Yourself

- Remove code redundancy
- Don't repeat the knowledge: work on the patterns, not the code
- Rule Of Three:
  - First time you have to do something: just do it
  - Second time you have to do the same thing, do it again
  - Third time your have to do it, ****start refactoring****

---
{{% conclusion %}}