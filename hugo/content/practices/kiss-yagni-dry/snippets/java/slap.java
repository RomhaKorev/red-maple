public void  addOrder(ShoppingCart cart, String userName, Order order) throw SQLException {

       Connection c = null;
       Statement s = null;
       boolean transactionState = false;
       try {
          c = dbPool.getConnection();
          s = c.createStatement();
          transactionState = c.getAutoCommit();
          int userKey = getUserKey(userName, c);
          c.setAutoCommit(false);
          addSingleOrder(order, c, s, userKey);
          int orderKey = getOrderKey(s);
          addLineItems(cart, c, orderKey);
          c.commit(); 
          order.setOrderKeyFrom(orderKey);
       } catch (SQLException sql) {
          c.rollback();
          throw sqlx;
       } finally {
          try {
             c.setAutoCommit(transactionState);
             dbPool(release(c);
             if(s != null){
                s.close();
             }
             if(ps != null){
                ps.close();
             }
          } catch (SQLException ignored){}
       }
    }