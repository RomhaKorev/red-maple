
+++
title = "Mob Programming"
outputs = ["Reveal"]
draft = true
[cascade]
    authors = "Dimitry Ernot"

+++

{{% practices/introduction %}}

---
{{% sectionbreak "The Pilot, the Navigator & the Mob" %}}
{{% /sectionbreak %}}

---
## The Pilot
{{%columns %}}
{{%column %}}
{{% illustration "./img/pilot.png" %}}
{{% /column %}}
{{% column %}}
- The person at the wheel (the keyboard)
- Focusses on short goals
- Ignores the big picture
- *Must talk through what they are writing*
{{% /column %}}
{{% /columns %}}

---
## The Navigator
{{%columns %}}
{{%column %}}
{{% illustration "./img/copilot.png" %}}
{{% /column %}}
{{% column %}}
- The person at the observer position
- Understands where the mob have decided to aim for
- Compiles thoughts from the Mob to give the right directions
- Has the final word on the solutions
{{% /column %}}
{{% /columns %}}

---
## The facilitator
{{%columns %}}
{{%column %}}
{{% illustration "img/crew.jpg" %}}
{{% /column %}}
{{% column %}}
- Checks regularly that everyone is aligned with the code and the quality
- Insures that rules & roles are respected
- Monitors the group for mutual respect & equality
{{% /column %}}
{{% /columns %}}

---
## The Scout
{{%columns %}}
{{%column %}}
{{% illustration "img/scout.jpg" %}}
{{% /column %}}
{{% column %}}
- One or more people
- Explores potential solutions, technical documentations, etc.
- Anticipates challenges and issues
- Works on spikes, mock-ups, etc.
{{% /column %}}
{{% /columns %}}

---
## The Housekeeper
{{%columns %}}
{{%column %}}
{{% illustration "img/housekeeper.jpg" %}}
{{% /column %}}
{{% column %}}
- Fills & manages the TODO list
- Notices the places where the code is impeding the flow
- Cleans the existing code
{{% /column %}}
{{% /columns %}}

---
## The Mob
{{%columns %}}
{{%column %}}
{{% illustration "img/passengers.jpg" %}}
{{% /column %}}
{{% column %}}
- All other people participating to the mob programming session
- Developers, System Engineers, Domain Expert, etc.
- People can join or leave the mob according to the law of the two feet
  - use your two feet and find a place where you can contribute or add values
{{% /column %}}
{{% /columns %}}

---
{{% sectionbreak "Organize yourself" %}}
{{% /sectionbreak %}}

---
## Mob working area
{{%columns %}}
{{%column %}}
{{% illustration "img/group.jpg" %}}
{{% /column %}}
{{% column %}}
- Only one shared computer for programming
- A big screen. Everybody should be able to read the code on the screen
- One keyboard/mouse handled by the Pilot only
{{% /column %}}
{{% /columns %}}

---
## Time Management
{{%columns %}}
{{%column %}}
{{% illustration "./img/clock.png" %}}
{{% /column %}}
{{% column %}}
- Set a timer (usually 15 minutes)
- Pilot & Navigator leaves their role to other random persons in the mob
- The Scout and the Housekeeper can keep their role or not
{{% /column %}}
{{% /columns %}}

---
{{% sectionbreak "Behavior" %}}
{{% /sectionbreak %}}

---
## Act as a team
- Write email & answer to the phone as a team
- Don't work alone. Except for the scout
- Stay focus on the current task

---
## Be humble
- Not everyone is an expert
- Your pair may need more explanation or some time to understand

---
## Avoid Micro-Management
- Work as equals
- Do not give straight instructions (type *this*, create a new class, etc.)

---
{{% conclusion %}}