+++
weight = 1
+++


---
{{% sectionbreak "Strong-Style Pairing" %}}
To onboard new joiners
{{% /sectionbreak %}}

---
## The Golden Rule
{{% quote "For an idea to go from your head into the computer it *MUST* go through someone else's hands" "Llewellyn Falco" %}}


---
{{%columns %}}
{{%column %}}
{{% illustration "img/growth.png" %}}
{{% /column %}}
{{% column %}}
- Similar to the Pilot / Navigator style
- The Navigator is the most experienced
- The Pilot is a novice
- Pilot and Navigator will mostly stay in their role
{{% /column %}}
{{% /columns %}}

---
## Pilot
{{%columns %}}
{{%column %}}
{{% illustration "img/pilot.png" %}}
{{% /column %}}
{{% column %}}


> Trust your Navigator


- You don't understand *what* they are explaining: ask questions
- You don't understand *why* they are telling you something, wait until you have finished the section of code to ask questions
- Challenge the Navigator when they have finished to explain the solution
- Or when the Navigator looks like confused or cannot find a solution
{{% /column %}}
{{% /columns %}}

---
## Navigator
{{%columns %}}
{{%column %}}
{{% illustration "img/copilot.png" %}}
{{% /column %}}
{{% column %}}


> Share your knowledge and train the Pilot


- Give the next instruction when the Pilot is ready to implement it
- Talk in the highest level of abstraction the Pilot can understand
- Adjust your explanations according to the understanding of your Pilot
- Be patient: the Pilot could be less experienced than you
{{% /column %}}
{{% /columns %}}
