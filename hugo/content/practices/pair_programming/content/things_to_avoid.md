+++
weight = 1
+++


---
{{% sectionbreak "Behavior" %}}
{{% /sectionbreak %}}

---
## Stay Focus
- Don't read emails or your phone
- If you have to, wait for the break
- Or be transparent with your pair

---
## Be humble
- Not everyone is an expert
- Your pair may need more explanation or some time to understand

---
## Be Patient
- Apply the 5 seconds rule
        - Wait at least 5 seconds before reporting that the pair did something "wrong"
- Take time to explain as many times as needed
- Don't immediately point out any error or upcoming obstacle
- Don't disrupt the thinking process of your pair

---
## Avoid Micro-Management
- Work as equals
- Do not give straight instructions (type *this*, create a new class, etc.)
