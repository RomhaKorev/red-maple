+++
weight = 1
+++

---
{{% sectionbreak "Pilot & Navigator" %}}
{{% /sectionbreak %}}

---
## Pilot
{{%columns %}}
{{%column %}}
{{% illustration "img/pilot.png" %}}
{{% /column %}}
{{% column %}}
- The person at the wheel (the keyboard)
- Focusses on short goals
- Ignores the big picture
- *Must talk through what she is writing*
{{% /column %}}
{{% /columns %}}

---
## Navigator
{{%columns %}}
{{%column %}}
{{% illustration "img/copilot.png" %}}
{{% /column %}}
{{% column %}}
- The person at the observer position
- Reviews the code on the fly
- Shares her thoughts and directions
- Keeps an eye on the bigger picture
- Takes notes of next steps and potential issues
{{% /column %}}
{{% /columns %}}

---
## Pilot & Navigator
- Target a tiny objective at a time (writing or passing a Unit Test, for example)
- Switch role (and keyboard!) regularly to stay focus
- As Navigator:
    - Leave the details of coding to the Pilot
    - Take notes about next steps, obstacles, ideas, etc. and discuss about them after the current goal has been reached
