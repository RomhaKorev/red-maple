+++
weight = 1
+++


---
{{% sectionbreak "Red Green Refactor" %}}
When a task that can be implemented in a test-driven way
{{% /sectionbreak %}}

---
{{%columns %}}
{{%column %}}
{{% illustration "img/pingpong.png" %}}
{{% /column %}}
{{% column %}}
- Developer A writes a failing Unit test
- Developer B writes the code to pass
- Developers A & B work together to refactor the code if needed
- Developer A & B switch their role: A writes the test and B write the code
{{% /column %}}
{{% /columns %}}
