+++
weight = 1
+++


---
{{% sectionbreak "Pros & Cons" %}}
{{% /sectionbreak %}}

---
## Benefits
- Knowledge sharing
- Code reviews on the fly
- Two ways of thinking combined
- Collective code ownership
- Fast onboarding of new joiners

---
## Challenges
- Too much collaboration and less time for yourself
- Interruptions break the workflow
- At the beginning, a real need to cope with different skills levels
- Informal hierarchies lead to power dynamics
