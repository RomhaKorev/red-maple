+++
weight = 1
+++


---
{{% sectionbreak "Organize yourself" %}}
{{% /sectionbreak %}}

---
## Time Management
{{%columns %}}
{{%column %}}
{{% illustration "img/pomodoro.png" %}}
{{% /column %}}
{{% column %}}
- Set a timer (usually 25 minutes)
- Work without interruption (ask to your Scrum Master to *protect* you)
- Take a short break (5 minutes) when the timer rings
- After 4 *runs*, take a longer break (15 minutes)
    - Grab a tea, get some fresh air, etc.
    - Do not use these breaks for other work, like writing emails
{{% /column %}}
{{% /columns %}}

---
## Plan the day
{{%columns %}}
{{%column %}}
{{% illustration "img/clock.png" %}}
{{% /column %}}
{{% column %}}
- Take time to acknowledge on a schedule
- Agree on how many time you will pair according your other commitments
- Make sure that if you have to leave your pair for a meeting, he can continue without you
    - Don't use your own laptop, for example
- No pending work
- Finish the current task before going to a meeting
{{% /column %}}
{{% /columns %}}
