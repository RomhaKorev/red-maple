+++
weight = 1
+++


---
{{% sectionbreak "Other activities" %}}
Developing is not just coding
{{% /sectionbreak %}}

---
## Plan: Understand the problem 1/3
- Read through the User Story
- Explain it to each other
- Discuss with the Product Owner to clear up potential misunderstanding and unanswered questions
- Check that the User Story respects your Definition Of Ready

---
## Plan: Define a solution 2/3
- Brainstorm the potential solutions
- Share the context with the new joiner
- Choose the best solution all together

---
## Plan: Define your approach 3/3
- Split your solution into small steps
- Order them
- Define how you will test each of them
- Write each task on a sticky note

---
## Research & Explore
- Define all questions that you need to answer
- Divide the questions among you
- Or try to find answers to the same question separately
- Discuss and share your answers and understanding

---
## Documentation
- The pair has to be aligned and aware of the written Documentation
- Write it together
- Or write different parts separately and review it together
