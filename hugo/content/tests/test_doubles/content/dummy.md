+++
weight = 1
+++

{{% sectionbreak "Dummy object" %}}
An instance of some object that can be instantiated easily and with no dependencies
{{% /sectionbreak %}}

{{% note %}}
Le premier double est le dummy. Un objet qu'on peut instancier rapidement sans se poser de questions
{{% /note %}}

---
## How?
- Pass it as the parameter of the method of the SUT
- If any of its methods are invoked:
    - The test should throw an error
    - Trying to invoke a non-existent method will typically have that result

{{% note %}}
Les dummy sont très utiles pour remplacer des paramètres dont la valeur n'a pas de sens pour le test actuel. Par exemple, parce qu'on sait qu'il ne sera pas utilisé mais comme c'est un paramètre, il est obligatoire.

Ils peuvent aussi nous permettre de vérifier que le code ne l'utilise pas parce qu'on sait qu'il ne doit pas l'utiliser.
{{% /note %}}

---
## When?
- As attributes of other objects
- As arguments of methods on the SUT
- As other fixture objects 


{{% note %}}
On utilisera les dummy pour remplacer des paramètres obligatoires mais qui n'ont pas d'incidence sur le test. Par exemple, si on a besoin de créer un User définit par un ID et un nom. Si on sait que le nom ne sert à rien dans le test, on peut mettre n'importe quelle valeur. On s'en fiche.
{{% /note %}}

---
## Why?
- To build real object used in the test
- To avoid obscure Test by leaving out the irrelevant code
- To make it clear which objects and values are not used by the SUT

{{% note %}}
Tout l'intérêt est de ne pas avoir à construire des objets complexes uniquement parce qu'on a besoin de les passer en paramètre de la fonction à tester ou d'un constructeur quelconque.
{{% /note %}}

---
## Dummy value in a test

{{< snippet "dummy_value" >}}

---
## Dummy object in a test

{{< snippet "dummy_object" >}}