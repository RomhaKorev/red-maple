+++
weight = 3
+++

{{% sectionbreak "Spy" %}}
An object capable of capturing indirect output and providing indirect input as needed
{{% /sectionbreak %}}

{{% note %}}
Le spy est là pour tracer les sorties indirectes du système à tester.
{{% /note %}}

---
## How?
- Install the spy so that the SUT uses it instead of the real implementation
- Compares values passed to the spy by the SUT with the values expected by the test

{{% note %}}
Tout comme la stub et le mock, un spy va remplacer une vraie implémentation.
Il va tracer tous les appels qui lui sont faits avec les entrées, les temps, le nombre de fois, etc.
{{% /note %}}

---
## When?
- Inability to observe side-effects of invoking methods on the SUT
- Interaction with an external systemTo deal with test asynchronous mechanisms

{{% note %}}
On l'utilise quand on veut vérifier les effets de bord et surtout des informations sur les effets de bord du système.
{{% /note %}}

---
## Why?
- To capture indirect outputs of the SUT

{{% note %}}
On se servira donc d'un spy pour par exemple vérifier que le système l'a appelé une seule et unique fois. Le séquencement des appels, etc.
{{% /note %}}

---
## A Spy in a test

{{< snippet "spy" >}}