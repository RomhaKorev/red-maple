+++
weight = 4
+++

{{% sectionbreak "Mock Object" %}}
A special case object that mimic real objects. It is capable of controlling both indirect inputs and outputs
{{% /sectionbreak %}}

{{% note %}}
Le mock fonctionne un peu comme un stub mais avec un peu plus d'intelligence.
{{% /note %}}

---
## How?
- Define a mock object that implements the same interface as an object on which the SUT depends
- Configure the mock object with the values with which it should respond to the SUT and the method (complete with the expected arguments) calls expected from the SUT
- Install the mock so that the SUT uses it instead of the real implementation
- Verify the received arguments with the expected arguments

{{% note %}}
Tout comme une stub, un mock va implémenter la même interface que le vrai composant. Par contre, il va avoir un peu plus d'intelligence dans le sens où plutôt que de retourner une valeur en dur, on va pouvoir le paramétrer et lui dire ce qu'il doit retourner dans certaines conditions (si on t'appelle avec tel paramètre, alors retourne telle valeur. La seconde fois qu'on t'appellera, tu retourneras une erreur, etc.)

On va aussi pouvoir enregistrer ce qui se passe indirectement comme les données que le système lui envoie
{{% /note %}}

---
## When?
- Do behavior verification to avoid having an untested requirement
- Inability to observe side-effects of invoking methods on the SUT
- A part of code not under control

{{% note %}}
L'intérêt des mocks est de pouvoir tester des comportements qui pourraient être difficiles à atteindre
Et surtout de valider les effets de bords (est-ce que le système a bien essayé d'enregistrer la bonne information, qu'un événement a été publié, etc.)
{{% /note %}}


---
## Why?
- To fake a single part of the behavior of an object uses by SUT
- To verify the order of different calls

{{% note %}}
On l'utilise pour les mêmes raisons que la stub. Mais lorsqu'on a besoin de faire des validations sur les effets de bords et un peu plus d'intelligence.
{{% /note %}}

---

## ****Caution****
- Facilitate the white box way of testing
- Make tests harder to write and to understand
- Alter the 3-phase test

{{% note %}}
Par contre, il faut bien faire attention avec les mocks : dès le moment où dans nos tests, on commence à ajouter de la logique autour des appels (quand je t'appelle avec tel paramètre, retourne ça), on commence à augmenter l'adhérence de nos tests à l'implémentation actuelle. Donc de les rendre plus difficile à maintenir.

On va aussi devoir ajouter la configuration du mock dans le test. Donc, casser les tests en 3 phases (given, when, then)

{{% /note %}}
---
## A mock in a test

{{< snippet "mock" >}}