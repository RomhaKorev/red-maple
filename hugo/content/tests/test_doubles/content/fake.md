+++
weight = 5
+++

{{% sectionbreak "Fake object" %}}
A fake object is a simpler implementation of real objects.
{{% /sectionbreak %}}

{{% note %}}
Un fake est une implémentation fonctionnelle qui fait exactement comme le vrai composant mais de façon beaucoup plus simple.
{{% /note %}}

---
## How?
- Build a simple implementation of a component or a real object
- Install it so that the SUT uses it instead of the real implementation

{{% note %}}
Imaginons un repository qui accède à une base de données.
Une implémentation plus serait de créer un repository qui stockerait ses données directement en mémoire dans une liste, par exemple.

Il serait donc possible de faire les mêmes opérations de lecture, écriture, suppression, etc. Mais sans véritable base derrière.
{{% /note %}}

---
## When?
- To test infrastructural classes which are beyond our application limit (repositories or queues, unavailable components,…) and that make testing difficult and slow

{{% note %}}
L'intérêt est de pouvoir tester des classes qui dépendent de ressources hors de notre système (une base de données, un fichier, le réseau, etc.) ou dont le traitement peut être long ou difficilement prévisible.

A noter qu'un fake peut aussi servir de première implémentation le temps que le vrai composant soit disponible. Ce qui peut être très utile pour faire des démos.
{{% /note %}}

---
## Why?
- To make tests fast and isolated
- To avoid “do nothing” mocks
- To facilitate the black-box style of testing

{{% note %}}
Les tests doivent s'exécuter très rapidement. On peut faire référence au F des principes FIRST.

Et on fonctionne en boîte noire : on teste un comportement et non pas une implémentation. Donc, on ne peut pas aller jardiner dans un objet. On doit lui passer ses dépendances.
{{% /note %}}
---
## A fake in a test

{{< snippet "fake" >}}