+++
weight = 2
+++

{{% sectionbreak "Stub" %}}
A test-specific object which returns fake data
{{% /sectionbreak %}}

{{% note %}}
Le stub est l'implémentation la plus basique qu'on puisse faire d'un objet, souvent un service.
{{% /note %}}

---
## How?
- Define a test-specific implementation of an interface on which the SUT depends
- Configure it to respond to calls from the SUT with some hardcoded values
- Install the test stub so that the SUT uses it instead of the real implementation

{{% note %}}
Le principe est de construire un objet qui a le même comportement (i.e. la même interface) que l'objet réel mais qui retourne toujours la même valeur
On pourra donc remplacer la vraie implémentation grâce au principe d'inversion de dépendances.

Le stub est très utile pour fixer la date du jour à une date qui sera toujours la même. Et donc de maîtriser le temps dans nos tests.
{{% /note %}}

---
## When?
- Inability to control the indirect inputs of the SUT
- The real component is not available yet or unusable in the development environment

{{% note %}}
On utilisera un stub quand on ne peut contrôler directement les entrées du système qu'on teste. Parce qu'il fait appel à ses dépendances, notamment
Par exemple, un service qui va chercher des données dans un repository

On peut aussi se retrouver dans le cas où le vrai composant n'a simplement pas encore été écrit.
{{% /note %}}

---
## Why?
- To test the behavior of the SUT with various indirect inputs
- To inject values that allow us to get past a particular point in the software

{{% note %}}
Tout l'intérêt est de contrôler les entrées et sorties indirectes du système de façon à injecter des valeurs qu'on maîtrise.
{{% /note %}}

---
## A stub in a test

{{< snippet "stub" >}}