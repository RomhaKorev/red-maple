+++
title = "Test Doubles"
outputs = ["Reveal"]
[cascade]
  authors = "Olivier Albiez, Yann Danot & Dimitry Ernot"
+++

---
{{% introduction %}}

---
## Stunt Double
{{%columns %}}
{{%column %}}
{{% illustration "./img/double.png" %}}
{{% /column %}}
{{% column %}}
{{% quote "A stunt double is a cross between a body double and a stunt performer, specifically a skilled replacement used for dangerous film or video sequences" "Wikipedia" %}}
{{% /column %}}
{{% /columns %}}

{{% note %}}
Au cinéma, ce sont très rarement les acteurs et actrices qui réalisent les cascades dangereuses ou actions délicates.

On fait appel à des doubles cascadeurs pour ça.
{{% /note %}}

---
## Stunt Double
{{%columns %}}
{{%column %}}
{{% illustration "./img/fake.jpg"  "Photo by <a href='https://unsplash.com/@braydona?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Braydon Anderson</a> on <a href='https://unsplash.com/s/photos/costume-dog?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
{{% /column %}}
{{% column %}}
{{% quote "****Test double**** is a generic term for any case where you replace production object for testing purposes" "Martin Fowler" %}}
{{% /column %}}
{{% /columns %}}

{{% note %}}
Hé bien, pour le code, ça va être un peu la même chose.
Quand dans nos tests, nous allons avoir des composants effectuant des opérations un peu délicates comme écrire dans une base de données, accéder à une ressource, etc.

On va donc chercher à remplacer ces composants délicats dans nos tests de façon à les remplacer par d'autres ayant le même comportement mais qui nous permettent de respecter les principes FIRST

Il existe plusieurs sortes de test double. Ils ne sont pas forcément indépendants et nous ne sommes pas obligés de les appliquer. En gros, on a tout à fait le droit de les mélanger.
{{% /note %}}