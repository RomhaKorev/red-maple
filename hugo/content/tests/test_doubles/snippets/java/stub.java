public class GeographicalRepositoryStub implements GeographicalRepository {
    @Override
    public Area getAreaNear(Position ownLocation) {
      return new PlainArea(new Position(50.0, 50.0, 50.0), 50);
    }
  }  

@Test
public void should_add_rules() {
  Classifier classifier = new Classifier(new DummyGeographicalRepository(), dummyPosition)
      .with((aircraft, areasInSector) -> Optional.of(ThreatLevel.Low))
      .with((aircraft, areasInSector) -> Optional.of(ThreatLevel.High));

  Assertions.assertEquals(2, classifier.rules().size());
}