public class GeographicalRepositoryMock implements GeographicalRepository {

    private Map<Position, Area> values = new HashMap<>();

    public void whenReturn(Position input, Area output) {
        values.put(input, output);
    }

    @Override
    public Area getAreaNear(Position ownLocation) {
        if (values.containsKey(ownLocation)) {
            return values.get(ownLocation);
        }
        throw new IllegalArgumentException();
    }
}


@Test
public void should_apply_rules() {
    final Position dummyPosition = new Position(48.7667, -3.05, 50.0);
    final Aircraft dummyAircraft = new Aircraft(UUID.nameUUIDFromBytes("Dummy UUID".getBytes()),
        dummyPosition, new Velocity(100.0, 0.0, 0.0), AircraftType.F35);

    GeographicalRepositoryMock repository = new GeographicalRepositoryMock();

    repository.whenReturn(new Position(48.7667, -3.05, 50.0), new PlainArea(new Position(30.0, 30.0, 5000.0), 100));

    Classifier classifier = new Classifier(repository, dummyPosition)
        .with((aircraft, areasInSector) -> Optional.of(ThreatLevel.Low))
        .with((aircraft, areasInSector) -> Optional.of(ThreatLevel.High));

    Assertions.assertEquals(ThreatLevel.Low, classifier.getThreatLevelFor(dummyAircraft));
}