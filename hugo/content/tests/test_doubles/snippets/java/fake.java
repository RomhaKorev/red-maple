class GeographicalRepositoryFake implements GeographicalRepository {

  private List<Area> areas = new ArrayList<>();

  void add(Area area) {
    areas.add(area);
  }

  @Override
  public Area getAreaNear(Position ownLocation) {
    return null;
  }
}


@Test
public void Should_apply_rules_in_order() {
    final Position dummyPosition = new Position(48.7667, -3.05, 50.0);
    final Aircraft dummyAircraft = new Aircraft(UUID.nameUUIDFromBytes("Dummy UUID".getBytes()),
        dummyPosition, new Velocity(100.0, 0.0, 0.0), AircraftType.F35);

    GeographicalRepositoryFake repository = new GeographicalRepositoryFake();
    repository.add(new PlainArea(new Position(30.0, 30.0, 5000.0), 100));

    Classifier classifier = new Classifier(repository, dummyPosition)
        .with((aircraft, areasInSector) -> Optional.of(ThreatLevel.Low))
        .with((aircraft, areasInSector) -> Optional.of(ThreatLevel.High));

    Assertions.assertEquals(ThreatLevel.Low, classifier.getThreatLevelFor(dummyAircraft));
}
