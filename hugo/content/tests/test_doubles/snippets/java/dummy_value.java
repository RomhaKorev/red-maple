@Test
public void should_return_HIGH_when_is_mig() {
  final UUID dummyUUID = UUID.nameUUIDFromBytes("Dummy UUID".getBytes());
  final Position dummyPosition = new Position(48.7667, -3.05, 50.0);
  final Velocity dummyVelocity = new Velocity(100.0, 0.0, 0.0);
  
  final Area dummyArea = new ComposedArea();
  
  final Aircraft mig = new Aircraft(dummyUUID, dummyPosition, dummyVelocity, AircraftType.Mig);
  
  Optional<ThreatLevel> result = isMig.evaluate(mig, dummyArea);
  
  Assertions.assertEquals(Optional.of(ThreatLevel.High), result);
}