class SpyRule implements EvaluationRule {
    public int count = 0;
   @Override
   public Optional<ThreatLevel> evaluate(Aircraft aircraft, Area CloseArea) {
     ++count;
     return Optional.empty();
   }
 }

 @Test
 public void should_apply_all_rules_until_match() {
   SpyRule spy = new SpyRule();

   Classifier classifier = new Classifier(new GeographicalRepositoryStub(), dummyPosition)
       .with(spy).with(spy)
       .with((aircraft, areasInSector) -> Optional.of(ThreatLevel.High)).with(spy);

   classifier.getThreatLevelFor(dummyAircraft);
   Assertions.assertEquals(2, spy.count);
 }