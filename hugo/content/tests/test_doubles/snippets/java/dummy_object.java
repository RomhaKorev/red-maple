class DummyGeographicalRepository implements GeographicalRepository {
    @Override
    public Area getAreaNear(Position ownLocation) {
      throw new ShouldNotBeCalledException();
    }
  }

  @Test
  public void should_add_rules_in_order() {

    final Position dummyPosition = new Position(48.7667, -3.05, 50.0);
    final Aircraft dummyAircraft = new Aircraft(UUID.nameUUIDFromBytes("Dummy UUID".getBytes()),
        dummyPosition, new Velocity(100.0, 0.0, 0.0), AircraftType.F35);

    Classifier classifier = new Classifier(new DummyGeographicalRepository(), dummyPosition)
        .with((aircraft, areasInSector) -> Optional.of(ThreatLevel.Low))
        .with((aircraft, areasInSector) -> Optional.of(ThreatLevel.High));

    Assertions.assertEquals(2, classifier.rules().size());
  }