
+++
title = "F.I.R.S.T. Principles"
outputs = ["Reveal"]
[cascade]
    authors = "Olivier Albiez, Yann Danot & Dimitry Ernot"
+++


{{% introduction %}}

{{% note %}}
Les principes FIRST sont 5 grands principes dont le but est de simplifier l'écriture et surtout la maintenance du nos tests.
{{% /note %}}

---
{{% sectionbreak "****F.**** I.R.S.T." %}}
Fast
{{% /sectionbreak %}}

{{% note %}}
Le "F" pour Fast.

Les tests doivent être rapides à exécuter.

{{% /note %}}

---
- Running tests should ****NOT**** be time-consuming
- Setup, teardown & tests should execute really fast (in milliseconds)
- Even if there are thousand of them

{{% note %}}
Jouer les tests ne doit pas être un frein. Chaque fois qu'on les lance, c'est du temps qu'on doit passer à attendre le résultat. Donc du temps perdu avant de savoir si ce qu'on a codé est valide ou non.

Plus les tests passeront vite, plus on sera efficace et moins on rechignera à les lancer.

Grosso modo, un test doit s'exécuter en quelques millisecondes. 
Peu importe le nombre de tests qu'on a, ils ne devraient pas être un point bloquant et on devrait pouvoir les lancer à n'importe quel moment pour nous assurer qu'on n'a pas introduit de régressions ou que nous sommes bien alignés avec ce que nous sommes en train de coder.
{{% /note %}}

---
{{% sectionbreak "F. ****I.**** R.S.T." %}}
Independent / Isolated

{{% note %}}
I pour indépendant ou isolé.

Les tests doivent être indépendants les uns des autres.
{{% /note %}}

{{% /sectionbreak %}}

---
- Success of a test does not dependent of the order of run
- A test should NOT depend of external resources (database, network, file system, etc.)


{{% note %}}
On doit pouvoir lancer les tests seuls ou pas. D'abord un puis un autre.
En gros, chaque test doit être indépendant des autres et surtout ne pas dépendre de l'ordre d'exécution.

Les tests doivent également ne dépendre que de ce qu'ils définissent eux-mêmes. C'est-à-dire qu'ils ne doivent pas reposer sur des ressources extérieures comme une base de données, le réseau, etc.
Le but est de s'assurer qu'un test qui échoue le fera pour de bonnes raisons. Et non pas parce que quelqu'un a retouché un fichier ou qu'on n'a pas accès au réseau

Cela signifie aussi que notre code qu'on veut tester ne doit pas manipuler directement les ressources du système. Mais passer par des services qui le feront pour lui et qu enous pourrons remplacer par un double

{{% /note %}}

---
{{% sectionbreak "F.I. ****R.**** S.T." %}}
Repeatable
{{% /sectionbreak %}}

{{% note %}}
R pour répétable.

Un test doit pouvoir être lancé à tout moment et donner le même résultat partout tout le temps
{{% /note %}}

---
- A test method should ****NOT**** depend on any data in the current environment
- Deterministic results
  - they should yield the same results every time and at every location
- No dependency on date/time or random functions output
- Each test should setup or arrange it's own data

{{% note %}}
Il n'y a rien de pire qu'un test qui échoue sur la machine voisine et pas sur la nôtre ou ne pas pouvoir les lancer quand on veut.
Ou de devoir le jouer plusieurs fois avant qu'il ne passe.

Un test doit donc être déterministe : pour une entrée donnée (le code), le résultat doit toujours être le même.

Ca veut dire qu'on va devoir maîtriser le temps ou l'aléatoire. Par exemple, en s'assurant que si une fonction dépend de l'heure, alors cette heure sera toujours la même lorsque notre test s'exécutera. Donc, le code qu'on veut tester ne doit pas dépendre directement de l'horloge système mais d'un service qu'on pourra remplacer par un double dans nos tests. On en revient au principe précédent sur le fait qu'on doit s'arranger pour ne pas dépendre du système dans nos tests.

{{% /note %}}

---
{{% sectionbreak "F.I.R. ****S.**** T." %}}
self-validating
{{% /sectionbreak %}}

{{% note %}}
S pour self-validating

Un test doit valider lui-même son résultat
{{% /note %}}

---
- No manual inspection required to check whether the test has passed or failed
- The test should carry its own assertions

{{% note %}}
A la fin d'un test, on doit savoir automatiquement et immédiatement s'il a réussi ou non.
Il n'est pas question de devoir aller vérifier nous-mêmes si ça s'est bien passé ou non (par exemple, aller jeter un oeil dans une base de données).
Ni devoir lancer un autre outil qui viendra valider le résultat
{{% /note %}}

---
{{% sectionbreak "F.I.R.S. ****T.****" %}}
Timely / Thorough
{{% /sectionbreak %}}

{{% note %}}
T pour Timely ou Thorough

Un test doit être à la fois pertinent et être écrit au bon moment. Autrement dit être écrit dès qu'on peut
{{% /note %}}


---
- A test should cover every use case scenario and ****NOT**** just aim for 100\% of code coverage
- Written about the same time as code under test (with TDD, tests written first, etc.)

{{% note %}}
Les tests sont notre filet de sécurité. Ils sont là pour nous assurer que les modifications que nous faisons sur le code ne casse pas l'existant (régression) voire sont alignées avec notre objectifs.
Ils doivent donc couvrir les scénarios de chaque cas d'utilisation et être suffisemment pertinents pour nous assurer que nous sommes dans de bonnes conditions pour mofidier le code sans avoir peur de casser quelque chose.

Et pour cela, il faut écrire les tests au fur et à mesure que nous construisons notre code. On peut également se tourner vers des approches comme le TDD.
{{% /note %}}

---
{{% sectionbreak "Examples" %}}
{{% /sectionbreak %}}

---
{{< snippet "1" >}}

---
{{< snippet "2" >}}

---
{{< snippet "3" >}}

---
{{< snippet "4" >}}

---
{{% conclusion %}}