
+++
title = "Unit Tests & Test Strategy"
outputs = ["Reveal"]
[cascade]
    authors = "Olivier Albiez & Yann Danot"
+++

{{% introduction %}}

---
{{% sectionbreak "What do you mean by test?" %}}
{{% /sectionbreak %}}

---
{{% sectionbreak "And by unit test?" %}}
{{% /sectionbreak %}}

---

---
## Unit test
{{% quote "In computer programming, unit testing is a software testing method by which individual ****units**** of source ****code****, sets of one or more computer program ****modules*** together with ****associated control data, usage procedures, and operating procedures****, are tested to determine whether they are fit for use." "" %}}

---
![Basic Architecture](img/architecture.svg)

---
![Unit Tests](img/pyramid-unit.svg)

---
![Unit Tests](img/architecture-unit.svg)

---
![Unit Tests](img/pyramid-integ.svg)

---
![Integration Tests](img/architecture-integ.svg)

---
![Unit Tests](img/pyramid-e2e.svg)

---
![E2E Tests](img/architecture-e2e.svg)

---
{{% sectionbreak "Test Strategy" %}}
{{% /sectionbreak %}}

---
![pyramid](img/pyramid.svg)

---
![Technical Strategy](img/strategy-technical.svg)

---
![Functional Strategy](img/strategy-functional.svg)

---
{{% sectionbreak "Why are we unit testing ?" %}}
{{% /sectionbreak %}}

---
## Our daily work is about ****learning the business****

#### Captures the understanding of business rules

Development is a learning process, we learn the business rules which may be ambiguous or contradictory, and ~~may~~ will change.

---
## Our daily work is about ****keeping control****

#### Modular and decoupled design

To be testable, it is necessary to inverse the dependencies and keep things small.

---
## Our daily work is about ****keeping control****

#### Allows evolutions

An unit tests suite can easily and quickly show regression or impact of changes.

---
## Our daily work is about ****keeping control****

#### Provides quick feedback

An unit tests suite is quicker than other tests.

---
## Summary
- To be able to change
- To always have a working software
- To make behaviors sharable and readable
#### To be agile

---
{{% conclusion %}}