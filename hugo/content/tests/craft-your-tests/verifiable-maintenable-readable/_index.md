+++
title = "Verifiable, Maintenable & Readable Tests"
outputs = ["Reveal"]
authors = "Yann Danot, Olivier Albiez"
+++


{{% introduction %}}

---
{{% sectionbreak "Verifiability" %}}
{{% /sectionbreak %}}

---
## Assertions

A test should have at least one assertion

- Explicit what do we want to test
- Depends on the way ware writing it
  - Story telling
  - Test case
- We custom our assertions to improve readability

---
## Challengeable

Our test can be challenged by changing a test case

- What if we change this input?
- What if we change this output?
- What if we change this call?

****Our test should fail****

---
{{< snippet "challengeable-1" >}}
{{< snippet "challengeable-2" >}}

---
{{% conclusion %}}