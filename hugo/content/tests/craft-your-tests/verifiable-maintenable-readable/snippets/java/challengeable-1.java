@Test
public void withdrawal_should_be_allowed_to_anyone_who_has_money(){
    given_a_standard_customer() .with_account_balance(Money.of(200));
    when_he_withdraws(Money.of(20));
    then_his_account_balance_should_be(Money.of(180));
}