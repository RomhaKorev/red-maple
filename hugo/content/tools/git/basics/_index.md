+++
title= "Git Basics"
subtitle = "By Matthieu Archez"
outputs = ["Reveal"]
authors="Matthieu Archez"
draft = true
+++


{{% introduction "short" %}}

---
{{% sectionbreak "Disclaimer" %}}
{{% /sectionbreak %}}

---
****This is not a tutorial on how to use Git****

It’s just a developer’s view of how to use it for my needs and lot’s of it is subjective.

---
****There are a lot of ways to use Git to get the exact same results****

- Git IS misleading and not as easy as some people might say
  - You cannot create branch with git branch !
  - You have to use git checkout to :
    - remove a file from the index
    - create a branch
    - move your pointer to another branch
    - ...

{{% floatingimage "img/disclaimer.png" "bottom-right" %}}

---
****Best way to learn is to practice in a repository that you can mess with****

---
{{% sectionbreak "/Disclaimer" %}}
{{% /sectionbreak %}}

---
## git config


{{% bracket title="File used to personalize your use of Git" %}}
- Your identity
- Your editors and merge/diff tools
- Your aliases (please use them, I beg you)
{{% /bracket %}}

{{% vspace %}}

Each repository can have its own git config that will amend the global configuration (in your `$HOME/.gitconfig`)

---
## Basics : branch manipulation

{{% bracket title="Create a repository ?" %}}
```bash
> git init
```
{{% /bracket %}}

{{% bracket title="Clone a remote repository ?" %}}
```bash
> git clone <repository_url> <local_name>
```
{{% /bracket %}}

{{% bracket title="Create a branch and basculate HEAD on that branch ?" %}}
```bash
> git checkout -b <branch_name>
```
{{% /bracket %}}

{{% bracket title="Move to an existing branch ?" %}}
```bash
> git checkout <branch_name>
```
{{% /bracket %}}

{{% bracket title="Delete a local branch ?" %}}
```bash
> git branch -D <branch_name>
```
{{% /bracket %}}


---
## Basics : fetching and merging

{{% bracket title="Get all the latests change from origin ?" %}}
```bash
> git fetch --all
```
{{% /bracket %}}

{{% bracket title="Merging your local branch..." %}}
```bash
# with another local branch:
> git merge <target_branch_name>

#with a remote branch :
> git merge origin <target_branch_name>
```
{{% /bracket %}}

{{% bracket title="Fetching *and* merging a remote branch into your local branch" %}}
```bash
> git pull origin <target_branch_name>
```
{{% /bracket %}}

{{% bracket title="****To avoid****" sub="If you have different upstream repositories, avoid this syntax" %}}
```bash
> git pull
# will git pull <remote_server> <local_branch_name>
```
{{% /bracket %}}

---
## Basics : adding, undoing and commiting

{{% bracket title="Add a file to the index ?" %}}
```bash
> git add <file_name>
```
{{% /bracket %}}

{{% bracket title="To add all the files in a directory" %}}
```bash
> git add <directory_name>
```
{{% /bracket %}}

{{% bracket title="Delete local changes of a file ?" %}}
```bash
> git checkout <file_name | dir_name>
```
{{% /bracket %}}

{{% bracket title="Remove a file from the index ?" %}}
```bash
> git reset HEAD <file_name | dir_name>
#All the indexed files
> git reset HEAD
```
{{% /bracket %}}

{{% bracket title="Commit the changes in your index" %}}
```bash
> git commit -m “Your clear and thoughtful commit message”
```
{{% /bracket %}}

{{% bracket title="Modify your last commit message and content" %}}
```bash
> git commit --amend
```
{{% /bracket %}}


---
## Basics : pushing
{{% bracket title="Pushing your branch to your upstream server ?" %}}
```bash
> git push -u <remote_alias> <local_branch>
# Modify the name of the branch on the server : 
> git push -u <remote_alias> <local_branch>:<remote_branch>
```
{{% /bracket %}}

{{% bracket title="****TO NOT DO****" %}}
```bash
git push
# Will push your local branch to your set upstream server 
# Unclear remote target and branch name, avoid using that
```
{{% /bracket %}}

{{% floatingimage "img/git-push.png" "bottom-left" %}}

---
## More basics...

{{% bracket title="Modifying the name of an already indexed file ?" %}}
```bash
> git mv <file_name>
```
{{% /bracket %}}
{{% bracket title="Seeing the changes of a file between two commits ?" %}}
```bash
> git diff <commit_id_1> <commit_id_2> -- <file_name>
```
{{% /bracket %}}
{{% bracket title="Seeing the content of a commit ?" %}}
```bash
> git show <commit_name> [--name-only]
```
{{% /bracket %}}
{{% bracket title="Seeing who put that “toto” trace in your beloved router file ?" %}}
```bash
git blame <file_name>
# IntelliJ also does it through the Annotate feature
```
{{% /bracket %}}

![image](img/git-blame.png)

---
## Cherry picking

{{%command bash "Apply any commit to your local branch" %}}
> git cherry-pick <commit-sha>
{{% /command %}}

![image](img/cherry-pick.png)


---
## Interactive rebase

{{% columns %}}
{{% column %}}
- Tool to rework your commits
    - by merging some of them (squash)
    - by deleting some of them
    - by amending them 
{{% /column %}}
{{% column  centered = true %}}
```bash
> git rebase -i <commit_pointer>~<range>

> git merge --squash <target_branch_name>
```
{{% /column %}}
{{% /columns %}}

![image](img/rebase.png)


---
## Reflog

- Git Ctrl-Z
- Contains the history of the lasts commit, rebase, reset, merge… on your repository
- ```bash > git reflow show```
- The reset command permit to go to an anterior state
    - ```bash > git reset HEAD@{<pointer>}```
- Cancel last commit and remove the changes from the index
- ```bash > git reset HEAD^ # (^ = précédent !)```


---
{{% columns %}}
{{% column %}}
![image](img/reflog-1.png)
{{% /column %}}
{{% column %}}
![image](img/reflog-meme.png)
{{% /column %}}
{{% /columns %}}

---
## A few rules of thumb

- Commit often, perfect later, push once
- ```bash git add .``` is ****not**** your friend, be careful what you’re adding to your index
- ```bash git push --force``` is rarely a good idea
  - Be sure you really know what you are doing before rewriting public history
- Think twice before you add very big files to your index
    - git is not an archive, use other tools to store big binaries or data files
    - Cloning and fetching becomes way longer if you add too much incompressible files
- ****everything**** you push will be in your git history forever, even if you delete and git remove it afterwards
    - if you pushed a key or a secret either change it or use ****this**** to your own risk


---
## Appendix
{{% columns %}}
{{% column %}}
An alias for your .gitconfig

```bash
lg = log --pretty=oneline --abbrev-commit \
 --graph --decorate --branches --remotes
```
{{% vspace %}}
Food for thoughts

[https://stevelosh.com/blog/2013/04/git-koans/](https://stevelosh.com/blog/2013/04/git-koans/)
{{% /column %}}
{{% column %}}
![image](img/appendix-meme.png)
{{% /column %}}
{{% /columns %}}



---
{{% conclusion %}}



