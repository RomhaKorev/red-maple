function openSnippet(evt, language, snippet) {

    console.log(language);
    // Declare all variables
    var i, tabcontent, tablinks;
  
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      if (tabcontent[i].id.endsWith(language)) {
        tabcontent[i].style.display = "block";
      } else {
        tabcontent[i].style.display = "none";
      }
    }
  
    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      if (tablinks[i].id.endsWith(language)) {
        tablinks[i].className = tablinks[i].className += " active";
      } else {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
    }
  }


function initDefaultLanguage() {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  if (urlParams.has('language')) {
    const defaultLanguage = urlParams.get('language');
    
    openSnippet(null, defaultLanguage, null);
  }
}