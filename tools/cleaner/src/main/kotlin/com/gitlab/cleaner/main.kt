package com.gitlab.cleaner

import java.nio.file.Files
import java.nio.file.Path

fun String.`find images`(): List<String> {
    val allRx = listOf(
        """\{\{%\s*illustrat(?:ion|ed)\s+"([^"]+)"(?:\s*"[^"]+")?\s*%}}""".toRegex(),
        """\{\{%\s*illustrat(?:ion|ed).+file="([^"]+)".+%}}""".toRegex(),
        """!\[.*]\((.+)\)""".toRegex(),
        """\{\{% floatingimage "([^"]+)" ".*" %}}""".toRegex()
    )

    return this.split("\n").mapNotNull {line ->
        allRx.firstNotNullOfOrNull { it.find(line)?.groupValues?.get(1) }
    }

}

class MDFile(val path: Path, images: List<String>) {
    val images: List<Path>
    val root : Path
    init {
        root = if (path.endsWith("_index.md")) {
            path.parent
        } else {
            path.parent.parent
        }

        this.images = images.map { root.resolve(it).normalize() }
    }

    override fun toString(): String {
        return "$root $path: $images"
    }
}

fun main(args: Array<String>) {
    val mdFiles = Files.walk(Path.of(args.first()))
        .filter { item -> Files.isRegularFile(item) }
        .filter { item -> item.toString().endsWith(".md") }
        .toList()
        .map { file -> MDFile(file, file.toFile().readText().`find images`()) }
    val referenced = mdFiles.flatMap { it.images }


    val allImagesFiles = Files.walk(Path.of(args.first()))
        .filter { item -> Files.isRegularFile(item) }
        .filter { item -> item.toString().endsWithOneThese(".png", "jpg", "jpeg") }

        referenced.filter { !it.toFile().exists() }
        .forEach { println("$it does not exist") }

    println("NOT USED:")
    allImagesFiles.filter { it !in referenced }.toList()
        .groupBy { it.parent }
        .forEach { println("${it.key}\n${it.value.joinToString("\n") {"\t$it"}}") }
}

private fun String.endsWithOneThese(vararg extensions: String) = extensions.firstOrNull { this.endsWith(it) } != null
