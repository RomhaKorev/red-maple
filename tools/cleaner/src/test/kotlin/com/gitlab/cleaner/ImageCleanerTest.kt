package com.gitlab.cleaner

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ImageCleanerTest {

    @Test
    fun `should find image`() {
        `in file` ("""
            {{% illustration "img/image.png" %}}
        """).`should find` ("img/image.png")
    }

    @Test
    fun `should find image with author`() {
        `in file` ("""
            {{% illustration "img/image.png" "author" %}}
        """).`should find` ("img/image.png")
    }

    @Test
    fun `should find all images`() {
        `in file` ("""
            {{% illustration "img/control-tower.jpg" "<a href='https://unsplash.com/@rob_vesseur?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Rob Vesseur</a> on <a href='https://unsplash.com/photos/HCvXRe1-vYM?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
            {{% block "tiny" %}}
            Track airplane \
            Detect emergency
            {{% /block %}}
            {{% columns %}}
            {{% column width="50" %}}
            {{% illustration "img/radar.jpg" "<a href='https://unsplash.com/@stellanj?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Stellan Johansson</a> on <a href='https://unsplash.com/photos/1PP0Fc-KSd4?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
            {{% /column %}}
            {{% column width="50"%}}
            {{% illustration "img/cockpit.jpg" "<a href='https://unsplash.com/@cikedondong?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Abby AR</a> on <a href='https://unsplash.com/photos/1uwzsExrKzY?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
            {{% /column %}}
            {{% columns %}}
        """).`should find` ("img/control-tower.jpg", "img/radar.jpg", "img/cockpit.jpg")
    }

    @Test
    fun `should find image with named parameters`() {
        `in file` ("""
            {{% illustration file="img/image1.png" author="author" %}}
            {{% illustration author="author" file="img/image2.png" %}}
        """).`should find` ("img/image1.png", "img/image2.png")
    }

    @Test
    fun `should find floating image`() {
        `in file` ("""
            {{% floatingimage "img/image1.png" "author" %}}
        """).`should find` ("img/image1.png")
    }

    @Test
    fun `should find image in illustrated`() {
        `in file` ("""
            {{% illustrated "img/airplane.jpg" "Photo by <a href='https://unsplash.com/@nathanhobbs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Nathan Hobbs</a> on <a href='https://unsplash.com/images/things/airplane?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText'>Unsplash</a>" %}}
            Focusing on essential info at design level \
            Regrouping key concepts
            {{% /illustrated %}}
        """).`should find` ("img/airplane.jpg")
    }

    @Test
    fun `should find image in markdown`() {
        `in file`("""
           ![Geek And Poke](img/comments.png) 
        """).`should find`("img/comments.png")
    }

    private fun `in file`(content: String): () -> String {
        return  { content }
    }

    fun (() -> String).`should find`(vararg s: String) {
        val content = this().trimIndent()

        val images: List<String> = content.`find images`()

        assertThat(images).containsExactlyInAnyOrder(*s)
    }
}
