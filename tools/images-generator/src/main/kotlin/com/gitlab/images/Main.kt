package com.gitlab.images

import com.gitlab.kigelia.dsl.core.Tag
import io.github.classgraph.ClassGraph
import java.io.File
import java.lang.RuntimeException
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.reflect.jvm.kotlinFunction


class SvgGenerator(filename: String, val generator: () -> Tag) {
    private val filename: Path = Paths.get("target", "generated", "svg", "$filename.svg")
    operator fun invoke() {
        generator().save()
    }

    private fun Tag.save() {
        val file = File(filename.toUri())
        prepare(file)
        println("Saving ${filename.toAbsolutePath()}")
        file.printWriter().use { out ->
            out.println(this.render())
        }
    }

    private fun prepare(file: File) {
        val path = file.toPath()
        val directory = path.parent.toFile()
        if (directory.exists())
            return
        println("Creating directory $directory")
        directory.mkdirs()
    }
}

@Throws(Exception::class)
fun getAllImages(): List<SvgGenerator> {
    val annotation = Image::class.java
    val pkg = annotation.`package`.name
    val routeAnnotation = annotation.canonicalName

    return ClassGraph()
        .enableAllInfo()
        .acceptPackages(pkg)
        .scan().use { scanResult ->
            scanResult.getClassesWithMethodAnnotation(routeAnnotation).flatMap { routeClassInfo ->
                routeClassInfo.methodInfo
                    .filter { method ->
                        method.hasAnnotation(annotation)
                    }
                    .mapNotNull { method ->
                        val filename = method.getAnnotationInfo(routeAnnotation).parameterValues.find { it.name == "filename" }?.value ?: throw IllegalArgumentException("Not filename")
                        val function = method.loadClassAndGetMethod().kotlinFunction ?: throw RuntimeException("Function is null")
                        val params = if (function.parameters.size == 1)
                                        listOf("black")
                                     else emptyList()
                        val u : () -> Tag = { function.call(*params.toTypedArray()) as Tag }
                        SvgGenerator(filename as String, u)
                    }
            }
        }
}


fun main() {
    getAllImages()
        .forEach { function ->
            function()
        }
}
