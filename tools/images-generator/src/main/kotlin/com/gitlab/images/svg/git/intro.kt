package com.gitlab.images.svg.git

import com.gitlab.images.Image
import com.gitlab.kigelia.dsl.core.Tag
import com.gitlab.kigelia.gitflow.domain.flow
import com.gitlab.kigelia.gitflow.painter.toSvg

@Image("git/feature-branch-intro")
fun featurebranch(color: String): Tag {
    return flow {
        spareBranch("local/feature-1")
        commit("local/master")
        `in parallel` {
            commit("local/feature-1")
            commit("local/feature-1")
            commit("local/feature-1")
            commit("local/feature-1")
            commit("local/feature-1")
        }
        `in parallel` {
            commit("local/feature-2")
            commit("local/feature-2")
            commit("local/feature-2")
        }
        "local/feature-2".push("local/master")
        "local/master".tag("v1.0")
        "local/feature-1".push("local/master")
        "local/master".tag("v2.0")
    }.toSvg()
}
