package com.gitlab.images.svg

import com.gitlab.images.Image
import com.gitlab.kigelia.dsl.core.Tag
import com.gitlab.kigelia.dsl.svg.elements.g
import com.gitlab.kigelia.dsl.svg.helpers.boxedText
import com.gitlab.kigelia.dsl.svg.helpers.connectors.ConnectorType
import com.gitlab.kigelia.dsl.svg.helpers.connectors.connect
import com.gitlab.kigelia.dsl.svg.svg

@Image("oop/polymorphism")
fun polymorphism(color: String): Tag {
    return svg {
        style {
            "font-size"("18px")
        }
        boxedText {
            id("movingobject")
            box {
                "stroke-width"("2")
                "stroke"(color)
            }
            geometry {
                x(77)
                y(2)
                width(150)
                height(40)
            }
            text("MovingObject") {
                "fill"(color)
            }
        }

        boxedText {
            id("explorer")
            box {
                "stroke-width"("2")
                "stroke"(color)
            }
            geometry {
                moveTo(40.below("movingobject"))
                resizeLike("movingobject")
            }
            text("Explorer") {
                "fill"(color)
            }
        }

        boxedText {
            id("searching")
            box {
                "stroke-width"("2")
                "stroke"(color)
            }
            geometry {
                moveTo(40.rightOf("explorer"))
                resizeLike("explorer")
            }
            text("Searching") {
                "fill"(color)
            }
        }

        boxedText {
            id("rover")
            box {
                "stroke-width"("2")
                "stroke"(color)
            }
            geometry {
                moveTo(40.below("explorer"))
                moveTo((-40).rightOf("explorer"))
                resizeLike("movingobject")
            }
            text("Rover") {
                "fill"(color)
            }
        }
        boxedText {
            id("drone")
            box {
                "stroke-width"("2")
                "stroke"(color)
            }
            geometry {
                resizeLike("movingobject")
                moveTo((-40).leftOf("explorer"))
                moveTo(40.below("explorer"))
            }
            text("Drone") {
                "fill"(color)
                "font-size"("px")
            }
        }
        g {
            "stroke-width"("1")
            "stroke"(color)
            connect("movingobject".bottom(), "explorer".top(), type = ConnectorType.Squared)
            connect("explorer".bottom(), "drone".top(), type = ConnectorType.Squared)
            connect("explorer".bottom(), "rover".top(), type = ConnectorType.Squared)
            connect("searching".bottom(), "rover".top(), type = ConnectorType.Squared) {
                text("foobar")
            }
        }
    }
}
