package com.gitlab.images.svg


import com.gitlab.images.Image
import com.gitlab.kigelia.dsl.core.Point
import com.gitlab.kigelia.dsl.core.Tag
import com.gitlab.kigelia.dsl.svg.elements.g
import com.gitlab.kigelia.dsl.svg.elements.polyline
import com.gitlab.kigelia.dsl.svg.svg

@Image("misc/home")
fun home(color: String): Tag {
    return svg {
        growBy {
            horizontal(5.0)
            vertical(5.0)
        }
        g {
            polyline {
                points(
                        Point(5.0, 20.0),
                        Point(20.0, 5.0),
                        Point(35.0, 20.0)
                )
            }
            polyline {
                points(
                        Point(10.0, 22.0),
                        Point(10.0, 35.0),
                        Point(20.0, 35.0),
                        Point(30.0, 35.0),
                        Point(30.0, 22.0)
                )
            }
            "stroke-linejoin"("round")
            "stroke-width"("4")
            "stroke"(color)
            "stroke-linecap"("round")
            "fill"("none")
        }
    }
}
