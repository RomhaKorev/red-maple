package com.gitlab.images.svg.git

import com.gitlab.images.Image
import com.gitlab.kigelia.dsl.core.Point
import com.gitlab.kigelia.dsl.core.Tag
import com.gitlab.kigelia.dsl.svg.SvgElement
import com.gitlab.kigelia.dsl.svg.elements.g
import com.gitlab.kigelia.dsl.svg.elements.text
import com.gitlab.kigelia.dsl.svg.svg
import com.gitlab.kigelia.gitflow.domain.flow
import com.gitlab.kigelia.gitflow.painter.options.AuthorBasedColorSelector
import com.gitlab.kigelia.gitflow.painter.options.Options
import com.gitlab.kigelia.gitflow.painter.toPainter


@Image("git/master-only-intro")
fun masteronly(): Tag {
    return svg {
        geometry {
            width(36)
            height(154)
        }

        g {
            flow {
                repository("origin") {
                    id = "repository"
                    branch("master") {
                        prior = true
                        later = true
                    }
                }
                commit("origin/master") by "Igor"
                commit("origin/master") by "Igor"
                commit("origin/master") by "Igor"
                commit("origin/master") by "Igor"
                commit("origin/master") by "Igor"
            }.toPainter().paint(this, Point(0.0, 0.0), Options().with(AuthorBasedColorSelector()).withShowInformation(false))

            val position = 10.below("origin-master-1")
            val repository = findSibling("repository")
            repository?.text {
                x(position.x)
                y(position.y)
                geometry {
                    height(30)
                }
                - "Igor"
            }
        }
    }
}

