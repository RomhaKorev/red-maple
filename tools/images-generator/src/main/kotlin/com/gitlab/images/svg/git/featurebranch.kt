package com.gitlab.images.svg.git

import com.gitlab.images.Image
import com.gitlab.kigelia.dsl.core.Tag
import com.gitlab.kigelia.gitflow.domain.flow
import com.gitlab.kigelia.gitflow.domain.model.Flow
import com.gitlab.kigelia.gitflow.painter.toSvg

private fun base(): Flow {
    return flow {
        repository("david") {
            branch("master") {
                later = true
                prior = true
            }
        }
        repository("origin") {
            branch("feature-1") {}
            branch("master") {
                later = true
                prior = true
            }
        }
    }
}


private fun Flow.step1(): Flow {
    commit("origin/master")
    "david/master".pull("origin/master")
    "david/master".push("david/feature-1")
    `in parallel` {
        commit("david/feature-1")
        commit("david/feature-1")
        commit("david/feature-1")
    }
    return this
}

private fun Flow.step2(): Flow {
    "david/feature-1".push("origin/feature-1")
    return this
}

private fun Flow.step3(): Flow {
    commit("david/feature-1")
    commit("david/feature-1")
    "david/feature-1".push("origin/feature-1")
    "origin/feature-1".push("origin/master")
    return this
}

@Image("git/feature-branch-1")
fun `1`(color: String): Tag = base().step1().toSvg()


@Image("git/feature-branch-2")
fun `2`(color: String): Tag = base().step1().step2().toSvg()

@Image("git/feature-branch-4")
fun `3`(color: String): Tag = base().step1().step2().step3().toSvg()


@Image("git/feature-branch-4")
fun `4`(color: String): Tag = flow {
    repository("matthieu") {
        branch("feature-2") {
            later = true
        }
    }
    repository("origin") {
        branch("feature-2") {
            later = true
        }
        branch("master") {
            later = true
            prior = true
        }
        branch("feature-1") {
            prior = true
        }
    }

    commit("origin/feature-1")
    commit("matthieu/feature-2")
    commit("matthieu/feature-2")
    commit("origin/feature-1")
    commit("matthieu/feature-2")
    "origin/feature-1".push("origin/master")
    "origin/master".push("matthieu/feature-2")
    commit("matthieu/feature-2")
    commit("matthieu/feature-2")
    "matthieu/feature-2".push("origin/feature-2")
}.toSvg()
