package com.gitlab.images

@Target(AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY
)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
internal annotation class Image(val filename: String = "")
