import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application

@Composable
fun EquiRow() {

    val columns: MutableList<String> by remember { mutableStateOf(mutableListOf("Booh")) }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        repeat(columns.count()) {index ->
            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxHeight()
                    //.background(Color.Blue)
            ) {
                OutlinedTextField(
                    value = columns[index],
                    onValueChange = { text -> columns[index] = text },
                    modifier = Modifier.fillMaxWidth().fillMaxHeight().wrapContentHeight().padding(16.dp),
                    singleLine = false
                )
            }
        }
    }
}


@Composable
@Preview
fun App() {
    var text by remember { mutableStateOf("Hello, World!") }

    MaterialTheme {
        EquiRow()

    }
}

fun main() = application {
    Window(onCloseRequest = ::exitApplication) {
        App()
    }
}
