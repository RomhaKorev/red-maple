package com.gitlab.hexaofcontent.contentbrowsing

import com.gitlab.hexaofcontent.contentbrowsing.BrowseContent
import com.gitlab.hexaofcontent.contentbrowsing.ContentRepository
import com.gitlab.hexaofcontent.contentbrowsing.Slides
import com.gitlab.hexaofcontent.contentbrowsing.Title
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test


class FileRepositoryStub: ContentRepository {
    override fun invoke(path: ContentRepository.Path): List<ContentRepository.Path> {
        return listOf(ContentRepository.Path("/foo/bar/slides", "bar/slides", isCategory = false, isSlides = true))
    }

    override fun getContent(path: ContentRepository.Path, vararg more: String): List<String> {
        return """
            +++
            title = "A subject"
            outputs = ["Reveal"]
            authors = "Foo Bar"
            +++
        """.trimIndent().split("\n")
    }

}

class BrowseContentTest {

    @Test
    fun `Should find slides`() {
        val expected = listOf(Slides(Title("A subject"), "bar/slides", false))
        val result = BrowseContent(FileRepositoryStub())("bar/slides")

        Assertions.assertEquals(expected, result)
    }
}
