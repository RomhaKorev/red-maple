package com.gitlab.hexaofcontent.view

import com.gitlab.hexaofcontent.contentbrowsing.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class GridTest {

    @Test
    fun `Should place first item at center`() {
            "Test F.I.R.S.T" should_produce

            """
             Test F.I.R.S.T -> 0;0
            """
    }

    @Test
    fun `Items should be placed side by side`() {
        "Test: Test F.I.R.S.T, Test Doubles, Unit Tests" should_produce

        """
         Test -> 0;0
         Test F.I.R.S.T -> 0;-1
         Test Doubles -> 1;0
         Unit Tests -> 0;1
        """
    }

    private infix fun String.should_produce(positions: String) {
        val content: TrainingContent = interpretContent(this.trimIndent())
        val result = Grid(content)

        check(positions.interpretPositions(), result)
    }

    private fun check(positions: Map<Title, Point>, result: Grid) {
        Assertions.assertEquals(positions.size, result.items.size)
        result.items.forEach {
            val expected = positions[it.title]
            Assertions.assertEquals(expected, it.position, "Expecting ${it.title} to be at $expected. But it's at ${it.position}")
        }
    }

    private fun interpretContent(content: String): TrainingContent {
        if (content.contains(":"))
        return Category(Title(content.split(": ").first())).with(content.split("\n").filter {it.trim() != ""}.flatMap {
            line ->
            val parts = line.split(":")
            if (parts.size == 2) {
                listOf(
                    Category(Title(parts[0].trim()))
                    .with(parts[1].split(",").map { it.trim() }.map { Slides(Title(it), "", false) }))
            } else {
                parts[0].split(",").map { it.trim() }.map { Slides(Title(it), "", false) }
            }
        })

        return SingleSlides(listOf(Slides(Title(content), "", false)))
    }

    private fun String.interpretPositions(): Map<Title, Point> {
        return this.trimIndent().split("\n").associate { line ->
            val parts = line.split("->").map { it.trim() }
            val point = parts[1].split(";").map { it.trim() }
            Title(parts[0]) to Point(point[0].toDouble(), point[1].toDouble())
        }
    }



}
