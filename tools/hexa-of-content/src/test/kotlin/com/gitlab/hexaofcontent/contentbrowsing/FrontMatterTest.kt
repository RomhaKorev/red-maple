package com.gitlab.hexaofcontent.contentbrowsing

import com.gitlab.hexaofcontent.contentbrowsing.FrontMatter
import com.gitlab.hexaofcontent.contentbrowsing.Title
import com.gitlab.hexaofcontent.contentbrowsing.getFrontMatter
import com.gitlab.hexaofcontent.contentbrowsing.getMetadata
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class FrontMatterTest {


    @Test
    fun `Should find the title in a front matter`() {
        """
            +++
            title = "Unit Tests"
            outputs = ["Reveal"]
            authors = "Seasick Steve"
            +++
        """ should_produce FrontMatter(Title("Unit Tests"), disabled = false, draft = false)
    }

    @Test
    fun `Should find the title in metadata`() {
        """
            {
                "title": "Craft Your Tests"
            }
        """.as_json() should_produce FrontMatter(Title("Craft Your Tests"), disabled = false, draft = false)
    }

    @Test
    fun `Should disable a content from a front matter`() {
        """
            +++
            title = "Unit Tests"
            outputs = ["Reveal"]
            authors = "Seasick Steve"
            visible = false
            +++
        """ should_produce FrontMatter(Title("Unit Tests"), disabled = true, draft = false)
    }

    @Test
    fun `Should disable a content from metadata`() {
        """
            {
                "title": "Craft Your Tests",
                "disabled": true
            }
        """.as_json() should_produce FrontMatter(Title("Craft Your Tests"), disabled = true, draft = false)
    }

    @Test
    fun `Should tag a content as draft from a front matter`() {
        """
            +++
            title = "Unit Tests"
            outputs = ["Reveal"]
            authors = "Seasick Steve"
            draft = true
            +++
        """ should_produce FrontMatter(Title("Unit Tests"), disabled = false, draft = true)
    }

    @Test
    fun `Should tag a content as draft from metadata`() {
        """
            {
                "title": "Craft Your Tests",
                "draft": true
            }
        """.as_json() should_produce FrontMatter(Title("Craft Your Tests"), disabled = false, draft = true)
    }

    @Test
    fun `Should use the tile value instead of title when it exists`() {
        """
            +++
            tile = "Introduction to UT"
            title = "Unit Tests"
            outputs = ["Reveal"]
            authors = "Seasick Steve"
            +++
        """ should_produce FrontMatter(Title("Introduction to UT"), disabled = false, draft = false)
    }





    private fun String.as_json(): Json {
        return Json(this)
    }

    private infix fun String.should_produce(expected: FrontMatter) {
        val result = getFrontMatter(this.trimIndent().split("\n"))

        Assertions.assertEquals(expected, result)
    }

    private infix fun Json.should_produce(expected: FrontMatter) {
        val result = getMetadata(value.trimIndent().split("\n"))

        Assertions.assertEquals(expected, result)
    }
}
data class Json(val value: String)
