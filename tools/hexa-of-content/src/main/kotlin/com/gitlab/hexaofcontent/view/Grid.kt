package com.gitlab.hexaofcontent.view

import com.gitlab.hexaofcontent.contentbrowsing.Category
import com.gitlab.hexaofcontent.contentbrowsing.SingleSlides
import com.gitlab.hexaofcontent.contentbrowsing.TrainingContent


object colors {
    val colors = Color.all
    var iterator = colors.iterator()

    fun next(): Color {
        if (iterator.hasNext())
            return iterator.next()
        iterator = colors.iterator()
        return iterator.next()
    }
}

class Grid {
    val items = mutableListOf<Item>()

    constructor(content: TrainingContent) {
        if (content is SingleSlides) {
            val singleGroup = Group()
            val positions = Positions()
            val mainColor = colors.next()
            content.forEach {
                singleGroup.place(positions, it, mainColor)
            }

            items.addAll(
                singleGroup.items
            )
        } else if (content is Category) {
            val positions = Positions()
            val group = Group()
            val groupColor = colors.next()
            group.place(positions, content, groupColor)
            val flattenContent = flattenCategory(content.subContent())
            flattenContent.forEach { subContent -> group.place(positions, subContent, groupColor) }

            items.addAll(
                group.items
            )
        }
    }

    private fun Group.place(positions: Positions, content: TrainingContent, color: Color) {
        val position = positions.findFreePosition()
        val item = Item(content, position, content.subContent().isNotEmpty(), color)
        this.add(item)
    }

    private fun flattenCategory(content: List<TrainingContent>): List<TrainingContent> {
        return content.flatMap {
            it.all()
        }
    }

    private fun TrainingContent.all(): List<TrainingContent> {
        if (subContent().isEmpty())
            return listOf(this)
        return this.subContent().flatMap { it.all() }
    }
}


