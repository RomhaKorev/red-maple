package com.gitlab.hexaofcontent.contentbrowsing

data class Title(val value: String) {
    override fun toString(): String {
        return value
    }

    infix fun or(other: Title): Title {
        if (this == INVALID)
            return other
        return this
    }

    companion object {
        val INVALID = Title("")
    }
}
