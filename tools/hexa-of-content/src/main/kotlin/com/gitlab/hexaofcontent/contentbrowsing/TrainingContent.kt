package com.gitlab.hexaofcontent.contentbrowsing

sealed interface TrainingContent {
    fun subContent(): List<TrainingContent>

    fun notEmpty(): Boolean

    val title: Title
    val path: String
    val draft: Boolean

    fun isValid(): Boolean
}
