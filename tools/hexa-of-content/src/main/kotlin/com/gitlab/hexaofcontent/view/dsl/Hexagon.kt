package com.gitlab.hexaofcontent.view.dsl


import com.gitlab.kigelia.core.dsl.svg.orphan
import com.gitlab.hexaofcontent.HEIGHT
import com.gitlab.hexaofcontent.WIDTH
import com.gitlab.hexaofcontent.view.Item
import com.gitlab.hexaofcontent.view.Point
import com.gitlab.kigelia.core.dsl.common.Tag


fun Point.toReal(offset: Point): Point {
    var newX = x * WIDTH
    val newY = y * HEIGHT * 3.0 / 4.0
    if ((y + 1).toInt().rem(2) != 0) {
        newX -= WIDTH / 2
    }
    return Point(newX , newY) + offset
}


class Hexagon(val item: Item, offset: Point): Tag("g") {
    val position = item.position.toReal(offset)
    val color = if(item.isRoot) "none" else item.color.primary
    val textColor = if(item.isRoot) item.color.primary else "white"
    val strokeColor = if(item.isRoot) item.color.primary else "#20353A"
    val scaleFactor = (WIDTH - 4.0) / WIDTH

    val isDraft = item.isDraft

    fun representation(): Tag {
        return if (item.isRoot) title() else subject()
    }

    private fun title(): Tag {
        return orphan("g") {
                if (isDraft) {
                    style {
                        "opacity"(0.5)
                    }
                }
                "transform"("translate(${position.x} ${position.y})")
                "fill"(color)
                "stroke"(strokeColor)
                "stroke-width"("2px")

                "g" {
                    "transform"("scale($scaleFactor $scaleFactor) translate(2 2)")
                    "fill"(color)
                    "stroke"(strokeColor)

                    "use" {
                        "href"("#hexagon")
                    }
                    "foreignObject" {
                        geometry {
                            x(10.0)
                            y(10.0)
                            width((WIDTH - 20))
                            height((HEIGHT - 20))
                        }

                        "div" {
                            style {
                                "color"(textColor)
                                "text-align"("center")
                                "height"("100%")
                                "display"("flex")
                                "justify-content"("center")
                                "align-content"("center")
                                "flex-direction"("column")
                                "text-transform"("uppercase")
                            }
                            "b" {-item.title.toString() }
                        }
                    }
                }
        }
    }

    private fun subject(): Tag {
        return orphan("g") {
                if (isDraft) {
                    style {
                        "opacity"(0.5)
                    }
                }
                "transform"("translate(${position.x} ${position.y})")
                "fill"(color)
                "stroke"(strokeColor)

                "use" {
                    "href"("#hexagon")
                }
                "a" {
                    "href"(item.path)
                    "foreignObject" {
                        geometry {
                            x(10.0)
                            y(10.0)
                            width((WIDTH - 20))
                            height((HEIGHT - 20))
                        }

                        "div" {
                            style {
                                "color"(textColor)
                                "text-align"("center")
                                "height"("100%")
                                "display"("flex")
                                "justify-content"("center")
                                "align-content"("center")
                                "flex-direction"("column")
                            }
                            - item.title.toString()
                        }
                    }
                }
            }
    }
}



fun Tag.hexagon(item: Item, offset: Point): Tag {
    val hex = Hexagon(item, offset)
    this.add(hex.representation())
    return this
}
