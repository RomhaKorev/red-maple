package com.gitlab.hexaofcontent.view.dsl

import com.gitlab.kigelia.core.dsl.html.HtmlElement
import com.gitlab.kigelia.core.dsl.svg.svg
import com.gitlab.hexaofcontent.HEIGHT
import com.gitlab.hexaofcontent.WIDTH
import com.gitlab.hexaofcontent.contentbrowsing.*
import com.gitlab.hexaofcontent.view.Grid
import com.gitlab.hexaofcontent.view.Point
import com.gitlab.kigelia.core.dsl.html.elements.div


fun HtmlElement.category(grid: Grid): HtmlElement {
    val items = grid.items
    val o = Point(0.5 - (items.minOf { it.position.x }),
                       0 - items.minOf { it.position.y })

    val offset = Point(o.x * WIDTH,
                       o.y * HEIGHT * 3 / 4)

    val viewBoxWidth = (items.map { it.position.toReal(offset).x }.maxOrNull() ?: 0.0) + WIDTH + 10.0
    val viewBoxHeight = (items.map { it.position.toReal(offset).y }.maxOrNull() ?: 0.0) + HEIGHT + 10.0

    return category {
        val e = svg {
            style {
                "align-self"("center")
                "width"(viewBoxWidth)
                "height"(viewBoxHeight)
            }
            viewbox {
                width(viewBoxWidth)
                height(viewBoxHeight)
            }
            "symbol" {
                "polygon" {
                    id("hexagon")
                    "points"("${WIDTH / 2} 0,$WIDTH ${HEIGHT / 4},$WIDTH ${HEIGHT / 4 * 3},${WIDTH / 2} $HEIGHT,0 ${HEIGHT / 4 * 3},0 ${HEIGHT / 4}")
                }
            }
            "g" {
                "transform"("translate(5 5)")
                "stroke-width"("2")
                grid.items. map {
                    hexagon(it, offset)
                }
            }
        }
        this.add(e)
    }
}

fun HtmlElement.category(init: HtmlElement.() -> Unit): HtmlElement {
    val elt = HtmlElement("div")
    elt.init()
    this.add(elt)
    return this
}

fun HtmlElement.category(content: TrainingContent): HtmlElement {
    val i: HtmlElement.() -> Unit = {
        "h2" { - content.title.toString() }

        div {
            style {
                "margin"("48px")
                "display"("flex")
                "flex-flow"("row wrap")
                "justify-content"("center")
                "justify-content"("space-around")
            }

            val outsideCategory = content.subContent().filter { it is Slides }
            if (outsideCategory.isNotEmpty()) {
                val grid = Grid(SingleSlides(outsideCategory))
                category(grid)
            }

            content.subContent().filter { it is Category }.forEach {
                val grid = Grid(it as Category)
                category(grid)
            }
        }
    }
    val elt = HtmlElement("div")
    elt.i()
    this.add(elt)
    return this
}
