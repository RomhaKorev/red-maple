package com.gitlab.hexaofcontent.contentbrowsing

data class Slides(override val title: Title, override val path: String, override val draft: Boolean): TrainingContent {
    override fun notEmpty(): Boolean {
        return true
    }

    override fun toString(): String {
        return title.toString()
    }

    override fun isValid(): Boolean {
        return this != INVALID
    }

    override fun subContent(): List<TrainingContent> {
        return emptyList()
    }

    companion object {
        val INVALID = Slides(Title.INVALID, "", false)
    }
}
