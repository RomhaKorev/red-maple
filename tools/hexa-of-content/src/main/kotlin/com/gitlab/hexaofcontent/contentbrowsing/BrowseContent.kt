package com.gitlab.hexaofcontent.contentbrowsing


class BrowseContent(private val repository: ContentRepository) {
    operator fun invoke(path: String): List<TrainingContent> {
        return browse(ContentRepository.pathOf(path), path).sortedBy {
            it.title.value
        }
    }

    private fun browse(path: ContentRepository.Path, root: String): List<TrainingContent> {
        val content = mutableListOf<TrainingContent>()
        repository(path).forEach { child ->
            if (child.isCategory) {
                content.add(makeCategory(child, root))
            } else if (child.isSlides) {
                //println(child.path)
                content.add(makeSlides(child))
            }
        }
        return content.toList().filter { it.isValid() }
    }

    private fun makeCategory(path: ContentRepository.Path, root: String): TrainingContent {
        val frontMatter = getMetadata(repository.getContent(path, "metadata.json"))

        if (frontMatter.disabled) {
            return Category.INVALID
        }

        val category = Category(frontMatter.title or Title(path.basename()))
        val content = browse(path, root)
        if (content.size == 1 && content[0].title == category.title)
            return content[0]
        return category.with(content)
    }

    private fun makeSlides(path: ContentRepository.Path): Slides {
        val frontMatter = getFrontMatter(repository.getContent(path, "_index.md"))

        if (frontMatter.disabled) {
            return Slides.INVALID
        }

        return Slides(frontMatter.title or Title(path.realPath), path.path, frontMatter.draft)
    }

}

