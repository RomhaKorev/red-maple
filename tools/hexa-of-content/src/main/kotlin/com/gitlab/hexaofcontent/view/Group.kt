package com.gitlab.hexaofcontent.view

class Group {
    val items = mutableListOf<Item>()
    fun add(item: Item) {
        items.add(item)
    }


    private fun left(): Double {
        return items.map { it.position.x }.minOrNull() ?: 0.0
    }

    private fun right(): Double {
        return items.map { it.position.x }.maxOrNull() ?: 0.0
    }

    fun moveToTheRightOf(previous: Group) {
        val offset = Point(previous.right() - this.left() + 1.5, 0.0)
        items.forEach {
            it.moveBy(offset)
        }
    }

    fun isNotEmpty(): Boolean {
        return items.isNotEmpty()
    }
}
