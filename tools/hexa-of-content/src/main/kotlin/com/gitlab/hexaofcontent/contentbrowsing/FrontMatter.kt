package com.gitlab.hexaofcontent.contentbrowsing

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

data class FrontMatter(val title: Title, val disabled: Boolean, val draft: Boolean) {
    @JsonCreator
    constructor(title: String, disabled: Boolean, draft: Boolean): this(Title(title), disabled, draft)
}


private fun sanitize(value: String): String {
    return value.replace("\\n", " ").replace("**", "").replace("\"", "")
}

fun getFrontMatter(lines: List<String>): FrontMatter {
    fun extractLines(): List<String> {
        if (lines.isEmpty()) {
            return emptyList()
        }
        val startAt = lines.indexOfFirst { it == "+++" }
        val endAt = startAt + lines.subList(startAt + 1, lines.size).indexOfFirst { it == "+++" }

        return lines.subList(startAt + 1, endAt + 1).map { it.trim() }
    }

    fun getProperty(line: String): Pair<String, String> {
        val parts = line.split("=").map { it.trim() }
        if (parts.size == 2) {
            return Pair(parts.first().lowercase(), sanitize(parts.last()))
        }
        return Pair("", "")
    }

    var title = ""
    var disabled = false
    var draft = false
    val frontMatterContent = extractLines()
    frontMatterContent.forEach { line ->
        val property = getProperty(line)

        if (property.first == "tile")
            title = property.second
        if (property.first == "title" && title == "")
            title = property.second
        if (property.first == "visible")
            disabled = !property.second.toBoolean()
        if (property.first == "draft")
            draft = property.second.toBoolean()
    }
    return FrontMatter(Title(title), disabled, draft)
}


fun getMetadata(lines: List<String>): FrontMatter {
    if (lines.isEmpty()) {
        return FrontMatter(Title.INVALID, disabled = false, draft = false)
    }
    val mapper = jacksonObjectMapper()
    return mapper.readValue<FrontMatter>(lines.joinToString("\n") { it })
}
