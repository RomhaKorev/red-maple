package com.gitlab.hexaofcontent

import com.gitlab.hexaofcontent.contentbrowsing.*
import com.gitlab.hexaofcontent.view.Grid
import com.gitlab.hexaofcontent.view.dsl.category
import com.gitlab.hexaofcontent.view.dsl.page
import java.io.File

val WIDTH = 150.0
val HEIGHT = 150.0



fun main(args: Array<String>) {
    val result = BrowseContent(FileRepository())(args[0])

    val image = page {
        result.map {
            category(it)
        }
    }

    val fileName = args[1]
    val myfile = File(fileName)

    println(image.render())

    myfile.printWriter().use { out ->
        out.println(image.render())
    }
}





