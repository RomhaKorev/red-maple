package com.gitlab.hexaofcontent.contentbrowsing

import java.io.File
import java.nio.file.Paths


interface ContentRepository {
    data class Path internal constructor(val realPath: String, val path: String, val isCategory: Boolean, val isSlides: Boolean) {
        fun basename(): String {
            return Paths.get(realPath).toFile().nameWithoutExtension
        }
    }
    operator fun invoke(path: Path): List<Path>
    fun getContent(path: Path, vararg more: String): List<String>

    companion object {
        fun pathOf(path: String): Path {
            val p = Paths.get(path)
            return Path(path,
                p.relativize(Paths.get(path)).toString(),
                p.toFile().isDirectory,
                Paths.get(path, "_index.md").toFile().exists())
        }
    }
}

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class FileRepository: ContentRepository {
    override operator fun invoke(path: ContentRepository.Path): List<ContentRepository.Path> {
        println("Retrieving ${path.realPath}")
        return File(path.realPath).list().toList()
            .map {
                val subPath = Paths.get(path.realPath, it)
                val containsIndexMd = Paths.get(subPath.toString(), "_index.md").toFile().exists()
                ContentRepository.Path(
                        subPath.toString(),
                        Paths.get(path.path, it).toString(),
                        !containsIndexMd && subPath.toFile().isDirectory,
                        containsIndexMd
                )
            }
    }

    override fun getContent(path: ContentRepository.Path, vararg more: String): List<String> {
        val completePath = Paths.get(path.realPath, *more)
        if (!completePath.toFile().exists()) {
            return emptyList()
        }
        return completePath.toFile().readLines()
    }
}
