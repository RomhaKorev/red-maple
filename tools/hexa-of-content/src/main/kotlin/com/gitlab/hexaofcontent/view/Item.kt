package com.gitlab.hexaofcontent.view

import com.gitlab.hexaofcontent.contentbrowsing.Title
import com.gitlab.hexaofcontent.contentbrowsing.TrainingContent

data class Item(private val content: TrainingContent, private val initialPosition: Point, val isRoot: Boolean, val color: Color) {
    private var currentPosition: Point = initialPosition

    val position: Point
    get() = currentPosition

    fun moveBy(offset: Point) {
        currentPosition += offset
    }

    val title: Title
        get() = content.title

    val isDraft: Boolean
        get() = content.draft

    val path: String
        get() = content.path
}




