package com.gitlab.hexaofcontent.view

class Positions {
    private val occupied = mutableListOf<Point>()

    fun findFreePosition(): Point {
        val position = occupied.firstNotNullOfOrNull { it.positionsAround().firstOrNull { p -> p.isFree() } } ?: Point(
            0.0,
            0.0
        )
        occupied.add(position)
        return position
    }

    private fun Point.isFree(): Boolean {
        return !occupied.contains(this)
    }
}
