package com.gitlab.hexaofcontent.view

data class Color(val primary: String, val secondary: String) {
    companion object {
        val all = listOf(Color("#26925E", "#36D068"), Color("#FF9600", "#FFBC0B"), Color("#2AA9B2", "#5DC5CC"), Color("rgb(219, 55, 89)", "rgb(219, 55, 89)"), Color("#51B6A9", "#51B6A9")/*Color("#D62626", "#F55656")*/)
    }
}
