package com.gitlab.hexaofcontent.view

data class Point(val x: Double, val y: Double): Comparable<Point> {
    override fun compareTo(other: Point): Int {
        return x.compareTo(other.x) + y.compareTo(other.y)
    }

    override fun toString(): String {
        return "(${x.toString().padStart(3)}; ${y.toString().padStart(3)})"
    }

    operator fun plus(offset: Point): Point {
        return Point(x + offset.x, y + offset.y)
    }


    fun positionsAround(dx: Double = 1.0, dy: Double = 1.0): List<Point> {
        return listOf(
            Point(x, y - dy),
            Point(x + dx, y),
            Point(x, y + dy),
            Point(x - dx, y + dy),
            Point(x - dx, y),
            Point(x - dx, y - dy)
        )
    }

    companion object {
        val INVALID = Point(Double.NaN, Double.NaN)
    }
}

