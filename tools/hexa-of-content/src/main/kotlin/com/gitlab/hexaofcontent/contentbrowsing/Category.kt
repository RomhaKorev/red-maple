package com.gitlab.hexaofcontent.contentbrowsing

class Category private constructor(override val title: Title, private val trainingContent: MutableList<TrainingContent>,
                                   override val path: String
): TrainingContent {
    constructor(title: Title): this(title, mutableListOf<TrainingContent>(), "")

    fun with(trainingContent: TrainingContent): Category {
        this.trainingContent.add(trainingContent)
        return this
    }

    fun with(trainingContent: List<TrainingContent>): TrainingContent {
        trainingContent.forEach {
            with(it)
        }
        return this
    }

    override fun notEmpty(): Boolean {
        return trainingContent.isNotEmpty()
    }

    override val draft: Boolean
        get() = false

    override fun toString(): String {
        return "$title: ${trainingContent.map { it.toString() }}"
    }

    override fun isValid(): Boolean {
        return this != INVALID && trainingContent.isNotEmpty()
    }

    override fun subContent(): List<TrainingContent> {
        return trainingContent
    }

    companion object {
        val INVALID = Category(Title.INVALID)
    }
}


class SingleSlides(private val trainingContent: List<TrainingContent>
): TrainingContent {
    override fun subContent(): List<TrainingContent> {
        return trainingContent
    }

    override fun notEmpty(): Boolean {
        return trainingContent.isNotEmpty()
    }

    override val title = Title("")
    override val path: String = ""
    override val draft: Boolean = false
    override fun isValid(): Boolean {
        return trainingContent.isNotEmpty()
    }

    fun forEach(f: (TrainingContent) -> Unit) {
        trainingContent.forEach(f)
    }
}