package com.gitlab.hexaofcontent.view.dsl

import com.gitlab.kigelia.core.dsl.html.*


fun page(init: HtmlElement.() -> Unit): HtmlStructure {
    val page = html {
        head {
            style {
                "h2" {
                        "position"  ("relative")
                        "font-size" ("30px")
                        "z-index"   (1)
                        "overflow"  ("hidden")
                        "text-align"("center")
                        "color"     ("#FAFAFA")
                    }
                "h2:before, h2:after" {
                    "position"        ("absolute")
                    "top"             ("51%")
                    "overflow"        ("hidden")
                    "width"           ("50%")
                    "height"          ("1px")
                    "content"          ("'\\a0'")
                    "background-color"("#F3F3F3")
                }
                "h2:before" {
                    "margin-left"("-50%")
                    "text-align" ("right")
                }
                "h1" {
                    "font-size"("64px")
                    "color"    ("#F3F3F3")
                }
            }
            meta {
                name("apple-mobile-web-app-capable")
                content("yes")
            }
            meta {
                name("apple-mobile-web-app-status-bar-style")
                content("black-translucent")
            }
            meta {
                name("viewport")
                content("width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no")
            }
            link {
                rel("stylesheet")
                href("./css/pumpkin.css")
            }
        }
        body {
            style {
                "background-color"("rgb(32, 53, 59)")
                "font-family"("'Lato', sans-serif !important")
                "color"("var(--foreground-color-primary) !important")
                "display"("flex")
                "flex-direction"("column")
            }
            "center" {
                "h1" { - "Things Explained" }
            }
            init()
        }
    }

    return page
}


/*
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>All the things we had to learn</title>

    <style>
        h2 {
        position: relative;
        font-size: 30px;
        z-index: 1;
        overflow: hidden;
        text-align: center;
        color: #FAFAFA;
    }
    h2:before, h2:after {
        position: absolute;
        top: 51%;
        overflow: hidden;
        width: 50%;
        height: 1px;
        content: '\a0';
        background-color: #F3F3F3;
    }

    h2:before {
        margin-left: -50%;
        text-align: right;
    }
    </style>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="./css/pumpkin.css">
  </head>
  <body style="background-color: rgb(32, 53, 59); font-family: 'Lato', sans-serif !important;color: var(--foreground-color-primary) !important;display: flex; flex-direction: column">
  <center><h1 style="font-size: 64px; color: #F3F3F3;">All the things we had to learn</h1></center>
 */
